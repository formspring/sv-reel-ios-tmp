//
//  RAPaginatingTableViewController.h
//  Reel
//
//  Created by Tim Bowen on 8/29/12.
//
//

#import <UIKit/UIKit.h>
#import "RAApiClient.h"
#import "RARefreshTableHeaderView.h"
#import "RAFeedItemCell.h"

@class RAPhotoPopoverViewController;
@class RAPhotoData;

@interface RAPaginatingTableViewController : UITableViewController <EGORefreshTableHeaderDelegate, RAFeedItemCellDelegate> {
    UILabel *_emptyLabel;
}

@property (assign) API_ENDPOINT endpoint;
@property (strong) NSMutableArray *items;
@property (strong, nonatomic) NSString *dataClassName;
@property (assign) BOOL loadingMore;
@property (assign, getter = canPaginate) BOOL paginatable;
@property (assign, getter = canRefresh) BOOL refreshable;
@property (strong, nonatomic) NSDictionary *paginationParams;
@property (strong, nonatomic) RARefreshTableHeaderView *refreshHeader;
@property (strong, nonatomic) RARefreshTableHeaderView *refreshFooter;
@property (strong, nonatomic) RAPhotoPopoverViewController *photoPopover;
@property (strong, nonatomic) NSString *urlPath;
@property (strong, nonatomic) NSString *trackedViewName;


- (id)initWithStyle:(UITableViewStyle)style andEndpoint:(API_ENDPOINT)endpoint forClass:(NSString *)className;
- (void)showLoadingIndicator;
- (void)hideLoadingIndicator;
- (void)showProfile:(RAAccountData *)account;
- (void)showPhoto:(RAPhotoData *)photo;
- (void)showPhotoWithID:(unsigned long long)photoID;
- (NSArray *)transformItemsArray:(NSArray*)array;
- (NSMutableDictionary *)paramsForRefresh;
-(void)fetchDataWithParams:(NSDictionary*)params;

@end
