//
//  RAForgotPasswordViewController.m
//  Reel
//
//  Created by Tim Bowen on 10/29/12.
//
//

#import "RAForgotPasswordViewController.h"
#import "SVProgressHUD.h"

@interface RAForgotPasswordViewController ()

@end

@implementation RAForgotPasswordViewController

@synthesize viewConfigured = _viewConfigured;
@synthesize tokenString = _tokenString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
        self.viewConfigured = NO;
        self.tokenString = nil;
    }
    return self;
}

- (void)loadView {
    [super loadView];
    if(self.tokenString) {
        self.title = @"Reset Password";
    } else {
        self.title = @"Forgot Password";
    }
    [self.signupButton setTitle:@"Submit" forState:UIControlStateNormal];
    self.facebookButton.hidden = YES;
    self.orLabel.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [(UITextField *)[(RAAccountInfoTableCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] infoView] becomeFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_BEIGE;
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(!self.viewConfigured) {
        self.signupButton.center = CGPointMake(self.signupButton.center.x, self.signupButton.center.y - 95);
        self.viewConfigured = YES;
    }
}

- (int)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"CellID";
    
    RAAccountInfoTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if(self.tokenString == nil) {
        if(cell == nil)
            cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"email"];
        cell.title = @"Email";
        cell.type = @"email";
        [(UITextField *)cell.infoView setReturnKeyType:UIReturnKeyDone];
        cell.delegate = self;
    } else {
        if(cell == nil)
            cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"password"];
        cell.title = @"Password";
        cell.type = @"password";
        [(UITextField *)cell.infoView setReturnKeyType:UIReturnKeyDone];
        cell.delegate = self;
    }

    return cell;
}


- (void)signupTouched:(id)sender {
    if(self.tokenString) {
        NSString *token = [[self.tokenString componentsSeparatedByString:@"="] objectAtIndex:[[self.tokenString componentsSeparatedByString:@"="] count] -1];
        
        //Eat the slash at the end of the token.
        token = [token substringToIndex:[token length] - 2];
        [self.signupInfo setObject:token forKey:@"token"];
        
        [SVProgressHUD show];
        
        [[RAApiClient sharedClient] post:PASSWORD_RESET withData:self.signupInfo successBlock:^(NSDictionary *data) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [defaults setObject:archivedData forKey:@"session"];
            [defaults synchronize];
            
            [SVProgressHUD showSuccessWithStatus:@""];
            [APPDELEGATE initializeAccount];
            [APPDELEGATE showLoggedInAppView];
        } errorBlock:^(NSError *error, NSURLRequest *request) {
            [SVProgressHUD showErrorWithStatus:@""];
        }];
    } else {
        [[RAApiClient sharedClient] get:PASSWORD_RESET withQueryParams:self.signupInfo successBlock:^(NSDictionary *data){
            RAAlert(@"Success", @"You should get an email shortly, please follow the instructions to reset your password");
        } errorBlock:^(NSError *error, NSURLRequest *request){
            RAAlert(@"Error", @"There was a problem resetting your password.");
        }];
    }
}

- (void)accountInfoReady:(id)accountInfo withType:(NSString *)type atIndex:(int)index {
    [super accountInfoReady:accountInfo withType:type atIndex:index];
    if(index == 0) {
        [self signupTouched:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
