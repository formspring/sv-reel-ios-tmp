//
//  RANotificationTableViewController.h
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RAPaginatingTableViewController.h"

@class RARequestDelegate;

@interface RANotificationTableViewController : RAPaginatingTableViewController
@end
