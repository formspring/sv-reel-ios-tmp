//
//  RAForgotPasswordViewController.h
//  Reel
//
//  Created by Tim Bowen on 10/29/12.
//
//

#import <UIKit/UIKit.h>
#import "RASignupViewController.h"

@interface RAForgotPasswordViewController : RASignupViewController 

@property (assign) BOOL viewConfigured;
@property (nonatomic, strong) NSString *tokenString;

@end
