//
//  RAAccountInfoTableCell.m
//  Reel
//
//  Created by Tim Bowen on 10/4/12.
//
//

#import "RAAccountInfoTableCell.h"
#import "RASignupViewController.h"

@implementation RAAccountInfoTableCell

@synthesize type = _type;
@synthesize title = _title;
@synthesize label = _label;
@synthesize infoView = _infoView;
@synthesize optional = _optional;
@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier forType:(NSString *)theType
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.optional = NO;
        self.type = theType;
        if([self.type isEqualToString:@"photo"]) {
            UIImageView *profilePicture = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"take_profilepic.png"]];
            self.infoView = (UIView *)profilePicture;
        } else {
            UITextField *textField = [[UITextField alloc] init];
            textField.returnKeyType = UIReturnKeyNext;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            self.infoView = (UIView *)textField;
        }
        NSLog(@"self.infoView = %@", self.infoView);
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect myFrame = self.contentView.frame;
    if (self.label == nil) {
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 95, myFrame.size.height)];
        self.label.text = self.title;
        self.label.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.label];
        NSLog(@"Adding a label! %@", self.label);
        NSLog(@"Subviews:  %@ %@", self.label, [self.contentView subviews]);
    }
    if(![self.type isEqualToString:@"photo"]) {
        //Set up for photo - add optional label I guess;
        CGRect textFieldFrame = CGRectMake(105, 0, self.contentView.frame.size.width - 105, self.contentView.frame.size.height);
        UITextField *textField = (UITextField *)self.infoView;
        self.infoView.frame = textFieldFrame;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        if (self.optional) {
            textField.placeholder = @"(Optional)";
        }
        if ([self.type isEqualToString:@"password"])  {
            textField.secureTextEntry = YES;
        }
        
        textField.delegate = self;
        self.infoView = (UIView *)textField;
    } else {
        self.infoView.frame = CGRectMake(105, 5, 40, 40);
        NSLog(@"info view: %@", self.infoView);
    }
    if([self.infoView superview] != self.contentView) {
        [self.contentView addSubview:self.infoView];
    }
    NSLog(@"Subviews: %@", [self.contentView subviews]);
}

- (void)activate {
    if([self.infoView isKindOfClass:[UITextField class]]) {
        [(UITextField *)self.infoView becomeFirstResponder];
    } else {
        //Picture
        if([self.delegate respondsToSelector:@selector(takeProfilePhoto)]) {
            [self.delegate takeProfilePhoto];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"text field return");
    [textField resignFirstResponder];
    [self.delegate accountInfoReady:textField.text withType:self.type atIndex:self.tag];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSLog(@"Did end editing");
    [textField resignFirstResponder];
    [self.delegate accountInfoReady:textField.text withType:self.type atIndex:-1];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [NSString stringWithFormat:@"%@%@", textField.text, string];
    [self.delegate accountInfoReady:newString withType:self.type atIndex:-1];
    return YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];
    if(selected) {
        if([self.infoView isKindOfClass:[UITextField class]]) {
            [self.infoView becomeFirstResponder];
        } else {
            //Picture
            
            if([self.delegate respondsToSelector:@selector(takeProfilePhoto)]) {
                [self.delegate takeProfilePhoto];
            }
        }
    }
}

@end
