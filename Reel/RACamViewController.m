//
//  RACamViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/11/12.
//
//

#import "RACamViewController.h"
#import "AVCamRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import "AVCamUtilities.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "RAReminderData.h"


@interface RACamViewController () <UIGestureRecognizerDelegate>

@end

@interface RACamViewController (InternalMethods)
- (CGPoint)convertToPointOfInterestFromViewCoordinates:(CGPoint)viewCoordinates;
- (void)tapToAutoFocus:(UIGestureRecognizer *)gestureRecognizer;
- (void)tapToContinouslyAutoFocus:(UIGestureRecognizer *)gestureRecognizer;
- (void)updateButtonStates;
@end

@interface RACamViewController (AVCamCaptureManagerDelegate) <AVCamCaptureManagerDelegate>
@end


@implementation RACamViewController
@synthesize infoButton;
@synthesize photoBar;
@synthesize cancelButton;
@synthesize switchToLibraryButton;
@synthesize orientation;

-(id)init
{
    if (self = [super initWithNibName:@"RACamViewController" bundle:nil]) {
        self.hidesBottomBarWhenPushed = YES;
        [self deviceOrientationDidChange];
    }
    return self;
}

-(void)applyButtonStyles
{
    self.photoBar.backgroundColor = [UIColor colorWithPatternImage:
                                     [UIImage imageNamed:@"photo_bar"]];
    
    self.flashToggleButton.layer.cornerRadius = 18;
    self.flashToggleButton.layer.borderColor = UIColorFromRGBWithAlpha(0xFFFFFF, 0.8).CGColor;
    self.flashToggleButton.layer.borderWidth = 1.0;
    self.flashToggleButton.backgroundColor = UIColorFromRGBWithAlpha(0xFFFFFF, 0.5);
    
    self.infoButton.layer.cornerRadius = 18;
    self.infoButton.layer.borderColor = UIColorFromRGBWithAlpha(0xFFFFFF, 0.8).CGColor;
    self.infoButton.layer.borderWidth = 1.0;
    self.infoButton.backgroundColor = UIColorFromRGBWithAlpha(0xFFFFFF, 0.5);
    
    self.trackedViewName = @"Camera Prompt";
    
    
    self.cameraToggleButton.layer.cornerRadius = 18;
    self.cameraToggleButton.layer.borderColor = UIColorFromRGBWithAlpha(0xFFFFFF, 0.8).CGColor;
    self.cameraToggleButton.layer.borderWidth = 1.0;
    [self.cameraToggleButton setBackgroundColor:UIColorFromRGBWithAlpha(0xFFFFFF, 0.5)];
    
    [self.infoButton setHidden:YES];
       
    [self.takePictureButton setEnabled:NO];
}

-(void)stopCamera
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[[self captureManager] session] stopRunning];
    });
}

-(void)startCamera
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[[self captureManager] session] startRunning];
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.takePictureButton setEnabled:YES];
        });
    });
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.reminder != nil && !self.isInfoHidden) {
        [self.infoView setHidden:NO];
        if (self.isNewUser) {
            [self.infoTitleLabel setText:@"Take your first photo"];
        } else {
            [self.infoTitleLabel setText:@"It's been a while"];
        }
        if (self.reminder.question != nil) {
            [self.infoTextView setText:self.reminder.question];
        } else {
            NSString *infoString = @"Take a photo";
            if(self.reminder.reel.name) {
                infoString = [infoString stringByAppendingFormat:@" of \"%@\"",self.reminder.reel.name];
            }
            [self.infoTextView setText:infoString];
        }
    } else {
        [self.infoView setHidden:YES];
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DLog(@"Beginning the orientation notification observation");
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self applyButtonStyles];
    
    
    
    if ([self captureManager] == nil) {
		AVCamCaptureManager *manager = [[AVCamCaptureManager alloc] init];
		[self setCaptureManager:manager];
		
		[[self captureManager] setDelegate:self];
        
		if ([[self captureManager] setupSession]) {
            AVCaptureVideoPreviewLayer *newCaptureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:[[self captureManager] session]];
			UIView *view = [self videoPreviewView];
			CALayer *viewLayer = [view layer];
			[viewLayer setMasksToBounds:YES];
			
			CGRect bounds = [view bounds];
			[newCaptureVideoPreviewLayer setFrame:bounds];
			
            AVCaptureConnection *stillImageConnection = [AVCamUtilities connectionWithMediaType:AVMediaTypeVideo fromConnections:[[manager stillImageOutput] connections]];
			if ([stillImageConnection isVideoOrientationSupported]) {
				[stillImageConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
			}
			
			[newCaptureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
			
			[viewLayer insertSublayer:newCaptureVideoPreviewLayer below:[[viewLayer sublayers] objectAtIndex:0]];
			
			[self setCaptureVideoPreviewLayer:newCaptureVideoPreviewLayer];
			
//            [self.photoBar setHidden:YES];
            // Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
            [self startCamera];
            
            UILabel *newFocusModeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, viewLayer.bounds.size.width - 20, 20)];
			[newFocusModeLabel setBackgroundColor:[UIColor clearColor]];
			[newFocusModeLabel setTextColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.50]];

            // Add a single tap gesture to focus on the point tapped, then lock focus
			UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToAutoFocus:)];
			[singleTap setDelegate:self];
			[singleTap setNumberOfTapsRequired:1];
			[view addGestureRecognizer:singleTap];
			
            // Add a double tap gesture to reset the focus mode to continuous auto focus
			UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToContinouslyAutoFocus:)];
			[doubleTap setDelegate:self];
			[doubleTap setNumberOfTapsRequired:2];
			[singleTap requireGestureRecognizerToFail:doubleTap];
			[view addGestureRecognizer:doubleTap];

        }
    }
    
    CGRect videoFrame = self.videoPreviewView.frame;
    videoFrame.origin.y = ([[UIScreen mainScreen] bounds].size.height - self.photoBar.frame.size.height - videoFrame.size.height)/2;
    DLog(@"%f, %f,%f", self.view.bounds.size.height, self.photoBar.frame.size.height, videoFrame.size.height);
    self.videoPreviewView.frame = videoFrame;
    
    CGRect accessoryFrame = self.accessoryView.frame;
    if (videoFrame.origin.y != 0) {
        accessoryFrame.origin.y = videoFrame.origin.y;
        self.accessoryView.frame = accessoryFrame;
    }
    //WHY U NO CALL THIS AT THE RIGHT TIME.
    [self viewDidAppear:NO];
}


-(IBAction)toggleFlash:(id)sender
{
    [self.flashToggleButton setEnabled:NO];

    AVCaptureFlashMode flashMode = [[self captureManager] toggleFlashMode];
        if(flashMode == AVCaptureFlashModeAuto){
            [self.flashToggleButton setImage:[UIImage imageNamed:@"flash-auto"] forState:UIControlStateNormal];
        }else if(flashMode == AVCaptureFlashModeOn){
            [self.flashToggleButton setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
        }else{
            [self.flashToggleButton setImage:[UIImage imageNamed:@"flash-off"] forState:UIControlStateNormal];
        }
        [self.flashToggleButton setEnabled:YES];
}

- (IBAction)toggleCamera:(id)sender
{
    // Toggle between cameras when there is more than one
    [[self captureManager] toggleCamera];
    
    // Do an initial focus
    [[self captureManager] continuousFocusAtPoint:CGPointMake(.5f, .5f)];
}

-(IBAction)showInfoView:(id)sender{
    [self.infoView setHidden:NO];
    [self.infoButton setHidden:YES];
}

-(IBAction)dismissInfoView:(id)sender{
    [self.infoView setHidden:YES];
    [self.infoButton setHidden:NO];
}

- (IBAction)captureStillImage:(id)sender
{
    // Capture a still image
    [[self takePictureButton] setEnabled:NO];
    
    
    AVCaptureConnection *stillImageConnection = [AVCamUtilities connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self.captureManager stillImageOutput] connections]];
    
    if ([stillImageConnection isVideoOrientationSupported])
    {
        [stillImageConnection setVideoOrientation:self.orientation];
        DLog(@"%u",self.orientation);
    }
    
    [[self.captureManager stillImageOutput] captureStillImageAsynchronouslyFromConnection:stillImageConnection
                                                         completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
															 
															 
															 if (imageDataSampleBuffer != NULL) {
																 NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
																 
                                                                 UIImage *image = [[UIImage alloc] initWithData:imageData];
                                                                 NSDictionary *photoDict = [NSDictionary dictionaryWithObject:image forKey:UIImagePickerControllerOriginalImage];
                                                                 [self.delegate imagePickerController:nil didFinishPickingMediaWithInfo:photoDict];
																 
															 }
															 else
																 [self captureManager:self.captureManager didFailWithError:error];
															 
																 [self captureManagerStillImageCaptured:self.captureManager];
                                                         }];
    
    
    
//    [[self captureManager] captureStillImage];
    
    // Flash the screen white and fade it out to give UI feedback that a still image was taken
    UIView *flashView = [[UIView alloc] initWithFrame:[[self videoPreviewView] frame]];
    [flashView setBackgroundColor:[UIColor whiteColor]];
    [[[self view] window] addSubview:flashView];
    
    [UIView animateWithDuration:.4f
                     animations:^{
                         [flashView setAlpha:0.f];
                     }
                     completion:^(BOOL finished){
                         [flashView removeFromSuperview];
                     }
     ];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewDidUnload
{
    [self setPhotoBar:nil];
    [self setCancelButton:nil];
    [self setSwitchToLibraryButton:nil];
    [self setInfoButton:nil];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setAccessoryView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.delegate = nil;
}

-(void)dealloc
{
    DLog(@"IMA HERE DEALLOCING");
}


- (void)deviceOrientationDidChange {
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    if(deviceOrientation == UIDeviceOrientationLandscapeLeft) {
        self.orientation = AVCaptureVideoOrientationLandscapeRight;
    }
    else if (deviceOrientation == UIDeviceOrientationLandscapeRight) {
        self.orientation = AVCaptureVideoOrientationLandscapeLeft;
    }
    else if (deviceOrientation == UIDeviceOrientationPortrait) {
        self.orientation = AVCaptureVideoOrientationPortrait;
    }
    else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown) {
        self.orientation = AVCaptureVideoOrientationPortraitUpsideDown;
    }
    else {
        self.orientation = AVCaptureVideoOrientationPortrait;
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)switchToLibraryClicked:(id)sender {
    [self.delegate showPhotoLibrary];
}

-(IBAction)cancelClicked:(id)sender
{
    self.flashToggleButton.hidden = YES;
    self.infoButton.hidden = YES;
    self.cameraToggleButton.hidden = YES;
    [self.delegate imagePickerControllerDidCancel:nil];
}


@end

@implementation RACamViewController (InternalMethods)

// Convert from view coordinates to camera coordinates, where {0,0} represents the top left of the picture area, and {1,1} represents
// the bottom right in landscape mode with the home button on the right.
- (CGPoint)convertToPointOfInterestFromViewCoordinates:(CGPoint)viewCoordinates
{
    CGPoint pointOfInterest = CGPointMake(.5f, .5f);
    CGSize frameSize = [[self videoPreviewView] frame].size;
    
    if ([_captureVideoPreviewLayer isMirrored]) {
        viewCoordinates.x = frameSize.width - viewCoordinates.x;
    }
    
    if ( [[_captureVideoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResize] ) {
		// Scale, switch x and y, and reverse x
        pointOfInterest = CGPointMake(viewCoordinates.y / frameSize.height, 1.f - (viewCoordinates.x / frameSize.width));
    } else {
        CGRect cleanAperture;
        for (AVCaptureInputPort *port in [[[self captureManager] videoInput] ports]) {
            if ([port mediaType] == AVMediaTypeVideo) {
                cleanAperture = CMVideoFormatDescriptionGetCleanAperture([port formatDescription], YES);
                CGSize apertureSize = cleanAperture.size;
                CGPoint point = viewCoordinates;
                
                CGFloat apertureRatio = apertureSize.height / apertureSize.width;
                CGFloat viewRatio = frameSize.width / frameSize.height;
                CGFloat xc = .5f;
                CGFloat yc = .5f;
                
                if ( [[_captureVideoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspect] ) {
                    if (viewRatio > apertureRatio) {
                        CGFloat y2 = frameSize.height;
                        CGFloat x2 = frameSize.height * apertureRatio;
                        CGFloat x1 = frameSize.width;
                        CGFloat blackBar = (x1 - x2) / 2;
						// If point is inside letterboxed area, do coordinate conversion; otherwise, don't change the default value returned (.5,.5)
                        if (point.x >= blackBar && point.x <= blackBar + x2) {
							// Scale (accounting for the letterboxing on the left and right of the video preview), switch x and y, and reverse x
                            xc = point.y / y2;
                            yc = 1.f - ((point.x - blackBar) / x2);
                        }
                    } else {
                        CGFloat y2 = frameSize.width / apertureRatio;
                        CGFloat y1 = frameSize.height;
                        CGFloat x2 = frameSize.width;
                        CGFloat blackBar = (y1 - y2) / 2;
						// If point is inside letterboxed area, do coordinate conversion. Otherwise, don't change the default value returned (.5,.5)
                        if (point.y >= blackBar && point.y <= blackBar + y2) {
							// Scale (accounting for the letterboxing on the top and bottom of the video preview), switch x and y, and reverse x
                            xc = ((point.y - blackBar) / y2);
                            yc = 1.f - (point.x / x2);
                        }
                    }
                } else if ([[_captureVideoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspectFill]) {
					// Scale, switch x and y, and reverse x
                    if (viewRatio > apertureRatio) {
                        CGFloat y2 = apertureSize.width * (frameSize.width / apertureSize.height);
                        xc = (point.y + ((y2 - frameSize.height) / 2.f)) / y2; // Account for cropped height
                        yc = (frameSize.width - point.x) / frameSize.width;
                    } else {
                        CGFloat x2 = apertureSize.height * (frameSize.height / apertureSize.width);
                        yc = 1.f - ((point.x + ((x2 - frameSize.width) / 2)) / x2); // Account for cropped width
                        xc = point.y / frameSize.height;
                    }
                }
                
                pointOfInterest = CGPointMake(xc, yc);
                break;
            }
        }
    }
    
    return pointOfInterest;
}

// Auto focus at a particular point. The focus mode will change to locked once the auto focus happens.
- (void)tapToAutoFocus:(UIGestureRecognizer *)gestureRecognizer
{
    if ([[[_captureManager videoInput] device] isFocusPointOfInterestSupported]) {
        CGPoint tapPoint = [gestureRecognizer locationInView:[self videoPreviewView]];
        CGPoint convertedFocusPoint = [self convertToPointOfInterestFromViewCoordinates:tapPoint];
        [_captureManager autoFocusAtPoint:convertedFocusPoint];
    }
}

// Change to continuous auto focus. The camera will constantly focus at the point choosen.
- (void)tapToContinouslyAutoFocus:(UIGestureRecognizer *)gestureRecognizer
{
    if ([[[_captureManager videoInput] device] isFocusPointOfInterestSupported])
        [_captureManager continuousFocusAtPoint:CGPointMake(.5f, .5f)];
}

// Update button states based on the number of available cameras and mics
- (void)updateButtonStates
{
//	NSUInteger cameraCount = [[self captureManager] cameraCount];
//	NSUInteger micCount = [[self captureManager] micCount];
//    
//    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
//        if (cameraCount < 2) {
//            [[self cameraToggleButton] setEnabled:NO];
//            
//            if (cameraCount < 1) {
//                [[self stillButton] setEnabled:NO];
//                
//                if (micCount < 1)
//                    [[self recordButton] setEnabled:NO];
//                else
//                    [[self recordButton] setEnabled:YES];
//            } else {
//                [[self stillButton] setEnabled:YES];
//                [[self recordButton] setEnabled:YES];
//            }
//        } else {
//            [[self cameraToggleButton] setEnabled:YES];
//            [[self stillButton] setEnabled:YES];
//            [[self recordButton] setEnabled:YES];
//        }
//    });
}

@end


@implementation RACamViewController (AVCamCaptureManagerDelegate)

- (void)captureManager:(AVCamCaptureManager *)captureManager didFailWithError:(NSError *)error
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
        RAAlert([error localizedDescription], [error localizedFailureReason]);
    });
}

- (void)captureManagerStillImageCaptured:(AVCamCaptureManager *)captureManager
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
        [[self takePictureButton] setEnabled:YES];
    });
}

- (void)captureManagerDeviceConfigurationChanged:(AVCamCaptureManager *)captureManager
{
	[self updateButtonStates];
}

@end
