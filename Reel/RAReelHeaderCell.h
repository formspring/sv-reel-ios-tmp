//
//  RAReelHeaderCell.h
//  Reel
//
//  Created by Sandosh Vasudevan on 10/24/12.
//
//

#import <UIKit/UIKit.h>
#import "RAFeedItemCell.h"

typedef enum {
    RAReelHeaderCellTypePlain = 0,
    RAReelHeaderCellTypeReel = 1
} RAReelHeaderCellType;

@class RAPhotoData;

@interface RAReelHeaderCell : UITableViewCell

@property (strong, nonatomic) RAPhotoData *photo;
@property (assign, nonatomic) id<RAFeedItemCellDelegate> delegate;
@property (assign, nonatomic) RAReelHeaderCellType type;

@end
