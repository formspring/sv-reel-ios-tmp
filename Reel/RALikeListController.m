//
//  RALikeListController.m
//  Reel
//
//  Created by Tim Bowen on 9/4/12.
//
//

#import "RALikeListController.h"
#import "RAPhotoData.h"
#import "RALikeDataArray.h"
#import "RALikeData.h"
#import "UIImageView+AFNetworking.h"
#import "RALikeCell.h"

@interface RALikeListController ()

@end

@implementation RALikeListController

@synthesize feedItem;

- (id)initWithStyle:(UITableViewStyle)style forFeedItem:(RAPhotoData *)item
{
    self = [super initWithStyle:style andEndpoint:PHOTO forClass:@"RALikeData"];
    if (self) {
        // Custom initialization
        self.feedItem = item;
        self.urlPath = [NSString stringWithFormat:@"%lld/likes", self.feedItem.itemID];
        self.endpoint = PHOTO;
        self.dataClassName = NSStringFromClass([RALikeData class]);
        self.title = [NSString stringWithFormat:@"%i Like%@", self.feedItem.likeCount, (self.feedItem.likeCount > 1) ? @"s" : @""];
        self.tableView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Likes";
    self.trackedViewName = self.title;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self items] count];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 50;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    RALikeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIImageView *avatarView;
    if(cell == nil) {
        cell = [[RALikeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        avatarView = [[UIImageView alloc] init];
        avatarView.frame = CGRectMake(5, 5, 40, 40);
        avatarView.tag = 1;
        [cell.contentView addSubview:avatarView];
    } else {
        avatarView = (UIImageView*)[cell.contentView viewWithTag:1];
    }
    RALikeData *like = [self.items objectAtIndex:indexPath.row];
    [cell.textLabel setText:like.liker.name];
    [avatarView setImageWithURL:[NSURL URLWithString:like.liker.avatar150]];
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RALikeData *like = [self.items objectAtIndex:indexPath.row];
    [self showProfile:like.liker];
}

@end
