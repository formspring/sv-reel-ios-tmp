//
//  RANotificationSettings.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "RANotificationSettingsViewController.h"

@interface RANotificationSettingsViewController ()
@property (strong, nonatomic) NSMutableArray *notifications;
@end

@implementation RANotificationSettingsViewController
@synthesize notifications = _notifications;

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Notifications";
    //self.tableView.backgroundColor = COLOR_BEIGE;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:self.tableView.frame];
    self.tableView.backgroundView.backgroundColor = COLOR_BEIGE;
    [self addSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
       [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
           UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] 
                                               initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
           spinner.frame = CGRectMake(cell.frame.origin.x + 20, cell.frame.size.height/2 - spinner.frame.size.height/2, spinner.frame.size.width, spinner.frame.size.height);
           [spinner startAnimating];
           [spinner setTag: 1];
           [cell addSubview:spinner];
           [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
           [cell setAccessoryType:UITableViewCellAccessoryNone];
       }]; 
    }];
    
    [[RAApiClient sharedClient] get:ACCOUNT_SETTINGS successBlock:^(NSDictionary *data) {
        DLog(@"%@", data);
        NSArray *notificationSettings = [data objectForKey:@"notifications"];
        self.notifications = [NSMutableArray arrayWithCapacity:[notificationSettings count]];
        [notificationSettings enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSMutableDictionary *notification = [NSMutableDictionary dictionaryWithDictionary:obj];
            [self.notifications addObject:notification];
        }];
        [self displaySettings];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        RAAlert(@"Unable to load settings", [error localizedDescription]);
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0]];
        UIActivityIndicatorView *spinner = (UIActivityIndicatorView*)[cell viewWithTag: 1];
        [spinner stopAnimating];
    }];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


-(void)displaySettings
{
    [self.tableView beginUpdates];
    [self removeSectionAtIndex:0];
    __weak RANotificationSettingsViewController *_self = self;
    [self insertSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
        [self.notifications enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [_self addNotificationSettingCell:obj inSection:section];
        }];
    } atIndex:0 animated:YES];
    
    [self.tableView endUpdates]; 
}

-(void)addNotificationSettingCell:(NSDictionary *)notificationSetting inSection:(JMStaticContentTableViewSection *)section
{
    [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        cell.accessoryView = switchView;
        [switchView addTarget:self action:@selector(notificationSwitchChanged:) forControlEvents:UIControlEventValueChanged];
        if ([[notificationSetting objectForKey:@"value"] boolValue]) {
            [switchView setOn:YES animated:NO];
        } else {
            [switchView setOn:NO animated:NO];
        }
        [switchView setTag:indexPath.row];
        [cell.textLabel setText:[notificationSetting objectForKey:@"label"]];        
    }];
}

- (void)notificationSwitchChanged:(id)sender {
    NSMutableDictionary *notificationSetting = [self.notifications objectAtIndex:[sender tag]];
    [notificationSetting setObject:[NSNumber numberWithBool:[sender isOn]] forKey:@"value"];
    
    NSDictionary *setting = @{@"setting": [notificationSetting objectForKey:@"name"],
                                @"value": [[notificationSetting objectForKey:@"value"] stringValue]
                            };
    [[RAApiClient sharedClient] post:ACCOUNT_SETTINGS withData:setting successBlock:^(NSDictionary *data) {
        DLog(@"success: %@", data);
        
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        RAAlert(@"Error", @"Unable to save settings");
        NSNumber *newSetting = [NSNumber numberWithBool:![(NSNumber *)[notificationSetting objectForKey:@"value"] boolValue]];
        [notificationSetting setObject:newSetting forKey:@"value"];
        [self.tableView reloadData];
    }];
}


@end
