//
//  RAProfileViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//
//

#import <UIKit/UIKit.h>
#import "RAAccountData.h"

@interface RAProfileViewController : UIViewController <UIActionSheetDelegate>

@property (strong, nonatomic) RAAccountData *account;

+ (id)instanceForAccount:(RAAccountData *)account;
@property (strong, nonatomic) UIViewController *currentViewController;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end
