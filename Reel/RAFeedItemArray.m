//
//  RAFeedItemDataArray.m
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RAFeedItemArray.h"
#import "RAFeedItemData.h"

@implementation RAFeedItemArray

+ (NSMutableArray *)fromHttpRequest:(ASIHTTPRequest *)request {
    DLog(@"%@", [[request url] absoluteString]);
    NSError *e = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData: [request responseData] options: NSJSONReadingMutableContainers error: &e];
    if (!e) {
        return [RAFeedItemArray fromDict:jsonData];
    }
    return nil;
}

+ (NSMutableArray *) fromDict:(NSDictionary *)dict
{
    NSMutableArray *feedItemArray = [[NSMutableArray alloc] init];
    NSArray *matches = dict[@"matches"];
    for (NSDictionary *jsonDict in matches) {
        [feedItemArray addObject:[RAFeedItemData fromDict:jsonDict]];
    }
    return feedItemArray;
}

@end
