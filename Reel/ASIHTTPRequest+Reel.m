//
//  ASIHTTPRequest+Reel.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//
//

#import "ASIHTTPRequest+Reel.h"

@implementation ASIHTTPRequest (Reel)

-(id)jsonData
{
    NSError *e = nil;
    if([[self responseData] length] > 0) {
        id jsonData = [NSJSONSerialization JSONObjectWithData: [self responseData] options: NSJSONReadingMutableContainers error: &e];
        if (!e) {
            return jsonData;
        } else {
            return nil;
        }
    }
    return nil;
}

@end
