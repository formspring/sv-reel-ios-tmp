//
//  RAReelCell.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//
//

#import <UIKit/UIKit.h>
#import "RAReelData.h"
#import "RAGroupedPhotosCell.h"

typedef enum {
    REEL_CELL_TYPE_ONE,
    REEL_CELL_TYPE_TWO,
    REEL_CELL_TYPE_THREE,
} REEL_CELL_TYPE;

@interface RAReelCell : RAGroupedPhotosCell

@property(strong, nonatomic) RAReelData *reel;

@end
