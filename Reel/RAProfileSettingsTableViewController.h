//
//  RAProfileSettingsTableViewController.h
//  Reel
//
//  Created by Tim Bowen on 10/17/12.
//
//

#import <UIKit/UIKit.h>
#import "RAAccountInfoTableCell.h"
#import "RACamHolderViewController.h"

@interface RAProfileSettingsTableViewController : UITableViewController <RAAccountInfoDelegate, RACamHolderViewDelegate>

@property (nonatomic, strong)NSMutableDictionary *accountInfo;

@end
