//
//  RALikeDataArray.h
//  Reel
//
//  Created by Tim Bowen on 9/4/12.
//
//

#import <Foundation/Foundation.h>

@interface RALikeDataArray : NSObject <RADataObject>

+ (NSMutableArray *) fromDict:(NSDictionary *)dict;
+ (NSMutableArray *)fromHttpRequest:(ASIHTTPRequest *)request;

@end
