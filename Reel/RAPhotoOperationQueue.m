//
//  RAPhotoOperationQueue.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//
//

#import "RAPhotoOperationQueue.h"
#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest+Reel.h"
#import "UIImage+RAFixOrientation.h"
#import "RAPhotoData.h"
#import "Reachability.h"
#import "GTMUIImage+Resize.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"

@implementation RAPhotoOperationQueue
@synthesize photo = _photo;
@synthesize photoID = _photoID;
@synthesize photoSaveRequest = _photoSaveRequest;
@synthesize photoUploadRequest = _photoUploadRequest;
@synthesize result = _result;
@synthesize payload = _payload;

-(id)initWithPhoto:(UIImage *)photo 
{
    self = [super init];
    if (self != nil) {
        [self setMaxConcurrentOperationCount:1];
        self.payload = [NSMutableDictionary dictionary];
        self.photo = photo;
        
        
        NSInvocationOperation *photoDataOperation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                         selector:@selector(photoDataOperation) object:nil];
        NSInvocationOperation *photoUploadOperation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                         selector:@selector(photoUploadOperation) object:nil];
        [self addOperation:photoDataOperation];
        [self addOperation:photoUploadOperation];
        [APPDELEGATE.photoQueueArray addObject:self];
        
        [self beingBackgroundTask];
    }
    return self;
}

- (void) beingBackgroundTask
{
    self.backgroundUpdateTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self endBackgroundTask];
    }];
}

- (void) endBackgroundTask
{
    [[UIApplication sharedApplication] endBackgroundTask: self.backgroundUpdateTask];
    self.backgroundUpdateTask = UIBackgroundTaskInvalid;
}

+(RAPhotoOperationQueue *)instanceForPhoto:(UIImage *)photo fromCamera:(BOOL)fromCamera
{
    RAPhotoOperationQueue *operationQueue = [[RAPhotoOperationQueue alloc] initWithPhoto:photo];
    operationQueue.fromCamera = fromCamera;
    return operationQueue;
}

-(void) sendFinishedNotification
{
    [APPDELEGATE.photoQueueArray removeObject:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PhotoUploaded" object:self];
}

-(void) photoUploadOperation
{
    NSData *imageData = UIImageJPEGRepresentation(self.photo, 1);
    NSMutableURLRequest *request = [[RAApiClient sharedClient] multipartFormRequestWithMethod:@"POST" path:@"/photo" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"photo_upload" fileName:@"photo_upload.jpg" mimeType:@"image/jpeg"];
    }];
    
    self.photoUploadRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        DLog(@"%@", JSON);
        self.photoID = [JSON objectForKey:@"original"];
        [self.payload setObject:self.photoID forKey:@"photo_url"];
        if (self.mockPhotoData != nil) {
            [self addPhotoSaveRequestOperation];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if ([JSON objectForKey:@"error_message"]) {
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:[JSON objectForKey:@"error_message"] forKey:NSLocalizedDescriptionKey];
            self.error = [NSError errorWithDomain:@"me.makereel.reel" code:[[JSON objectForKey:@"error_code"] intValue] userInfo:details];
        } else {
            self.error = error;
        }
        DLog(@"%@", error);
        DLog(@"%@", [JSON objectForKey:@"error_message"]);
        [self endBackgroundTask];
        [self sendFinishedNotification];
        [self cancelAllOperations];
    }];
    [self.photoUploadRequest setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        self.progress = (float)totalBytesWritten/(float)totalBytesExpectedToWrite;
        DLog(@"%f", self.progress);
    }];
    [self addOperation:self.photoUploadRequest];
}

-(void) photoDataOperation
{
    UIImage *imageWithProperOrientaiton;
    if (self.photo.size.width > 1136 || self.photo.size.height > 1136) {
        imageWithProperOrientaiton = [self.photo gtm_imageByResizingToSize:CGSizeMake(1136, 1136) preserveAspectRatio:YES trimToFit:NO];
    } else {
        imageWithProperOrientaiton = self.photo;
    }
    if (self.isFromCamera) {
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library saveImage:self.photo toAlbum:@"Reel" withCompletionBlock:^(NSURL *assetURL, NSError *error) {
            DLog(@"PHOTO Saved to album %@", [assetURL absoluteString]);
            if (!error) {
                self.assetURL = assetURL;
            }
        }];
    }
    self.photo = imageWithProperOrientaiton;
}

- (void)request:(ASIHTTPRequest *)request didSendBytes:(long long)bytes
{
    DLog(@"%lld, %lld", bytes, [request postLength]);
    self.progress = bytes/[request postLength];
}

-(BOOL)savePhotoToReel:(RAReelData *)reel comment:(NSString *)comment share:(BOOL)share
{
    if (self.photoUploadRequest.isCancelled
        || (self.photoUploadRequest.isFinished && self.photoID == nil)) {
        // May be we wanna do something else here.
        [self endBackgroundTask];
        [self sendFinishedNotification];
        return NO;
    }

    
    if (reel.itemID > 0) {
        [self.payload setObject:[NSString stringWithFormat:@"%lld", reel.itemID] forKey:@"reel_id"];
    }
    if (reel.name != nil) {
        [self.payload setObject:reel.name forKey:@"reel_name"];        
    }
    if (comment != nil) {
        [self.payload setObject:comment forKey:@"comment_text"];        
    }
    [self.payload setObject:[NSNumber numberWithBool:share] forKey:@"share_facebook"];
    [self.payload setObject:[NSNumber numberWithBool:YES] forKey:@"save"];
    
    self.mockPhotoData = [[RAPhotoData alloc] init];
    self.mockPhotoData.account = APPDELEGATE.account;
    self.mockPhotoData.reel = reel;
    
    if (self.photoID != nil) {
        [self addPhotoSaveRequestOperation];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PhotoUploading" object:self];
    
    return YES;
}

-(void)addPhotoSaveRequestOperation {
    NSInvocationOperation *photoSaveOperation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                     selector:@selector(photoSaveRequestOperation) object:nil];
    [self addOperation:photoSaveOperation];
}

-(void)photoSaveRequestOperation {
    DLog(@"%@", self.payload);
    NSURLRequest *request = [[RAApiClient sharedClient] multipartFormRequestWithMethod:@"POST" path:@"/photo" parameters:self.payload constructingBodyWithBlock: ^(id <AFMultipartFormData> formData) {
    }];
    
    self.photoSaveRequest =  [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        DLog(@"%@", JSON);
        RAPhotoData *photo = [RAPhotoData fromDict:JSON];
        if (photo.itemID) {
            self.result = JSON;
        }
        [self endBackgroundTask];
        [self sendFinishedNotification];

    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if ([JSON objectForKey:@"error_message"]) {
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:[JSON objectForKey:@"error_message"] forKey:NSLocalizedDescriptionKey];
            self.error = [NSError errorWithDomain:@"me.makereel.reel" code:[[JSON objectForKey:@"error_code"] intValue] userInfo:details];
        } else {
            self.error = error;
        }
        [self endBackgroundTask];
        [self sendFinishedNotification];
        [self cancelAllOperations];
    }];
    [self addOperation:self.photoSaveRequest];
}



@end
