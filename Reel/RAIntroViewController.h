//
//  RAIntroViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/15/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface RAIntroViewController : GAITrackedViewController <UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *firstView;
@property (strong, nonatomic) IBOutlet UIView *secondView;
@property (strong, nonatomic) IBOutlet UIView *thirdView;
@property (strong, nonatomic) IBOutlet UIView *fourthView;
@property (strong, nonatomic) IBOutlet UIView *fifthView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, assign) BOOL pageControlBeingUsed;
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (assign, getter = isLoginOnly) BOOL loginOnly;

- (IBAction)loginWithFacebookTouched:(id)sender;
- (IBAction)changePage:(id)sender;
- (IBAction)signupTouched:(id)sender;
- (IBAction)loginTouched:(id)sender;
- (IBAction)exploreTouched:(id)sender;

@end
