//
//  RAAddReelViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "JMStaticContentTableViewController.h"
#import "RAReelData.h"

@interface RAAddReelViewController : JMStaticContentTableViewController <UITextFieldDelegate>

@property (strong, nonatomic) RAReelData *selectedReel;

@end
