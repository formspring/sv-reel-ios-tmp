//
//  UIActivityIndicatorView+Reel.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//



@interface UIActivityIndicatorView (Reel)

+ (UIActivityIndicatorView *)addSpinnerTo:(UITableViewCell *)cell;
@end
