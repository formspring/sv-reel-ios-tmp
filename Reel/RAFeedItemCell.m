//
//  RAFeedItemCell.m
//  Reel
//
//  Created by Tim Bowen on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RAFeedItemCell.h"
#import "RAApiClient.h"
#import "ASIHTTPRequest.h"
#import "ASIDownloadCache.h"
#import "RAPhotoData.h"
#import "UIImageView+AFNetworking.h"
#import "RAPhotoOperationQueue.h"
#import "DDProgressView.h"

@implementation RAFeedItemCell
@synthesize feedItem = _feedItem;

@synthesize photoView = _photoView;
@synthesize likeButton = _likeButton;
@synthesize commentButton = _commentButton;
@synthesize timestampLabel = _timestampLabel;
@synthesize likeLabel = _likeLabel;
@synthesize commentLabel = _commentLabel;
@synthesize optionsContainer = _optionsContainer;
@synthesize delegate = _delegate;
@synthesize indexPath = _indexPath;

- (id)initWithFeedItem:(RAPhotoData *)feedItemData andReuseIdentifier:(NSString *)reuseIdentifier;
{
    self = [super initWithStyle:UITableViewStylePlain reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.photoView = [[UIImageView alloc] init];
        self.heartView = [UIButton buttonWithType:UIButtonTypeCustom];
        self.feedItem = feedItemData;
        self.commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIColor *shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
        self.commentButton.titleLabel.shadowOffset = CGSizeMake(0, 1);
        [self.commentButton.titleLabel setShadowColor:shadowColor];
        self.likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.likeButton.titleLabel setShadowColor:shadowColor];
        self.likeButton.titleLabel.shadowOffset = CGSizeMake(0, 1);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.indexPath = nil;
        self.contentView.backgroundColor = COLOR_BEIGE;
        self.backgroundColor = COLOR_BEIGE;
        
        self.photoActionVC = [[RAPhotoActionViewController alloc] initWithNibName:@"RAPhotoActionViewController" bundle:nil];
        self.photoActionVC.view.backgroundColor = [UIColor clearColor];
        self.photoActionVC.delegate = self;
        self.photoActionVC.photo = self.feedItem;
        [self.photoActionVC.headerView setHidden:YES];
        self.photoActionVC.footerView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
        tapRecognizer.cancelsTouchesInView = NO;
        [self addGestureRecognizer:tapRecognizer];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    DLog(@"%@, %f", keyPath, self.photoQueue.progress);
    if ([keyPath isEqualToString:@"progress"]) {
        [self.progressView setProgress:self.photoQueue.progress > 0.95 ? 0.95 : self.photoQueue.progress];
    } else {
        [self layoutSubviews];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentFrame = self.contentView.frame;
    int padding = 10;
    //Photo should be a square image that uses the rest of hte space.
    int wide = MIN(self.frame.size.height - 2 *padding, self.frame.size.width - 2 *padding);
    
    
    self.photoView.frame = CGRectMake(padding, padding, wide, wide);
    self.photoView.center = CGPointMake(self.contentView.frame.size.width/2, self.photoView.center.y);
    self.photoView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"feed_cell.png"]];
    backgroundView.center = self.photoView.center;
    backgroundView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
    [self.contentView addSubview:backgroundView];
    [self.contentView addSubview:self.photoView];

    
    if (self.isUploading) {
        self.photoView.alpha = 0.2;
        self.progressView = [[DDProgressView alloc] initWithFrame:CGRectMake((contentFrame.size.width - 200)/2, contentFrame.size.height/2, 200, 10)];
        self.progressView.innerColor = [UIColor whiteColor];
        self.progressView.outerColor = [UIColor whiteColor];
        self.progressView.center = self.photoView.center;
        self.progressView.progress = self.photoQueue.progress;
        [self.contentView addSubview:self.progressView];
    }
    else {
        int buttonHeight = 25;
        self.photoView.alpha = 1;
        self.photoActionVC.view.frame = CGRectMake(padding, self.frame.size.height - 3 * padding - buttonHeight, self.contentView.frame.size.width - (2*padding), 50);
        [self.contentView addSubview:self.photoActionVC.view];
        [self.photoActionVC setupUI];
    }
    
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.contentView.autoresizesSubviews = NO;


}

- (void)getImages {
    NSURL *photoURL = [NSURL URLWithString:self.feedItem.url600x600l];
    [self.photoView setImageWithURL:photoURL];
}

- (void)setFeedItem:(RAPhotoData *)feedItem {
    _feedItem = feedItem;
    if (_feedItem == nil) {
        return;
    }
    
    if (self.isUploading) {
        [self.photoView setImage:self.photoQueue.photo];
    } else {
        [self getImages];
    }
    
    [self.feedItem addObserver:self forKeyPath:@"likeCount" options:NSKeyValueObservingOptionNew  context:NULL];
    [self.feedItem addObserver:self forKeyPath:@"commentCount" options:NSKeyValueObservingOptionNew  context:NULL];
    
    self.photoActionVC.photo = feedItem;

}

- (void)setPhotoQueue:(RAPhotoOperationQueue *)photoQueue
{
    _photoQueue = photoQueue;
    [self.photoQueue addObserver:self forKeyPath:@"progress" options:NSKeyValueObservingOptionNew context:NULL];
}

-(void)likeListClickedForPhoto:(RAPhotoData *)photo
{
    [self.delegate showLikeList:self.feedItem];
}

-(void)commentClickedForPhoto:(RAPhotoData *)photo
{
    [self.delegate commentClicked:self.feedItem];
}


- (void)likeClicked {
    NSLog(@"We be clickin yall");
    [self.delegate showLikeList:self.feedItem];
    
}

- (void)commentClicked {
    [self.delegate commentClicked:self.feedItem];
}

- (void)heartClicked {
    [self.delegate likeClicked:self.feedItem atIndexPath:[(UITableView *)self.superview indexPathForCell: self]];
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    DLog(@"A TAP WAS DETECTED HOLY MOLEY");
    CGPoint point = [sender locationInView:self];
    if(CGRectContainsPoint(self.photoActionVC.view.frame, point)) {
        return;
    }
    [self.delegate showPhoto:self.feedItem];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.feedItem removeObserver:self forKeyPath:@"likeCount"  context:NULL];
    [self.feedItem removeObserver:self forKeyPath:@"commentCount"  context:NULL];
    [self.photoQueue removeObserver:self forKeyPath:@"progress" context:NULL];

    self.feedItem = nil;
    self.photoView.image = nil;
    
    
    self.delegate = nil;
    self.indexPath = nil;
    self.progressView = nil;
    self.photoQueue = nil;

}

- (void)dealloc {
    [self.feedItem removeObserver:self forKeyPath:@"likeCount"  context:NULL];
    [self.feedItem removeObserver:self forKeyPath:@"commentCount"  context:NULL];
}


@end
