//
//  RAExploreViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/23/12.
//
//

#import "RAExploreViewController.h"
#import "RAPhotoData.h"
#import "UIImageView+AFNetworking.h"
#import "RAReelHeaderCell.h"
#import "RALoggedoutExploreViewController.h"
#import "RAProfileViewController.h"
#import "RAPhotoBrowser.h"

@interface RAExploreViewController ()

@end

@implementation RAExploreViewController

- (id)init
{
    if (self = [super initWithAccount:nil Reel:nil]) {
        self.refreshable = NO;
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(next:)];
    if (self.parentController) {
        self.parentController.title = @"Explore";
        self.parentController.navigationItem.rightBarButtonItem = button;
        self.trackedViewName = @"LoggedoutExplore";
    } else {
        self.title = @"Explore";
        self.navigationItem.rightBarButtonItem = button;
        self.trackedViewName = self.title;
    }
}

-(void)setItems:(NSMutableArray *)items
{
    [super setItems:items];
    if (self.lastReelID) {
        self.urlPath = [NSString stringWithFormat:@"%llu/photos", self.lastReelID];
    } else {
        self.urlPath = [NSString stringWithFormat:@"featured"];
    }
}

-(void)next:(id)sender {
    self.items = nil;
    self.urlPath = [NSString stringWithFormat:@"featured"];
    [self.tableView reloadData];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [self egoRefreshTableHeaderDidTriggerRefresh:self.refreshHeader];
}

-(void)hideLoadingIndicator
{
    [super hideLoadingIndicator];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 50;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0 && self.items.count > 0) {
        return 1;
    }
    return [super tableView:tableView numberOfRowsInSection:section];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor whiteColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        RAPhotoData *photo = self.items[0][0];
        RAReelHeaderCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"ReelHeaderCell"];
        if (headerCell == nil) {
            headerCell = [[RAReelHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReelHeaderCell"];
            headerCell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        headerCell.photo = photo;
        headerCell.delegate = self;
        return headerCell;
    } else {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && APPDELEGATE.account != nil) {
        RAPhotoData *photo = self.items[0][0];
        RAProfileViewController *profileVC = [RAProfileViewController instanceForAccount:photo.account];
        if (self.parentController) {
            [self.parentController.navigationController pushViewController:profileVC animated:YES];
        } else {
            [self.navigationController pushViewController:profileVC animated:YES];            
        }
    } else {
        [[self.tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
    }
}

-(void)photoClicked:(RAPhotoData *)photo forCell:(id)cell
{
    RAPhotoBrowser *browser = [[RAPhotoBrowser alloc] initWithRAPhotoArray:self.flatArray];
    [browser setInitialPageIndex:[self.flatArray indexOfObject:photo]];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    if (self.parentController) {
        [self.parentController presentModalViewController:nc animated:YES];
    } else {
        [self presentModalViewController:nc animated:YES];
    }
}



@end
