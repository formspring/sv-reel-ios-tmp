//
//  RAPhotosViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/19/12.
//
//

#import "RAPhotosViewController.h"
#import "RAReelData.h"
#import "RAAccountData.h"
#import "RAUtils.h"
#import "RAPhotoData.h"
#import "RAGroupedPhotosCell.h"
#import "RAPhotoActionViewController.h"
#import "RAPhotoBrowser.h"
#import "SVProgressHUD.h"

@implementation RAPhotosViewController
@synthesize items = _items;

-(id)initWithAccount:(RAAccountData *)account Reel:(RAReelData *)reel
{
    if (self = [super init]) {
        self.dataClassName = @"RAPhotoData";
        if (account != nil) {
            self.account = account;
            self.endpoint = ACCOUNT_DETAILS;
            self.urlPath = [NSString stringWithFormat:@"%lld/photos", account.itemID];
        }
        else if (reel != nil) {
            self.reel = reel;
            self.endpoint = REEL;
            self.urlPath = [NSString stringWithFormat:@"%llu/photos", self.reel.itemID];
        } else {
            self.endpoint = REEL;
            self.urlPath = [NSString stringWithFormat:@"featured"];
        }
        self.paginatable = NO;
        self.refreshable = YES;
    }
    return self;
}


+(id)instanceForAccount:(RAAccountData *)account
{
    RAPhotosViewController *instance = [[RAPhotosViewController alloc] initWithAccount:account Reel:nil];
    return instance;
}

+(id)instanceForReel:(RAReelData *)reel
{
    RAPhotosViewController *instance = [[RAPhotosViewController alloc] initWithAccount:nil Reel:reel];
    return instance;
}

-(void)setItems:(NSMutableArray *)items
{
    _items = items;
    NSMutableArray *mItems = [NSMutableArray array];
    [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
       [obj enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx, BOOL *stop) {
           [mItems addObject:obj2];
       }];
    }];
    _flatArray = [NSArray arrayWithArray:mItems];
    if (_flatArray.count > 0) {
        RAPhotoData *photo = _flatArray[0];
        _lastReelID = photo.reel.itemID;
    }
    self.selectedPhotos = [NSMutableArray arrayWithCapacity:[_flatArray count]];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSArray *)transformItemsArray:(NSArray *)array
{
    return [RAUtils groupedPhotosForStyle:array];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (self.reel != nil) {
        self.title = self.reel.name;
        self.trackedViewName = @"Reel";
    }
    else if (self.account.itemID == APPDELEGATE.account.itemID) {
        self.title = @"Me";
        self.trackedViewName = @"MyPhotos";
    } else if (self.account != nil) {
        self.title = self.account.name;
        self.trackedViewName = @"AccountPhotos";
    }
    if (self.parentViewController == [self.parentViewController.navigationController.viewControllers objectAtIndex:0]) {
        self.parentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(toggleEditMode:)];
    }
}

-(void)toggleEditMode:(id)sender
{
    self.editing = !self.editing;
    
    if (self.editing) {
        [self.parentViewController.navigationItem.leftBarButtonItem setTitle:@"Cancel"];
        self.parentViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStyleDone target:self action:@selector(doneEditMode:)];
    } else {
        [self.parentViewController.navigationItem.leftBarButtonItem setTitle:@"Edit"];
        UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings_gear.png"] style:UIBarButtonItemStylePlain target:self.parentViewController action:@selector(showSettings)];
        self.parentViewController.navigationItem.rightBarButtonItem = settingsButton;
    }
}

-(void)doneEditMode:(id)sender
{

    NSMutableArray *ids = [NSMutableArray array];
    NSArray *newItems = [_flatArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        DLog(@"%@", evaluatedObject);
        if ([evaluatedObject selected]) {
            RAPhotoData *p = evaluatedObject;
            [ids addObject:@(p.itemID)];
        }
        return ![evaluatedObject selected];
    }]];
    self.items = [NSMutableArray arrayWithArray:[RAUtils groupedPhotosForStyle:[NSArray arrayWithArray:newItems]]];
    [self toggleEditMode:sender];
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PhotoDeleted" object:ids];
    [[RAApiClient sharedClient] post:PHOTO withPath:@"delete" data:@{@"ids": ids} successBlock:^(NSDictionary *data) {
        [SVProgressHUD showSuccessWithStatus:@"Deleted"];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        [SVProgressHUD showErrorWithStatus:@"Failed to delete"];
        self.items = nil;
        [self fetchDataWithParams:nil];
    }];
}

-(NSMutableDictionary *)paramsForRefresh
{
    NSMutableDictionary *dict = [super paramsForRefresh];
    if (self.account == nil && self.reel == nil) {
        DLog(@"%lld", _lastReelID);
        [dict setObject:[NSString stringWithFormat:@"%lld", _lastReelID] forKey:@"current"];
    }
    return dict;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == self.items.count - 1) {
        return RAGroupedPhotosCellTypeEdgeTop;
    }
    return RAGroupedPhotosCellTypeOther;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.items.count;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *photos = [self.items objectAtIndex:indexPath.row];
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%@_%d_%d", [RAPhotoData class], [[UIDevice currentDevice] orientation], photos.count];
    
    RAGroupedPhotosCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RAGroupedPhotosCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.delegate = self;
        cell.photoSelectable = YES;
        cell.selectable = YES;//self.account.itemID == APPDELEGATE.account.itemID;
    }
    
    if (indexPath.row == 0) {
        cell.cellType = RAGroupedPhotosCellTypeEdgeTop;
    } else if (indexPath.row == self.items.count - 1) {
        cell.cellType = RAGroupedPhotosCellTypeEdgeBottom;
    }else {
        cell.cellType = RAGroupedPhotosCellTypeOther;
    }
    
    cell.photos = photos;
    return cell;
}



-(void)photoClicked:(RAPhotoData *)photo forCell:(id)cell
{
    RAPhotoBrowser *browser = [[RAPhotoBrowser alloc] initWithRAPhotoArray:_flatArray];
    [browser setInitialPageIndex:[_flatArray indexOfObject:photo]];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
}

-(void)photo:(RAPhotoData *)photo selected:(BOOL)selected forCell:(id)cell
{
//    NSIndexPath *path = [self.tableView indexPathForCell:cell];
//    [self.selectedPhotos removeObject:photo];
//    if (selected) {
//        [self.selectedPhotos addObject:photo];
//    }
    photo.selected = selected;
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _flatArray.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _flatArray.count && index > -1) {
        RAPhotoData *photo = _flatArray[index];
        return [MWPhoto photoWithRAPhotoData:photo];
    }
    return nil;
}

- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
    RAPhotoData *photo = _flatArray[index];
    RAPhotoActionViewController *photoAction = [[RAPhotoActionViewController alloc] initWithNibName:@"RAPhotoActionViewController" bundle:nil];
    photoAction.photo = photo;
    return (MWCaptionView *)photoAction.view;
}


@end
