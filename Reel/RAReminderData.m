//
//  RAReminderData.m
//  Reel
//
//  Created by Stephan Soileau on 8/28/12.
//
//

#import "RAReminderData.h"
#import "RAUtils.h"
#import "RAPhotoData.h"

@interface RAReminderData ()

@end

@implementation RAReminderData
@synthesize itemID = _itemID;
@synthesize accountId = _accountId;
@synthesize notified = _notified;

+(RAReminderData *) fromDict:(NSDictionary *)dict {
//    DLog(@"%@",dict);
    RAReminderData *me = [[RAReminderData alloc] init];
    NSDictionary *dictionary = [RAUtils sanitizedDictionary:dict];
    [me setItemID:[[dictionary objectForKey:@"id"] longLongValue]];
    [me setAccountId:[[dictionary objectForKey:@"account_id"] longLongValue]];
    [me setNotified:[[dictionary objectForKey:@"notified"] isEqualToString:@"no"]];
    [me setReel:[RAReelData fromDict:[dictionary objectForKey:@"reel"]]];
    [me setQuestion:[dictionary objectForKey:@"question"]];
    return me;
}
+ (id) fromHttpRequest:(ASIHTTPRequest *)request {
    NSError *e = nil;
    NSDictionary *jsonData = [RAUtils sanitizedDictionary:[NSJSONSerialization JSONObjectWithData: [request responseData] options: NSJSONReadingMutableContainers error: &e]];
    if (!e) {
        NSMutableArray *itemArray = [[NSMutableArray alloc] init];
//        DLog(@"%@", jsonData);
        for (NSDictionary *jsonDict in jsonData[@"matches"]) {
            [itemArray addObject:[RAReminderData fromDict:jsonDict]];
        }
        return itemArray;
    }
    return nil;
  
}
@end
