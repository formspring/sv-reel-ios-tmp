//
//  RACaptionView.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/9/12.
//
//

#import "RACaptionView.h"

@implementation RACaptionView

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if (self.frame.size.height < 50) {
        return YES;
    }
    return point.y < 70 || point.y > (self.frame.size.height - 50);
}

@end
