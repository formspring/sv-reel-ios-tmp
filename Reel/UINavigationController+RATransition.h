//
//  UINavigationController+RATransition.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/9/12.
//
//

#import <UIKit/UIKit.h>

@interface UINavigationController (RATransition)
- (void) pushController: (UIViewController*) controller
         withTransition: (UIViewAnimationTransition) transition;
- (void) presentModalViewController: (UIViewController*) controller
                     withTransition: (UIViewAnimationTransition) transition;
- (void)popControllerWithTransition: (UIViewAnimationTransition) transition;
@end
