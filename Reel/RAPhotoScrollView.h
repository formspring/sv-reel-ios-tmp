//
//  RAPhotoScrollView.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/30/12.
//
//

#import <UIKit/UIKit.h>
#import "RAPaginatingTableViewController.h"

@interface RAPhotoScrollView : UIScrollView

@end
