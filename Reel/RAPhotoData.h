//
//  RAPhotoData.h
//  Reel
//
//  Created by Tim Bowen on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RADataObject.h"
#import "RAReelData.h"

@interface RAPhotoData : NSObject <RADataObject>


+ (id)fromHttpRequest:(ASIHTTPRequest *)request;
+ (id)fromDict:(NSDictionary *)dictionary;
- (void)toggleLike;

@property (strong, nonatomic) RAReelData *reel;
@property (strong, nonatomic) RAAccountData *account;
@property (assign) unsigned long long accountID;
@property (strong, nonatomic) NSDate *created;
@property (strong, nonatomic) NSString *file;
@property (assign) unsigned long long itemID;
@property (assign) unsigned long long reelID;
@property (assign) int commentCount;
@property (assign) int likeCount;
@property (assign) BOOL hasLiked;


@property (strong, nonatomic) NSString *url150;
@property (strong, nonatomic) NSString *url450;
@property (strong, nonatomic) NSString *url600x600;
@property (strong, nonatomic) NSString *url600x600l;
@property (strong, nonatomic) NSString *url300x300;
@property (strong, nonatomic) NSString *url300x300l;
@property (strong, nonatomic) NSString *url150x150l;
@property (strong, nonatomic) NSString *urlOriginal;
@property (strong, nonatomic) NSString *url960max;

@property (strong, nonatomic) NSString *webURL;
@property (assign, nonatomic) BOOL selected;

@end
