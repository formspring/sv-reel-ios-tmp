//
//  RAActivityItemData.h
//  Reel
//
//  Created by Stephan Soileau on 8/15/12.
//
//

#import <Foundation/Foundation.h>
@class RAAccountData;


@interface RAActivityItemData : NSObject
@property (assign) unsigned long long itemID;
@property (assign) unsigned long long accountId;
@property (assign) unsigned long long friendId;
@property (assign) unsigned long long targetId;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSDate *created;
@property (strong, nonatomic) RAAccountData *friendAccount;


+ (RAActivityItemData *)fromDict:(NSDictionary *)dict;
@end
