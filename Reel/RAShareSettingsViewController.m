//
//  RAShareSettingViewController.m
//  Reel
//
//  Created by Ade Olonoh on 9/8/12.
//
//

#import "RAShareSettingsViewController.h"

@interface RAShareSettingsViewController () {
    BOOL _shareSetting;
    BOOL _loading;
}

-(void)setShareSettingState: (UITableViewCell *)shareCell;

@end

@implementation RAShareSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Social Sharing";
    //self.tableView.backgroundColor = COLOR_BEIGE;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:self.tableView.frame];
    self.tableView.backgroundView.backgroundColor = COLOR_BEIGE;
    __weak RAShareSettingsViewController *_self = self;
    [self addSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.textLabel setText:@"Facebook"];
            [_self setShareSettingState:cell];
        }];
    }];
    
    self.footerText = @"Automatically share your likes and comments with friends on Facebook.";
    
     _loading = YES;
     
     [[RAApiClient sharedClient] get:ACCOUNT_SETTINGS successBlock:^(NSDictionary *data) {
         DLog(@"%@", data);
         _loading = false;
         UITableViewCell *shareCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
         NSDictionary *shareSetting = [[data objectForKey:@"share"] objectAtIndex:0];
         _shareSetting = [[shareSetting objectForKey:@"value"] boolValue];
         [_self setShareSettingState:shareCell];
     } errorBlock:^(NSError *error, NSURLRequest *request) {
         RAAlert(@"Error", [error localizedDescription]);
     }];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)setShareSettingState:(UITableViewCell *)shareCell
{
    if (_loading) {
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
                                            initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        shareCell.accessoryView = spinner;
        [spinner startAnimating];
        
    } else {
        UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        [switchView setOn:_shareSetting];
        shareCell.accessoryView = switchView;
        [switchView addTarget:self action:@selector(saveFacebookOpenGrah:) forControlEvents:UIControlEventValueChanged];
    }
}

- (void)saveFacebookOpenGrah: (id)sender
{
    _shareSetting = [sender isOn];
    [[RAApiClient sharedClient] post:ACCOUNT_SETTINGS
                            withData:@{@"setting": @"fb_open_graph",
     @"value": [[NSNumber numberWithBool:_shareSetting] stringValue]}
                        successBlock:^(NSDictionary *data) {
                        } errorBlock:^(NSError *error, NSURLRequest *request) {
                            _shareSetting = !_shareSetting;
                            UITableViewCell *shareCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                            [self setShareSettingState:shareCell];
                        }];
}

@end