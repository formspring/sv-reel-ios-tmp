//
//  UIImage+RAFixOrientation.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/6/12.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (RAFixOrientation)
- (UIImage *)fixOrientation;
@end
