//
//  UINavigationBar+RADropShadow.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/4/12.
//
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (RADropShadow)

-(void) applyDefaultStyle;

@end
