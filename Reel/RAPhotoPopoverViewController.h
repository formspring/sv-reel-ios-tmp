//
//  RAPhotoPopoverViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/30/12.
//
//

#import <UIKit/UIKit.h>
#import "RAPhotoData.h"
#import "RAPhotoImageView.h"
#import "GAITrackedViewController.h"

@interface RAPhotoPopoverViewController : GAITrackedViewController <RAPhotImageViewDelegate>

@property (strong, nonatomic) RAPhotoData *photo;
@property (strong, nonatomic) UIImage *thumb;

+(id)instanceForPhoto:(RAPhotoData *)photo withThumbnail:(UIImage *)image;

@end
