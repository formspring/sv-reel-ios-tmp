//
//  RACommentCell.h
//  Reel
//
//  Created by Tim Bowen on 8/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RAActivityTableCell.h"

@class RACommentData;


@protocol RACommentCellDelegate <NSObject>

- (void)showProfile:(RAAccountData *)account;

@end

@interface RACommentCell : UITableViewCell

@property (strong, nonatomic) RACommentData *commentData;

@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *commentTextLabel;
@property (strong, nonatomic) id<RACommentCellDelegate> delegate;

+ (UIFont *)nameLabelFont;
+ (UIFont *)commentLabelFont;

@end
