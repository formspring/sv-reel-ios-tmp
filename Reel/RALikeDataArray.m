//
//  RALikeDataArray.m
//  Reel
//
//  Created by Tim Bowen on 9/4/12.
//
//

#import "RALikeDataArray.h"
#import "RALikeData.h"

@implementation RALikeDataArray


+ (NSMutableArray *)fromHttpRequest:(ASIHTTPRequest *)request {
    DLog(@"%@", [[request url] absoluteString]);
    NSError *e = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData: [request responseData] options: NSJSONReadingMutableContainers error: &e];
    if (!e) {
        return [RALikeDataArray fromDict:jsonData];
    }
    return nil;
}

+ (NSMutableArray *) fromDict:(NSDictionary *)dict
{
    NSMutableArray *feedItemArray = [[NSMutableArray alloc] init];
    NSArray *matches = dict[@"matches"];
    for (NSDictionary *jsonDict in matches) {
        [feedItemArray addObject:[RALikeData fromDict:jsonDict]];
    }
    return feedItemArray;
}

@end
