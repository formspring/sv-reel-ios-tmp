//
//  RAPhotosViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 10/19/12.
//
//

#import <UIKit/UIKit.h>
#import "RAPaginatingTableViewController.h"
#import "RAGroupedPhotosCell.h"
@class RAReelData;
@class RAAccountData;

@interface RAPhotosViewController : RAPaginatingTableViewController <RAGroupedPhotoCellDelegate>

@property(strong) RAReelData *reel;
@property(strong) RAPhotoPopoverViewController *photoController;
@property(strong, nonatomic) RAAccountData *account;
@property(assign, nonatomic) long long lastReelID;
@property(strong, nonatomic) NSArray *flatArray;
@property(strong, nonatomic) NSMutableArray *selectedPhotos;

-initWithAccount:(RAAccountData*)account Reel:(RAReelData*)reel;
+(id)instanceForReel:(RAReelData *)reel;
+(id)instanceForAccount:(RAAccountData*)account;


@end
