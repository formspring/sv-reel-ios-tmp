//
//  RALoginViewController.h
//  Reel
//
//  Created by Tim Bowen on 10/3/12.
//
//

#import <UIKit/UIKit.h>
#import "RASignupViewController.h"
@interface RALoginViewController : RASignupViewController <UITextFieldDelegate>

//- (IBAction)loginClicked:(id)sender;

@property (assign) BOOL viewConfigured;

@end
