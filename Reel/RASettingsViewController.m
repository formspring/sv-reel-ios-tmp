//
//  RASettingsViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/17/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "RASettingsViewController.h"
#import "RANotificationSettingsViewController.h"
#import "RAShareSettingsViewController.h"
#import "SVProgressHUD.h"
#import "SVWebViewController.h"
#import "RAProfileSettingsTableViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface RASettingsViewController ()
@property (strong, nonatomic) NSMutableArray *notifications;
@end

@implementation RASettingsViewController
@synthesize notifications = _notifications;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Settings";
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:self.tableView.frame];
    self.tableView.backgroundView.backgroundColor = COLOR_BEIGE;
    __weak RASettingsViewController *_self = self;
    
    [self addSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
        [section setTitle:@"Account"];
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.cellStyle = UITableViewCellStyleDefault;
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell.textLabel setText:@"Profile Settings"];
        } whenSelected:^(NSIndexPath *indexPath) {
            RAProfileSettingsTableViewController *profileSettings = [[RAProfileSettingsTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
            profileSettings.title = @"Profile Settings";
            [_self.navigationController pushViewController:profileSettings animated:YES];
            
        }];

        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.cellStyle = UITableViewCellStyleDefault;
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell.textLabel setText:@"Social Sharing"];
        } whenSelected:^(NSIndexPath *indexPath) {
            if([[APPDELEGATE account] hasFacebook]){
                RAShareSettingsViewController *shareSettings = [[RAShareSettingsViewController alloc] initWithStyle:UITableViewStyleGrouped];
                [_self.navigationController pushViewController:shareSettings animated:YES];
            } else {
                [SVProgressHUD show];
                [FBSession openActiveSessionWithPermissions:[NSArray arrayWithObjects:@"publish_actions", @"email", @"user_birthday", nil]
                                               allowLoginUI:YES
                                          completionHandler:^(FBSession *session,
                                                              FBSessionState status,
                                                              NSError *error) {
                                              if (session.isOpen) {
                                                   NSDictionary *data = [NSDictionary dictionaryWithObject:session.accessToken forKey:@"facebook_token"];
                                                  [[RAApiClient sharedClient] post:ACCOUNT_DETAILS withPath:[NSString stringWithFormat:@"%llu", [[APPDELEGATE account] itemID]]  data:data
                                                                      successBlock: ^(NSDictionary *data) {
                                                                          [SVProgressHUD showSuccessWithStatus:@"OK"];
                                                                          NSLog(@"%@", data);
                                                                          [APPDELEGATE updateAccountData:data];
                                                  } errorBlock:^(NSError *error, NSURLRequest *request) {
                                                      [SVProgressHUD showErrorWithStatus:@""];
                                                      RAAlert(@"Error", @"We could not connect your Facebook account.");
                                                  }];
                                              } else {
                                                  [SVProgressHUD showErrorWithStatus:@""];
                                                  RAAlert(@"Error",
                                                          @"Facebook connect failed");
                                                  
                                                  [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"App"
                                                                                                     withAction:@"Error"
                                                                                                      withLabel:@"Facebook Connect Settings"
                                                                                                      withValue:nil];
                                              }
                                          }];
            }
        }];
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.cellStyle = UITableViewCellStyleDefault;
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell.textLabel setText:@"Notifications"];
        } whenSelected:^(NSIndexPath *indexPath) {
            RANotificationSettingsViewController *notificationSettings = [[RANotificationSettingsViewController alloc] initWithStyle:UITableViewStyleGrouped];
            [_self.navigationController pushViewController:notificationSettings animated:YES];
        }];
    }];
    
    [self addSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
        [section setTitle:@"App"];
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.cellStyle = UITableViewCellStyleValue1;
            [cell.textLabel setText:@"Version"];
            [cell.detailTextLabel setText:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }];
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.cellStyle = UITableViewCellStyleValue1;
            
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell.textLabel setText:@"Licenses"];
        } whenSelected:^(NSIndexPath *indexPath) {
            SVWebViewController *webViewController = [[SVWebViewController alloc] initWithAddress:@"http://www.makereel.com/site/ios_licenses"];
            webViewController.hidesBottomBarWhenPushed = YES;
            [_self.navigationController pushViewController:webViewController animated:YES];
        }];
    }];
    
    [self addSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.cellStyle = UITableViewCellStyleDefault;
            // Move logout to bottom
            [cell.textLabel setText:@"Logout"];
        } whenSelected:^(NSIndexPath *indexPath) {
            [_self logout];
        }];
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[[GAI sharedInstance] defaultTracker] trackView:@"Settings"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) logout
{
    [SVProgressHUD show];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"session"];
    [defaults synchronize];
    [[FBSession activeSession] closeAndClearTokenInformation];
    [APPDELEGATE showIntroView:YES];
    [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"App"
                                                       withAction:@"Logout"
                                                        withLabel:nil
                                                        withValue:nil];
    [[RAApiClient sharedClient] delete:SESSION successBlock:^(NSDictionary *data) {
        APPDELEGATE.account = nil;
        [SVProgressHUD showSuccessWithStatus:nil];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        APPDELEGATE.account = nil;
        [SVProgressHUD dismiss];
        RAAlert(@"Error", @"Unable to logout");
    }];
}

@end
