//
//  RADataObject.h
//  Reel
//
//  Created by Stephan Soileau on 8/14/12.
//
//

#import <Foundation/Foundation.h>
#import "ASIHttpRequest.h"

@protocol RADataObject <NSObject>

//NOT OPTIONAL
@required

//Everyone must have to have itO
+ (id) fromDict:(NSDictionary *)dict;

@optional

//"Arrays" of items don't use this - it's for pagination.
@property (assign) unsigned long long itemID;

//Some objects that always come down in arrays don't use this
+ (id) fromHttpRequest:(ASIHTTPRequest *)request;


@end
