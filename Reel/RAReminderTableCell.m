//
//  RAReminderTableCell.m
//  Reel
//
//  Created by Stephan Soileau on 8/28/12.
//
//

#import "RAReminderTableCell.h"
#import "RAReminderData.h"
#import "RAPhotoData.h"


@implementation RAReminderTableCell
@synthesize reminder = _reminder;
@synthesize reminderPhoto = _reminderPhoto;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.reminderPhoto = [[UIImageView alloc] init];        
        [self.contentView addSubview:self.reminderPhoto];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
   // CGRect contentFrame = self.contentView.frame;
    static float padding = 5;
    self.reminderPhoto.frame = CGRectMake(padding, padding, 40, 40);
    self.reminderPhoto.contentMode = UIViewContentModeScaleAspectFit;
    self.textLabel.center = CGPointMake(self.textLabel.center.x + self.frame.size.height - padding, self.textLabel.center.y);
    int textWidth = (int)self.frame.size.width - self.reminderPhoto.frame.size.width - self.accessoryView.frame.size.width - 5 * padding;
    
    self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x,
                                      self.textLabel.frame.origin.y,
                                      textWidth,
                                      self.textLabel.frame.size.height);
    self.textLabel.adjustsFontSizeToFitWidth = NO;
    self.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    self.detailTextLabel.center = CGPointMake(self.detailTextLabel.center.x + self.frame.size.height - padding, self.detailTextLabel.center.y);
    self.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
}


- (NSURL *) getPreviewUrl {
    return [NSURL URLWithString:[[self.reminder.reel.preview objectAtIndex:0] url150]];
}


- (int) getPadding {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
       return 5;
    } else {
        return 10;
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.reminder = nil;    
    self.reminderPhoto.image = nil;
}


@end
