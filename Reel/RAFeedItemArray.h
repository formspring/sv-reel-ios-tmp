//
//  RAFeedItemData.h
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RADataObject.h"

@interface RAFeedItemArray : NSObject <RADataObject>

+ (NSMutableArray *) fromHttpRequest:(ASIHTTPRequest *)request;
+ (NSMutableArray *) fromDict:(NSDictionary *)dict;

@end
