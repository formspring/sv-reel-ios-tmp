//
//  RALikeData.m
//  Reel
//
//  Created by Tim Bowen on 9/4/12.
//
//

#import "RALikeData.h"
#import "RAAccountData.h"
#import "RAUtils.h"

//Example JSON response:

/*
 {
 //Matches is array of like objects
 matches =     (
 {
 "account_id" = 100136;
 created = "2012-09-04 22:02:04";
 id = 20658;
 liker =             {
 id = 100137;
 name = "Timothy Bowen";
 "photo_urls" =                 {
 150x150 = "http://s3.amazonaws.com/photos.dev.reel/account/20120820/5032ebd138b6b0.08898993.150x150.jpg";
 50x50 = "http://s3.amazonaws.com/photos.dev.reel/account/20120820/5032ebd138b6b0.08898993.50x50.jpg";
 original = "http://s3.amazonaws.com/photos.dev.reel/account/20120820/5032ebd138b6b0.08898993.original.jpg";
 };
 username = timfbowen81716;
 };
 "liker_id" = 100137;
 "photo_id" = 183054;
 }
 );
 }
*/


@implementation RALikeData

@synthesize itemID = _itemID;
@synthesize liker = _liker;
@synthesize created = _created;
@synthesize photoID = _photoID;


+ (id) fromDict:(NSDictionary *)dict {
    RALikeData *like = [[RALikeData alloc] init];
    like.itemID = [[dict objectForKey:@"id"] longLongValue];
    like.liker = [RAAccountData fromDict:[dict objectForKey:@"liker"]];
    like.created = [[RAUtils getDateFormatter] dateFromString:[dict objectForKey:@"created"]];
    like.photoID = [[dict objectForKey:@"photo_id"]longLongValue];
    return like;
}

@end
