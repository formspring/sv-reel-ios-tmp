//
//  RAFriendActivityViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 10/11/12.
//
//

#import <UIKit/UIKit.h>
#import "RAPaginatingTableViewController.h"
#import "MWPhotoBrowser.h"
#import "RAGroupedPhotosCell.h"

@interface RAFriendActivityViewController : RAPaginatingTableViewController <MWPhotoBrowserDelegate, RAGroupedPhotoCellDelegate>

@end
