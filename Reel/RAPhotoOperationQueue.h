//
//  RAPhotoOperationQueue.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//
//

#import <Foundation/Foundation.h>
#import "RAReelData.h"
@class RAPhotoData;
@class AFJSONRequestOperation;

@interface RAPhotoOperationQueue : NSOperationQueue <ASIHTTPRequestDelegate, ASIProgressDelegate>

@property(strong, nonatomic) UIImage *photo;
@property(strong, nonatomic) NSString *photoID;
@property(strong, nonatomic) AFJSONRequestOperation *photoUploadRequest;
@property(strong, nonatomic) AFJSONRequestOperation *photoSaveRequest;
@property(strong, nonatomic) NSDictionary *result;
@property(strong, nonatomic) NSMutableDictionary *payload;
@property(assign, nonatomic) UIBackgroundTaskIdentifier backgroundUpdateTask;
@property(strong, nonatomic) NSURL *assetURL;
@property(assign, nonatomic, getter = isFromCamera) BOOL fromCamera;
@property(strong, nonatomic) RAPhotoData *mockPhotoData;
@property(assign, nonatomic) float progress;
@property(strong, nonatomic) NSError *error;

+(RAPhotoOperationQueue*)instanceForPhoto:(UIImage *)photo fromCamera:(BOOL)fromCamera;
-(BOOL)savePhotoToReel:(RAReelData *)reel comment:(NSString*)comment share:(BOOL)share;
- (void) beingBackgroundTask;
- (void) endBackgroundTask;
-(id)initWithPhoto:(UIImage *)photo;
@end
