//
//  RAPhotoData.m
//  Reel
//
//  Created by Tim Bowen on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

/*
//Example JSON data:
 {
 "account_id" = 100;
 created = "<null>";
 file = "photo/20120816/502d938a816893.62683577.original.jpg";
 id = 92690;
 "reel_id" = 99;
 urls =     {
    150x150 = "https://s3.amazonaws.com/photos.dev.reel/photo/20120816/502d938a816893.62683577.150x150.jpg";
    450x450 = "https://s3.amazonaws.com/photos.dev.reel/photo/20120816/502d938a816893.62683577.450x450.jpg";
    original = "https://s3.amazonaws.com/photos.dev.reel/photo/20120816/502d938a816893.62683577.original.jpg";
 };
 
 */


#import "RAPhotoData.h"
#import "RAUtils.h"


@implementation RAPhotoData

@synthesize accountID = _accountID;
@synthesize created = _created;
@synthesize file = _file;
@synthesize itemID = _itemID;
@synthesize reelID = _reelID;
@synthesize url150 = _url150;
@synthesize url450 = _url450;
@synthesize urlOriginal = _urlOriginal;
@synthesize url600x600 = _url600x600;
@synthesize url150x150l = _url150x150l;
@synthesize url600x600l = _url600x600l;
@synthesize url300x300 = _url300x300;
@synthesize url300x300l = _url300x300l;


+ (id)fromHttpRequest:(ASIHTTPRequest *)request {
    
    NSError *e;
    
    id jsonData = [NSJSONSerialization JSONObjectWithData: [request responseData] options: NSJSONReadingMutableContainers error: &e];
    
    if (!e) {

        
        return [RAPhotoData fromDict:jsonData];
    } 
    
    NSLog(@"JSON error :%@\n %@ %@ %u", [e localizedDescription], [self class], [[request url]absoluteString], [request responseStatusCode]);
    
    return nil;
}

+ (id)fromDict:(NSDictionary *)dictionary {
    
    NSMutableDictionary *jsonData = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    for(NSString *key in [jsonData allKeys]) {
        if([jsonData objectForKey:key] == [NSNull null]){
            [jsonData removeObjectForKey:key];
        }
    }
    RAPhotoData *data = [[RAPhotoData alloc] init];
    data.account = [RAAccountData fromDict:[dictionary objectForKey:@"account"]];
    data.reel = [RAReelData fromDict:[dictionary objectForKey:@"reel"]];

    data.accountID = [[jsonData objectForKey:@"account_id"] longLongValue];
    data.created = [[RAUtils getDateFormatter] dateFromString:[jsonData objectForKey:@"created"]];
    data.file = [jsonData objectForKey:@"file"];
    data.itemID = [[jsonData objectForKey:@"id"] longLongValue];
    data.reelID = [[jsonData objectForKey:@"reel_id"] longLongValue];
    NSDictionary *urls = [jsonData objectForKey:@"urls"];
    data.url150 = [urls objectForKey:@"150x150"];
    data.url450 = [urls objectForKey:@"450x450"];
    data.urlOriginal = [urls objectForKey:@"original"];
    data.url600x600 = [urls objectForKey:@"600x600"];
    data.url600x600l = [urls objectForKey:@"600x600l"];
    data.url300x300 = [urls objectForKey:@"300x300"];
    data.url300x300l = [urls objectForKey:@"300x300l"];
    data.url150x150l = [urls objectForKey:@"150x150l"];
    data.url960max = [urls objectForKey:@"960max"];
    
    data.webURL = [jsonData objectForKey:@"web_url"];
    
    data.commentCount = [[dictionary objectForKey:@"comment_count"] intValue];
    data.likeCount = [[dictionary objectForKey:@"like_count"] longLongValue];
    data.hasLiked = [[dictionary objectForKey:@"has_liked"] boolValue];

    return data;
}

-(void)toggleLike
{
    if(self.hasLiked) {
        self.hasLiked = NO;
        self.likeCount -= 1;
        
        NSDictionary *data = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%lld", self.itemID] forKey:@"photo_id"];
        
        
        [[RAApiClient sharedClient] delete:PHOTO_LIKE withPath:nil queryParams:data successBlock:^(NSDictionary *data) {
            NSLog(@"Success block of delete like");
            [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"Photo"
                                                               withAction:@"Unlike"
                                                                withLabel:nil
                                                                withValue:nil];
            
        } errorBlock:^(NSError *error, NSURLRequest *request) {
            NSLog(@"Error block woe is me");
            self.hasLiked = YES;
            self.likeCount += 1;
            
        }];
        
    } else {
        self.hasLiked = YES;
        self.likeCount += 1;
        
        NSDictionary *params = [NSDictionary dictionaryWithObject:[NSNumber numberWithUnsignedLongLong:self.itemID] forKey:@"photo_id"];
        
        
        [[RAApiClient sharedClient] post:PHOTO_LIKE
                                withData:params
                            successBlock:^(NSDictionary *data) {
                                NSLog(@"Success block bully");
                                [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"Photo"
                                                                                   withAction:@"Like"
                                                                                    withLabel:nil
                                                                                    withValue:nil];

                                
                            } errorBlock:^(NSError *error, NSURLRequest *request) {
                                NSLog(@"Error block woe is me");
                                NSLog(@"%@", [error localizedDescription]);
                                self.hasLiked = NO;
                                self.likeCount -= 1;
                            }];
    }
}

@end
