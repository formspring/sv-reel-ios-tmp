//
//  RAFeedItemData.m
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RAFeedItemData.h"
#import "RAReelData.h"
#import "RAAccountData.h"
#import "RAUtils.h"
#import "RAPhotoData.h"

@implementation RAFeedItemData

@synthesize commentCount = _commentCount;
@synthesize created = _created;
@synthesize imageUrlString = _imageUrlString;
@synthesize itemID = _feedID;
@synthesize likeCount = _likeCount;
@synthesize reel = _reel;
@synthesize url150 = _url150;
@synthesize url150l = _url150l;
@synthesize url300 = _url300;
@synthesize url300l = _url300l;
@synthesize url600 = _url600;
@synthesize url600l = _url600l;
@synthesize urlOriginal = _urlOriginal;
@synthesize hasLiked = _hasLiked;
@synthesize photo = _photo;

@synthesize account;

+ (RAFeedItemData *)fromDict:(NSDictionary *)dictionary {
    NSMutableDictionary *dict = [RAUtils sanitizedDictionary:dictionary];
    RAFeedItemData* feedItem= [[RAFeedItemData alloc] init];
    feedItem.account = [RAAccountData fromDict:[dict objectForKey:@"account"]];
    feedItem.commentCount = [[dict objectForKey:@"comment_count"] intValue];
    feedItem.created = [[RAUtils getDateFormatter] dateFromString:[dict objectForKey:@"created"]];    
    feedItem.imageUrlString = [dict objectForKey:@"file"];
    feedItem.itemID = [[dict objectForKey:@"id"] longLongValue];
    feedItem.likeCount = [[dict objectForKey:@"like_count"] longLongValue];
    feedItem.reel = [RAReelData fromDict:[dict objectForKey:@"reel"]];
    feedItem.hasLiked = [[dict objectForKey:@"has_liked"] boolValue];
    
    feedItem.url150 = [[dict objectForKey:@"urls"] objectForKey:@"150x150"];
    feedItem.url150l = [[dict objectForKey:@"urls"] objectForKey:@"150x150l"];
    feedItem.url300 = [[dict objectForKey:@"urls"] objectForKey:@"300x300"];
    feedItem.url300l = [[dict objectForKey:@"urls"] objectForKey:@"300x300l"];
    feedItem.url600 = [[dict objectForKey:@"urls"] objectForKey:@"600x600"];
    feedItem.url600l = [[dict objectForKey:@"urls"] objectForKey:@"600x600l"];
    feedItem.urlOriginal = [[dict objectForKey:@"urls"] objectForKey:@"original"];
    feedItem.photo = [RAPhotoData fromDict:dictionary];
    
    return feedItem;
}

@end
