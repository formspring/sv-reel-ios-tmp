//
//  RASignupViewController.h
//  Reel
//
//  Created by Tim Bowen on 10/2/12.
//
//

#import <UIKit/UIKit.h>
#import "RAAccountInfoTableCell.h"
#import "RACamHolderViewController.h"


@interface RASignupViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RAAccountInfoDelegate, RACamHolderViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *signupInfo;
@property (nonatomic, weak) IBOutlet UIButton *signupButton;
@property (nonatomic, weak) IBOutlet UIButton *facebookButton;
@property (nonatomic, weak) IBOutlet UILabel *orLabel;


- (void)signup;
- (void)accountInfoReady:(id)accountInfo withType:(NSString *)type atIndex:(int)index;
- (IBAction)fbTouched:(id)sender;
- (IBAction)signupTouched:(id)sender;

@end
