//
//  RAReelData.m
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


 /*
 //Example JSON data:
  {
    "account_id" = 79;
    created = "2012-08-11 23:42:11";
    id = 300;
    "last_updated" = "2012-08-12 14:28:57";
    name = "exclamation magnums livening blots ";
    "reminder_settings" = on;
  }
 */

#import "RAPhotoData.h"
#import "RAUtils.h"

@implementation RAReelData

@synthesize accountID = _accountID;
@synthesize itemID = _reelID;
@synthesize name = _name;
@synthesize reminderSettings = _reminderSettings;
@synthesize created = _created;
@synthesize lastUpdated = _lastUpdated;
@synthesize photoCount = _photoCount;
@synthesize preview = _preview;

+ (id)fromDict:(NSDictionary *)dict {
    RAReelData *reelData = [[RAReelData alloc] init];
    reelData.accountID = [[dict objectForKey:@"account_id"] longLongValue];
    reelData.itemID = [[dict objectForKey:@"id"] longLongValue];
    reelData.name = [dict objectForKey:@"name"];
    reelData.webURL = [dict objectForKey:@"web_url"];


    NSDateFormatter *dateFormatter = [RAUtils getDateFormatter];
    if (![[dict objectForKey:@"created"] isEqual:[NSNull null]]) {
        reelData.created = [dateFormatter dateFromString:[dict objectForKey:@"created"]];        
    }
    if (![[dict objectForKey:@"last_updated"] isEqual:[NSNull null]]) {
        reelData.lastUpdated = [dateFormatter dateFromString:[dict objectForKey:@"last_updated"]];
    }
    if (![[dict objectForKey:@"reminder_settings"] isEqual:[NSNull null]]) {
        //Ade to update API to return 1 or 0
        reelData.reminderSettings = [[dict objectForKey:@"reminder_settings"] boolValue];        
    }
    reelData.photoCount = [[dict objectForKey:@"photo_count"] intValue];
    if (NILIFNULL(dict[@"photo_preview"])) {
        NSArray *matches = dict[@"photo_preview"];
        NSMutableArray *preview = [NSMutableArray arrayWithCapacity:[matches count]];
        [matches enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [preview addObject:[RAPhotoData fromDict:obj]];
        }];
        reelData.preview = [NSArray arrayWithArray:preview];
    }
    return reelData;
}

//Unused
+ (id)fromHttpRequest:(ASIHTTPRequest *)request {
    return nil;
}

@end
