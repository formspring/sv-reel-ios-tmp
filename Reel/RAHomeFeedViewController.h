//
//  RAHomeFeedViewController.h
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequestDelegate.h"
#import "RAFeedItemCell.h"
#import "RAPaginatingTableViewController.h"
#import "RACamHolderViewController.h"


@interface RAHomeFeedViewController : RAPaginatingTableViewController <ASIHTTPRequestDelegate, RAFeedItemCellDelegate, RACamHolderViewDelegate>

@property (strong, nonatomic) NSMutableArray *uploadQueue;
@property (assign) BOOL loadingMore;
@property (strong, nonatomic) RACamHolderViewController *camHolder;

@end