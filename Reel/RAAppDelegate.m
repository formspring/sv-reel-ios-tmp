//
//  RAAppDelegate.m
//  Reel
//
//  Created by Tim Bowen on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RAAppDelegate.h"
#import "RAHomeFeedViewController.h"
#import "RANotificationTableViewController.h"
#import "RANotificationDetailController.h"
#import "RAMeTableViewController.h"
#import "RAMeDetailViewController.h"
#import "RAReminderViewController.h"
#import "RAIntroViewController.h"
#import "RAAddReelViewController.h"
#import "RASavePhotoViewController.h"
#import "RAThumbnailGridViewController.h"
#import "RAReelViewController.h"
#import "RAProfileViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SVProgressHUD.h"
#import "Appirater.h"
#import "RAPaginatingTableViewController.h"
#import "GAI.h"
#import "UAirship.h"
#import "UAPush.h"
#import "RAUtils.h"
#import "RACamHolderViewController.h"
#import "Reachability.h"
#import "RAReminderData.h"
#import "RAForgotPasswordViewController.h"
#import "AFNetworking.h"
#import "RAFriendActivityViewController.h"
#import "RAActivityViewController.h"
#import "RAExploreViewController.h"

#define NOTIFICATION_TYPE_PHOTO_LIKE @"photo_like"
#define NOTIFICATION_TYPE_PHOTO_COMMENT @"photo_comment"
#define NOTIFICATION_TYPE_REMINDER @"photo_reminder"
#define NOTIFICATION_TYPE_FOLLOW @"follow"
#define NOTIFICATION_TYPE_FRIEND_REEL @"friend_reel"

@interface RAMockCamControllerForTabBar : UIViewController @end

@implementation UITabBarController (iOS6OrientationFix)

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.selectedViewController supportedInterfaceOrientations];
}

@end

@implementation UINavigationController (iOS60OrientationFix)

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

@end

@implementation RAMockCamControllerForTabBar @end


@interface RAAppDelegate () {
    NSMutableDictionary *_takeoffOptions;
    
}
-(void)customizeApplicationApprearance;
@end

@implementation RAAppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize tabbarController = _tabbarController;
@synthesize introViewController = _introViewController;
@synthesize showCameraActionSheet;
@synthesize apiClient = _apiClient;
@synthesize account = _account;
@synthesize lastTabbarIndex = _lastTabbarIndex;
@synthesize photoQueueArray = _photoQueueArray;
@synthesize UUID = _UUID;


#define CAMERA_TAG 44001

- (NSArray *)getViewControllersiPhone {
    UIEdgeInsets tabbarInset = UIEdgeInsetsMake(5, 0, -5, 0);
    
    //Home
    RAHomeFeedViewController *homeFeedController = [[RAHomeFeedViewController alloc] initWithStyle:UITableViewStylePlain andEndpoint:HOME_FEED forClass:@"RAPhotoData"];
    homeFeedController.title = @"Home";
    UINavigationController *homeNav = [[UINavigationController alloc]initWithRootViewController:homeFeedController];
    homeNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[UIImage imageNamed:@"tabbar_feed.png"] tag:19];
    [homeNav.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_feed_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_feed.png"]];
    homeNav.tabBarItem.imageInsets = tabbarInset;

    
    //Notifications
    RAActivityViewController *activityViewController = [[RAActivityViewController alloc] initWithNibName:@"RAProfileViewController" bundle:nil];
    activityViewController.title = @"Activity";
    UINavigationController *notificationNav = [[UINavigationController alloc] initWithRootViewController:activityViewController];
    notificationNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[UIImage imageNamed:@"tabbar_notifications.png"] tag:-1];
    [notificationNav.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_notifications_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_notifications.png"]];
    notificationNav.tabBarItem.imageInsets = tabbarInset;
    
    //Camera
//    RACamHolderViewController *avc = [[RACamHolderViewController alloc] init];
    RAMockCamControllerForTabBar *avc = [[RAMockCamControllerForTabBar alloc] init];
    UINavigationController *imageNav = [[UINavigationController alloc] initWithRootViewController:avc];
    imageNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[UIImage imageNamed:@"tabbar_camera.png"] tag:CAMERA_TAG];
    [imageNav.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_camera.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_camera.png"]];
    imageNav.tabBarItem.imageInsets = tabbarInset;
    
    //Reminder
    RAExploreViewController *featured = [[RAExploreViewController alloc] init];
    featured.title = @"Explore";
    UINavigationController *reminderNav = [[UINavigationController alloc] initWithRootViewController:featured];
    reminderNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[UIImage imageNamed:@"tabbar_explore.png"] tag:-1];
    [reminderNav.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_explore_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_explore.png"]];
    reminderNav.tabBarItem.imageInsets = tabbarInset;
    
    //Me
    RAProfileViewController *profileVC = [RAProfileViewController instanceForAccount:self.account];
    profileVC.title = @"Me";
    UINavigationController *meNav = [[UINavigationController alloc] initWithRootViewController:profileVC];
    meNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"" image:[UIImage imageNamed:@"tabbar_profile.png"] tag:-1];
    [meNav.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_profile_selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_profile.png"]];
    meNav.tabBarItem.imageInsets = tabbarInset;
    
    return [NSArray arrayWithObjects:homeNav, notificationNav, imageNav, reminderNav, meNav, nil];
} 

- (NSArray *)getViewControllersiPad {
    
    //Home
    RAHomeFeedViewController *homeFeedController = [[RAHomeFeedViewController alloc] init];
    homeFeedController.title = @"Home";
    
    
    //Notifications
    //This guy is going to be a split view for pimpness
    RANotificationTableViewController *notificationMasterViewController = [[RANotificationTableViewController alloc] init];
    RANotificationDetailController *notificationDetailViewController = [[RANotificationDetailController alloc] init];
    UISplitViewController *notificationSplitViewController = [[UISplitViewController alloc] init];
    [notificationSplitViewController setViewControllers:[NSArray arrayWithObjects:notificationMasterViewController, notificationDetailViewController, nil]];
    notificationSplitViewController.title = @"Notifications";
    
    
    
    //Reminder
    RAReminderViewController *reminderVC = [[RAReminderViewController alloc] initWithStyle:UITableViewStylePlain];
    reminderVC.title = @"Reminders";
    
    //Me
    //Another pimpin split view
    RAMeTableViewController *meMasterViewController = [[RAMeTableViewController alloc] init];
    RAMeDetailViewController *meDetailViewController = [[RAMeDetailViewController alloc] init];
    UISplitViewController *meSplitViewController = [[UISplitViewController alloc] init];
    meSplitViewController.viewControllers = [NSArray arrayWithObjects:meMasterViewController, meDetailViewController, nil];
    meSplitViewController.title = @"Me";
    
    return [NSArray arrayWithObjects:homeFeedController, notificationSplitViewController, reminderVC, meSplitViewController, nil];
}

//Camera, Feed, Notifications, Me
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initializeAccount];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.apiClient = [RAApiClient sharedClient];
    self.photoQueueArray = [NSMutableArray array];
    // Override point for customization after application launch.

    if (!self.account) {
        [self showIntroView:NO];
    } else {
        [self showLoggedInAppView];
    }
    [self customizeApplicationApprearance];
    [self.window makeKeyAndVisible];
    [TestFlight takeOff:@"c935ec0be0b6f99f28a49f0a05c62053_MTI2MTQ4MjAxMi0wOC0yOCAxNjowODo1MC43NTk4NjI"];
    
    //Init Airship launch options
    NSMutableDictionary *takeOffOptions = [[NSMutableDictionary alloc] init];
    [takeOffOptions setValue:launchOptions forKey:UAirshipTakeOffOptionsLaunchOptionsKey];
    
    // Create Airship singleton that's used to talk to Urban Airship servers.
    // Please populate AirshipConfig.plist with your info from http://go.urbanairship.com
    [UAirship takeOff:takeOffOptions];
    
    
    // Register for notifications
    [[UAPush shared]
     registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert)];
    
    
    if([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]) {
        self.notificationDict = [[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey] objectForKey:@"reel_data"];
    }
    
    [GAI sharedInstance].debug = NO;
    [GAI sharedInstance].dispatchInterval = 60;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    __unused id<GAITracker> ga = [[GAI sharedInstance] trackerWithTrackingId:@"UA-11737621-9"];

    [Appirater setAppId:@"559515561"];
    [Appirater setDaysUntilPrompt:5];
    [Appirater setUsesUntilPrompt:10];
    [Appirater setSignificantEventsUntilPrompt:3];
    [Appirater setTimeBeforeReminding:5];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];
    
     [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];

    return YES;
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Updates the device token and registers the token with UA
    NSLog(@"This is a thing");
    [[UAPush shared] registerDeviceToken:deviceToken];
    NSString *tokenString = [deviceToken description];
    tokenString = [tokenString substringWithRange:NSMakeRange(1, [tokenString length]-1)];
    NSLog(@"%@", tokenString);
    NSLog(@"%@", deviceToken);
    //If it fails wat do?
    [[NSUserDefaults standardUserDefaults] setObject:tokenString forKey:@"UUID"];
    self.UUID = tokenString;
    if (self.account != nil) {
        [self registerUUID];
    }
}


- (void)registerUUID {
    NSString *tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"];
    if(tokenString) {
    
        [[RAApiClient sharedClient] post:REGISTER_UUID withData:[NSDictionary dictionaryWithObject:tokenString forKey:@"device_token"] successBlock:^(NSDictionary *data) {
            DLog(@"Success! %@", data);
        }errorBlock:^(NSError *error, NSURLRequest *request) {
            DLog(@"You fail! %@", [error localizedDescription])
        }];
    
    }
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state != UIApplicationStateActive) {
        NSMutableDictionary *pushDict = [NSMutableDictionary dictionaryWithDictionary:userInfo];
        NSLog(@"push dict: %@", pushDict);
        self.notificationDict = [pushDict objectForKey:@"reel_data"];
    }
}


-(void)customizeApplicationApprearance {
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:NO];
    UIImage *barBG = [[UIImage imageNamed:@"topbar.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    [[UINavigationBar appearance] setBackgroundImage:barBG forBarMetrics:UIBarMetricsDefault];
    [[UIToolbar appearance] setBackgroundImage:barBG forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:COLOR_PLUM];
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], UITextAttributeTextColor,
      [UIColor blackColor], UITextAttributeTextShadowColor,
      nil]];
    [[UISegmentedControl appearance] setTintColor:UIColorFromRGB(0xE0D9C3)];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                   UITextAttributeTextColor: UIColorFromRGB(0x6D6656),
                             UITextAttributeTextShadowColor: [UIColor whiteColor],
                            UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 1)]}
                                                   forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setBackgroundImage:[UIImage imageNamed:@"tagbg.png"]
                                               forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:[UIImage imageNamed:@"tagbg_selected.png"]
                                               forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
}

- (void)initializeAccount {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"session"];
    if (data != nil) {
        NSDictionary *session = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        //DLog(@"%@", session);
        self.account = [RAAccountData fromDict:[session objectForKey:@"account"]];
        NSString *sessionID = [NSString stringWithFormat:@"%@-%@", [session objectForKey:@"id"], [session objectForKey:@"token"]];
        DLog(@"%@", sessionID);
        [[RAApiClient sharedClient] setSession:sessionID];
    }
    
    [self registerUUID];
}

- (void)updateAccountData:(NSDictionary *)data {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *sessionData = [defaults objectForKey:@"session"];
    NSDictionary *sessionDict = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:sessionData];
    NSMutableDictionary *newAccountDict = [[NSMutableDictionary alloc] initWithDictionary:[sessionDict objectForKey:@"account"]];
    
    for(NSString *key in [data allKeys]) {
        [newAccountDict setObject:[data objectForKey:key] forKey:key];
    }
    NSMutableDictionary *newDictForArchiving = [NSMutableDictionary dictionaryWithDictionary:sessionDict];
    [newDictForArchiving setObject:newAccountDict forKey:@"account"];
    
    NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:newDictForArchiving];
    [defaults setObject:archivedData forKey:@"session"];
    [defaults synchronize];
    [self setAccount:[RAAccountData fromDict:newAccountDict]];
}

- (void)showIntroView:(BOOL)loginOnly {
    self.tabbarController = nil;
    self.introViewController = [[RAIntroViewController alloc] initWithNibName:@"RAIntroViewController" bundle:nil];
    self.introViewController.loginOnly = loginOnly;
    self.introViewController.wantsFullScreenLayout = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.introViewController];
    [nav setNavigationBarHidden:YES];
    self.window.rootViewController = nav;
}

- (void)showLoggedInAppView {
    self.introViewController = nil;
    /*
    [SVProgressHUD showWithStatus:@"Fetching account"];
    [[RAApiClient sharedClient] get:ACCOUNT_DETAILS successBlock:^(NSDictionary *data) {
        [SVProgressHUD showSuccessWithStatus:@""];
        self.account = [RAAccountData accountDataFromDict:data];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        [SVProgressHUD dismiss];
        RAAlert(@"Error", [error localizedDescription]);    
    }];
     */
    self.tabbarController = [[UITabBarController alloc] init];
    [[self.tabbarController tabBar] setBackgroundImage:[UIImage imageNamed:@"tabbar.png"]];
    [[self.tabbarController tabBar] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_selected.png"]];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.tabbarController.viewControllers = [self getViewControllersiPhone];
    } else {
        self.tabbarController.viewControllers = [self getViewControllersiPad];
    }
    self.tabbarController.selectedIndex = 0;
    self.tabbarController.delegate = self;
    self.window.rootViewController = self.tabbarController;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [Appirater appEnteredForeground:YES];
}

- (void) applicationDidBecomeActive:(UIApplication *)application {
    if(self.notificationDict) {
        [self processNotification];
    }
    if(![[Reachability reachabilityForInternetConnection] isReachable]) {
        RAAlert(@"Internet Connection", @"Looks like you are not connected to the intenet");
    }
}

- (void)processNotification {
    if(self.notificationDict) {
        //This is kind of a hack - basically we should be able to handle these
        //on every tab except for the Camera tab.
        [self.tabbarController setSelectedIndex:0];
        [(UINavigationController *)[self.tabbarController.viewControllers objectAtIndex:0]popToRootViewControllerAnimated:NO];
        DLog(@"%@",[self.tabbarController.viewControllers objectAtIndex:0]);
                     
        ErrorBlock errorBlock = ^(NSError *error, NSURLRequest *request) {
            DLog(@"ERROR %@", [error localizedDescription]);
            [SVProgressHUD dismiss];
        };
        

        
        NSLog(@"This is actually working yayaya %@", self.notificationDict);
        [SVProgressHUD show];
        NSString *type = [self.notificationDict objectForKey:@"type"];
        if([type isEqualToString:NOTIFICATION_TYPE_FOLLOW]) {
            //Show user's profile
            unsigned long long accountID = [[self.notificationDict objectForKey:@"value"] longLongValue];
            [[RAApiClient sharedClient] get:ACCOUNT_DETAILS withPath:[NSString stringWithFormat:@"%lld", accountID] queryParams:nil successBlock:^(NSDictionary *data) {
                NSLog(@"%@", data);
                RAAccountData *account = [RAAccountData fromDict:data];
                RAProfileViewController *profileVC = [RAProfileViewController instanceForAccount:account];
                [(UINavigationController *)[self.tabbarController selectedViewController] pushViewController:profileVC animated:YES];
                [SVProgressHUD dismiss];
            } errorBlock:errorBlock];
        } else if ([type isEqualToString:NOTIFICATION_TYPE_FRIEND_REEL]) {
            //View friend's reel I guess
            unsigned long long reelID = [[self.notificationDict objectForKey:@"value"]longLongValue];
            [[RAApiClient sharedClient] get:REEL withPath:[NSString stringWithFormat:@"%lld", reelID] queryParams:nil successBlock:^(NSDictionary *data) {
                RAReelData *reel = [RAReelData fromDict:data];
                RAThumbnailGridViewController *thumbnailVC = [RAThumbnailGridViewController controllerForReel:reel];
                [(UINavigationController *)[self.tabbarController selectedViewController] pushViewController:thumbnailVC animated:YES];
                [SVProgressHUD dismiss];
            }errorBlock:errorBlock];
            
            
        } else if ([type isEqualToString:NOTIFICATION_TYPE_PHOTO_COMMENT] || [type isEqualToString:NOTIFICATION_TYPE_PHOTO_LIKE]) {
            //Show photo and maybe later fancy pants animation once that's hooked up
            unsigned long long photoID = [[self.notificationDict objectForKey:@"value"] longLongValue];
            [[RAApiClient sharedClient] get:PHOTO withPath:[NSString stringWithFormat:@"%lld", photoID] queryParams:nil successBlock:^(NSDictionary *data) {
                RAPhotoData *photo = [RAPhotoData fromDict:data];
                //UINavigationController *visibleNav = (UINavigationController *)[self.tabbarController selectedViewController];
                //RAPaginatingTableViewController *tableVC = (RAPaginatingTableViewController *)[visibleNav visibleViewController];
                //Not my finest moment
                RAPaginatingTableViewController *tableVC = (RAPaginatingTableViewController *)[[(UINavigationController *)[self.tabbarController.viewControllers objectAtIndex:0] viewControllers] objectAtIndex:0];
                [tableVC showPhoto:photo];
                [SVProgressHUD dismiss];
                if([type isEqualToString:NOTIFICATION_TYPE_PHOTO_COMMENT]) {
                    //TODO - animate in the comments (flip animation a la formspring?)
                }
            }errorBlock:errorBlock];
        } else if ([type isEqualToString:NOTIFICATION_TYPE_REMINDER]) {
            //This seems kind of ... lame wat else to do?
            [self.tabbarController setSelectedIndex:3];
            [[RAApiClient sharedClient] get:REMINDER withPath:[NSString stringWithFormat:@"%lld", [[self.notificationDict objectForKey:@"value"] longLongValue]] queryParams:nil successBlock:^(NSDictionary *data) {
                RAReminderData *reminder = [RAReminderData fromDict:data];
                [SVProgressHUD dismiss];
                DLog(@"%@", data);
                RAReminderViewController *reminderVC = (RAReminderViewController *)[[(UINavigationController*)[self.tabbarController.viewControllers objectAtIndex:3] viewControllers] objectAtIndex:0];
                [reminderVC showCameraForReminder:reminder];
            } errorBlock:errorBlock];

        } else {
            [SVProgressHUD dismiss];
        }
        
        [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"App"
                                                           withAction:@"Notification"
                                                            withLabel:type
                                                            withValue:nil];

        self.notificationDict = nil;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [UAirship land];

    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}


- (BOOL)application:(UIApplication *)application 
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication 
         annotation:(id)annotation 
{
    NSString *urlString = [url absoluteString];
    
    if(!NSEqualRanges([urlString rangeOfString:@"reel://password_reset"], NSMakeRange(NSNotFound, 0))) {
        if([self account]) {
            return NO;
        }
        
        NSArray *components = [urlString componentsSeparatedByString:@"?"];
        NSString *reelURL = [components objectAtIndex:[components count] -1];
        reelURL = [reelURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        RAForgotPasswordViewController *forgotPassword = [[RAForgotPasswordViewController alloc] initWithNibName:@"RASignupViewController" bundle:nil];
        forgotPassword.tokenString = reelURL;
        [(UINavigationController *)self.window.rootViewController pushViewController:forgotPassword animated:YES];
        return YES;
    }
    
    if([sourceApplication isEqualToString:@"com.facebook.Facebook"] &&
       !NSEqualRanges([urlString rangeOfString:@"target_url"], NSMakeRange(NSNotFound, 0))) {
        NSArray *components = [urlString componentsSeparatedByString:@"target_url="];
        if(components && [components count] == 0) {
            return NO;
        }
        urlString = [components objectAtIndex:[components count] - 1];
        urlString = [urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        NSArray *campaignComponents = [urlString componentsSeparatedByString:@"?"];
        NSArray *pathComponents = [(NSString *)[campaignComponents objectAtIndex:0] pathComponents];
        if(!pathComponents || [pathComponents count] == 0) {
            return NO;
        }
        NSString *base36ID = [pathComponents objectAtIndex:[pathComponents count] -1];
        unsigned long long photoID = [RAUtils decimalIDfromBase36:base36ID];

        if(photoID > 0) {
            [self.tabbarController setSelectedIndex:0];
            UINavigationController *homeNav = (UINavigationController *)self.tabbarController.selectedViewController;
            if(homeNav.modalViewController) {
                [homeNav dismissModalViewControllerAnimated:NO];
            }
            if(![[homeNav visibleViewController] isKindOfClass:[RAHomeFeedViewController class]]) {
                return NO;
            }
            [(RAPaginatingTableViewController *)[homeNav visibleViewController] showPhotoWithID:photoID];
        }
        return YES;
    }

    return [FBSession.activeSession handleOpenURL:url];
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Reel" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Reel.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - UITabbarControllerDelegate methods

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    UINavigationController *nav = (UINavigationController*)viewController;
    UIViewController *view = [[nav childViewControllers] objectAtIndex:0];
    
    if([view class] == [RAMockCamControllerForTabBar class]) {
        RACamHolderViewController *camHolder = [[RACamHolderViewController alloc] init];
        UINavigationController *cameraNav = [[UINavigationController alloc] initWithRootViewController:camHolder];
        cameraNav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [nav presentModalViewController:cameraNav animated:YES];
        return NO;
    }
    return YES;
}

-(void)cameraViewDismissed
{
    self.tabbarController.selectedIndex = self.lastTabbarIndex;
}



@end
