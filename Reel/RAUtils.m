//
//  RAUtils.m
//  Reel
//
//  Created by Tim Bowen on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RAUtils.h"

@implementation RAUtils

+ (NSDateFormatter *)getDateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return dateFormatter;
}

+ (NSMutableDictionary *)sanitizedDictionary:(NSDictionary *)inputDict {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:inputDict];
    for(NSString *key in [dict allKeys]) {
        if([dict objectForKey:key] == [NSNull null]){
            [dict removeObjectForKey:key];
        }
    }
    return dict;
}

+ (NSString *)prettyStringForDate:(NSDate *)creationDate {    
    if(creationDate) {
        NSTimeInterval interval = [creationDate timeIntervalSinceNow];
        NSLog(@"Creation date: %@", creationDate);
        NSLog(@"time interval %f", interval);
        
        //Events in the past have negative intervals
        interval = interval * -1;
        
        if(interval < 60) {
            return [NSString stringWithFormat:@"%ds",(int)interval];
        } else if(interval < 60 * 60) {
            return [NSString stringWithFormat:@"%dm",(int)interval/60];
        } else if(interval < 60 * 60 * 24) {
            return [NSString stringWithFormat:@"%dh",(int)interval/(60 * 60)];
        } else if(interval < 60 * 60 * 24 * 365) {
            //dd is a formatter sequence so I guess I'll do this:
            NSNumber *days = [NSNumber numberWithInt:(int)interval/(60 * 60 * 24)];
            return [NSString stringWithFormat:@"%@d", days];
        } else {
            return [NSString stringWithFormat:@"%dy", (int)interval/(60 * 60 * 24 * 365)];
        }
    }
    return @"?";
}


+ (unsigned long long)decimalIDfromBase36:(NSString *)base36String {
    const char * cString = [base36String cStringUsingEncoding:NSUTF8StringEncoding];
    char * end;
    long long photoID = strtoll(cString, &end, 36);
    
    return photoID;
}

+ (NSArray *)groupedPhotosForStyle:(NSArray *)input {
    NSMutableArray *groupedPhotoArray = [NSMutableArray array];
    int filled = 0;
    int prev_row = 3;
    int curr_row = 0;
    int n = input.count;
    while (filled != input.count) {
        if (n - filled == 1 && prev_row == 1) {
            [groupedPhotoArray removeLastObject];
            filled--;
            if ([[groupedPhotoArray lastObject] count] == 2) {
                [groupedPhotoArray removeLastObject];
                filled -= 2;
                if ([[groupedPhotoArray lastObject] count] == 3) {
                    [groupedPhotoArray addObject:[input subarrayWithRange:NSMakeRange(filled++, 1)]];
                    [groupedPhotoArray addObject:[input subarrayWithRange:NSMakeRange(filled, 3)]];
                } else {
                    [groupedPhotoArray addObject:[input subarrayWithRange:NSMakeRange(filled, 3)]];
                    filled += 3;
                    [groupedPhotoArray addObject:[input subarrayWithRange:NSMakeRange(filled, 1)]];
                }
            } else {
                [groupedPhotoArray addObject:[input subarrayWithRange:NSMakeRange(filled, 2)]];
            }
            filled = input.count;
        } else {
            curr_row = prev_row;
            while (curr_row == prev_row) {
                curr_row = arc4random_uniform(MIN(3, n-filled)) + 1;
            }
            [groupedPhotoArray addObject:[input subarrayWithRange:NSMakeRange(filled, curr_row)]];
            filled += curr_row;
            prev_row = curr_row;
        }
    }
    
    return [NSArray arrayWithArray:groupedPhotoArray];
}

@end
