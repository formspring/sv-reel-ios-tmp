//
//  RALikeCell.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/6/12.
//
//

#import "RALikeCell.h"

@implementation RALikeCell

- (void) layoutSubviews {
    [super layoutSubviews];
    self.textLabel.center = CGPointMake(self.textLabel.center.x + 40 + 5, self.textLabel.center.y);
    self.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    self.textLabel.textColor = UIColorFromRGB(0x7B9D1F);
}

@end
