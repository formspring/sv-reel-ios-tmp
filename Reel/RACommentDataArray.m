//
//  RACommentDataArray.m
//  Reel
//
//  Created by Tim Bowen on 8/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RACommentDataArray.h"
#import "RACommentData.h"

@implementation RACommentDataArray

+ (NSMutableArray *)fromHttpRequest:(ASIHTTPRequest *)request {
    NSError *e = nil;
    id jsonData = [NSJSONSerialization JSONObjectWithData: [request responseData] options: NSJSONReadingMutableContainers error: &e];
    if (!e) {
        return [RACommentDataArray fromDict:jsonData];
    }
    return nil;
}

+ (id)fromDict:(NSDictionary *)dict {
    NSMutableArray *commentItemArray = [[NSMutableArray alloc] init];
    for (NSDictionary *jsonDict in dict[@"matches"]) {
        [commentItemArray addObject:[RACommentData fromDict:jsonDict]];
    }
    return commentItemArray;
}
@end
