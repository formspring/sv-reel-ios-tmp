//
//  RAGroupedPhotosCell.h
//  Reel
//
//  Created by Sandosh Vasudevan on 10/11/12.
//
//

#import <UIKit/UIKit.h>

typedef enum {
    RAGroupedPhotosCellTypeEdgeTop = 153,
    RAGroupedPhotosCellTypeEdgeBottom = 154,
    RAGroupedPhotosCellTypeOther = 146
} RAGroupedPhotosCellType;

@class RAPhotoData;

@protocol RAGroupedPhotoCellDelegate <NSObject>

@optional
-(void)photoClicked:(RAPhotoData *)photo forCell:(id)cell;
-(void)photo:(RAPhotoData *)photo selected:(BOOL)selected forCell:(id)cell;;

@end

@interface RAGroupedPhotosCell : UITableViewCell

@property (assign, nonatomic) RAGroupedPhotosCellType cellType;
@property (nonatomic, strong) UIImageView *imageViewOne;
@property (nonatomic, strong) UIImageView *imageViewTwo;
@property (nonatomic, strong) UIImageView *imageViewThree;
@property (nonatomic, strong) UIButton *deleteButtonOne;
@property (nonatomic, strong) UIButton *deleteButtonTwo;
@property (nonatomic, strong) UIButton *deleteButtonThree;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) RAPhotoData *selectedPhoto;
@property (nonatomic, assign) id<RAGroupedPhotoCellDelegate> delegate;
@property (nonatomic, assign, getter = isPhotoSelectable) BOOL photoSelectable;
@property (nonatomic, assign, getter = isSelectable) BOOL selectable;

@end
