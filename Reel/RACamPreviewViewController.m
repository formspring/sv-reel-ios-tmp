//
//  RACamPreviewViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/11/12.
//
//

#import "RACamPreviewViewController.h"

@interface RACamPreviewViewController ()

@end

@implementation RACamPreviewViewController
@synthesize photoBar;
@synthesize previewImageView;

-(id)initWithPhoto:(UIImage*)image
{
    if (self = [super initWithNibName:@"RACamPreviewViewController" bundle:nil]) {
        self.preview = image;
        self.wantsFullScreenLayout = YES;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.previewImageView setImage:self.preview];
    self.photoBar.backgroundColor = [UIColor colorWithPatternImage:
                                     [UIImage imageNamed:@"photo_bar"]];
    
    CGRect videoFrame = self.previewImageView.frame;
    videoFrame.origin.y = ([[UIScreen mainScreen] bounds].size.height - self.photoBar.frame.size.height-videoFrame.size.height)/2;
    DLog(@"%f, %f,%f", self.view.bounds.size.height, self.photoBar.frame.size.height,videoFrame.size.height);
    self.previewImageView.frame = videoFrame;
}

- (void)viewDidUnload
{
    [self setPreviewImageView:nil];
    self.preview = nil;
    self.delegate = nil;
    self.previewImageView.image = nil;
    self.previewImageView = nil;
    [self setPhotoBar:nil];
    [super viewDidUnload];
}

-(void)dealloc
{
    DLog(@"IMA HERE DEALLOCING");
}


-(IBAction)proceedClicked:(id)sender
{
    [self.delegate previewAccepted];
}

-(IBAction)cancelClicked:(id)sender
{
    [self.delegate previewDismissed];
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
