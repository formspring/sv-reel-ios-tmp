//
//  NSDictionary+QueryStringBuilder.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/17/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (QueryStringBuilder)
- (NSString *)queryString;
@end
