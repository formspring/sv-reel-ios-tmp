//
//  RAPhotoImageView.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/30/12.
//
//

#import <UIKit/UIKit.h>
#import "RAPhotoData.h"
#import "RAPhotoScrollView.h"
#import "RAPhotoActionViewController.h"


@protocol RAPhotImageViewDelegate <NSObject>

-(void)closeClikedFor:(id)view;

@end

@interface RAPhotoImageView : UIView <UIScrollViewDelegate>

@property (strong, nonatomic) RAPhotoData *photo;
@property (strong, nonatomic) UIImage *thumb;
@property (strong, nonatomic) id<RAPhotImageViewDelegate>delegate;

@property(strong, nonatomic,readonly) UIImageView *imageView;
@property(strong, nonatomic,readonly) RAPhotoScrollView *scrollView;
@property(strong, nonatomic, readonly) UIActivityIndicatorView *activityView;
@property(nonatomic,assign,getter=isLoading) BOOL loading;
@property(strong, nonatomic) RAPhotoActionViewController *photoActionController;

- (void)killScrollViewZoom;
- (void)layoutScrollViewAnimated:(BOOL)animated;
- (void)rotateToOrientation:(UIInterfaceOrientation)orientation;
-( void)loadPhoto;

@end
