//
//  RAReelViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//
//

#import "RAReelViewController.h"
#import "RAReelItemAray.h"
#import "RAPhotosViewController.h"
#import "RAReelCell.h"
#import "UIImageView+AFNetworking.h"
#import "RAReelHeaderCell.h"

@interface RAReelViewController ()

@end

@implementation RAReelViewController


+(id)instanceForAccount:(RAAccountData *)account
{
    RAReelViewController *instance = [[RAReelViewController alloc] initWithStyle:UITableViewStylePlain
                                                                     andEndpoint:ACCOUNT_DETAILS
                                                                        forClass:@"RAReelData"];
    instance.account = account;
    instance.urlPath = [NSString stringWithFormat:@"%lld/reels", account.itemID];
    instance.paginationParams = [NSDictionary dictionaryWithObject:@"3" forKey:@"photo_previews"];
    
    return instance;
}

- (id)initWithStyle:(UITableViewStyle)style andEndpoint:(API_ENDPOINT)endpoint forClass:(NSString *)className
{
    self = [super initWithStyle:style andEndpoint:endpoint forClass:className];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (self.account.itemID == APPDELEGATE.account.itemID) {
        self.title = @"Me";
    } else {
        self.title = self.account.name;
    }
    self.trackedViewName = @"Reels";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.items = nil;
    self.account = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        return 50;
    }
    RAReelData *reel = [self.items objectAtIndex:indexPath.row/2];
    if (reel.preview.count > 2) {
        return 306;
    }
    return 160;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.items count] * 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RAReelData *reel = [self.items objectAtIndex:indexPath.row/2];
    NSArray *photos = reel.preview;
    
    if (indexPath.row % 2 == 0) {
        RAReelHeaderCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"ReelHeaderCell"];
        if (headerCell == nil) {
            headerCell = [[RAReelHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReelHeaderCell"];
            headerCell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        RAPhotoData *photo = photos[0];
        photo.reel = reel;
        photo.account = self.account;
        headerCell.photo = photo;
        headerCell.delegate = self;
        headerCell.type = RAReelHeaderCellTypeReel;
        return headerCell;
    }
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%@_%d_%d", [RAReelData class], [[UIDevice currentDevice] orientation], photos.count];
    
    RAReelCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RAReelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.delegate = self;
    }
    cell.photos = photos;
    cell.reel = reel;
    cell.cellType = RAGroupedPhotosCellTypeEdgeTop;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RAPhotosViewController *thumbnailView = [RAPhotosViewController instanceForReel:[self.items objectAtIndex:indexPath.row/2]];
    [self.navigationController pushViewController:thumbnailView animated:YES];
}

-(void)photoClicked:(RAPhotoData *)photo forCell:(id)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [self tableView:self.tableView didDeselectRowAtIndexPath:indexPath];
}

@end
