//
//  RAApiClient.h
//  Reel
//
//  Created by Stephan Soileau on 8/14/12.
//
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"
@class Reachability;

// QWOP
extern NSString * const API_BASE;
extern NSString *const PHOTO_BASE_URL;

typedef enum {
    ACCOUNT_DETAILS,
    ACCOUNT_PHOTOS,
    ACCOUNT_REELS,
    ACCOUNT_SETTINGS,
    ACTIVITY_FEED,
    FOLLOW,
    HOME_FEED,
    PHOTO,
    PHOTO_LIKE,
    PHOTO_COMMENT,
    REEL,
    REEL_POPULAR,
    SESSION,
    REMINDER,
    REMINDER_FEED,
    REGISTER_UUID,
    PASSWORD_RESET
} API_ENDPOINT;

typedef void (^ SuccessBlock)(NSDictionary *data);
typedef void (^ ErrorBlock)(NSError *error, NSURLRequest *request);

@interface RAApiClient : AFHTTPClient

@property (strong, nonatomic) NSString *sessionID;
@property (strong, nonatomic) Reachability *reachability;

+(id)sharedClient;
-(void) setSession:(NSString *)sessionID;
-(void) makeRequest:(API_ENDPOINT) endpoint withPath:(NSString*)path data:(NSDictionary *)data queryParams:(NSDictionary*)params method:(NSString*)method successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;

-(void) post:(API_ENDPOINT)endpoint withData:(NSDictionary *)data successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;
-(void) put:(API_ENDPOINT)endpoint withData:(NSDictionary *)data successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;
-(void) post:(API_ENDPOINT)endpoint withPath:(NSString*)path data:(NSDictionary *)data successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;
//No implementation for this method so I commented it
//-(void) post:(API_ENDPOINT)endpoint withPath:(NSString*)path data:(NSDictionary *)data successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;

-(void) get:(API_ENDPOINT)endpoint successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;
-(void) get:(API_ENDPOINT)endpoint withQueryParams:(NSDictionary*)params successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;
-(void) get:(API_ENDPOINT)endpoint withPath:(NSString*)path queryParams:(NSDictionary*)params successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;

-(void) delete:(API_ENDPOINT)endpoint successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;
-(void) delete:(API_ENDPOINT)endpoint withPath:(NSString*)path queryParams:(NSDictionary*)params successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock;


@end
