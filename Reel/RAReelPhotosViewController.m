//
//  RAReelPhotosViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/30/12.
//
//

#import "RAReelPhotosViewController.h"
#import "Social/Social.h"
#import "SVProgressHUD.h"
#import <Twitter/Twitter.h>
#import "RAReelData.h"


@interface RAReelPhotosViewController () {
    NSMutableArray *shareButtons;
}

@end

@implementation RAReelPhotosViewController

+(id)instanceForReel:(RAReelData *)reel
{
    RAReelPhotosViewController *instance = [[RAReelPhotosViewController alloc] initWithAccount:nil Reel:reel];
    return instance;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.trackedViewName = @"Reel";
}

- (void)shareTouched:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    sheet.title = @"Share";
    shareButtons = [NSMutableArray array];
    if (SYSTEM_VERSION_LESS_THAN(@"6.0")) {
        if ([TWTweetComposeViewController canSendTweet]) {
            [shareButtons addObject:@"Twitter"];
        }
    } else {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            [shareButtons addObject:@"Facebook"];
        }
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            [shareButtons addObject:@"Twitter"];
        }
    }
    if ([MFMailComposeViewController canSendMail]) {
        [shareButtons addObject:@"Email"];
    }
    if ([MFMessageComposeViewController canSendText]) {
        [shareButtons addObject:@"Message"];
    }
    [shareButtons addObject:@"Copy Link"];
    [shareButtons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [sheet addButtonWithTitle:obj];
    }];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.destructiveButtonIndex = [shareButtons count];
    sheet.delegate = self;
    [sheet showFromTabBar:[APPDELEGATE.tabbarController tabBar]];
}

- (void)setItems:(NSMutableArray *)items
{
    [super setItems:items];
    if (self.items.count > 0 && self.reel.webURL) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonSystemItemAdd target:self action:@selector(shareTouched:)];
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    RAPhotoData *photo = self.items[0][0];
    NSURL *photoWebURL = [NSURL URLWithString:self.reel.webURL];
    NSString *shareText = ([self.reel.name length] == 0 ? @"Reel" : self.reel.name);
    NSString *shareTextHTML = [NSString stringWithFormat:@"<a href=\"%@\"><img src=\"%@\" /></a><br /><br /><a href=\"%@\">See the full photo on Reel</a>", self.reel.webURL, photo.url300x300, photo.webURL];
    
    if (buttonIndex == [shareButtons count]) {
        
    }
    else if ([shareButtons[buttonIndex] isEqual:@"Facebook"]) {
        SLComposeViewController *FBComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [FBComposer setInitialText:shareText];
        [FBComposer addURL:photoWebURL];
        [self presentViewController:FBComposer animated:YES completion:nil];
    }
    else if ([shareButtons[buttonIndex] isEqual:@"Twitter"]) {
        if (SYSTEM_VERSION_LESS_THAN(@"6.0")) {
            TWTweetComposeViewController *TWComposer = [[TWTweetComposeViewController alloc] init];
            [TWComposer setInitialText:shareText];
            [TWComposer addURL:photoWebURL];
            [self presentViewController:TWComposer animated:YES completion:nil];
        } else {
            SLComposeViewController *TWComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [TWComposer setInitialText:shareText];
            [TWComposer addURL:photoWebURL];
            [self presentViewController:TWComposer animated:YES completion:nil];
        }
    }
    else if ([shareButtons[buttonIndex] isEqual:@"Email"]) {
        MFMailComposeViewController *mfViewController = [[MFMailComposeViewController alloc] init];
        mfViewController.mailComposeDelegate = self;
        [mfViewController setSubject:shareText];
        [mfViewController setMessageBody:shareTextHTML isHTML:YES];
        [self presentModalViewController:mfViewController animated:YES];
    }
    else if ([shareButtons[buttonIndex] isEqual:@"Message"]) {
        MFMessageComposeViewController *mfmViewController = [[MFMessageComposeViewController alloc] init];
        mfmViewController.messageComposeDelegate = self;
        mfmViewController.body = [NSString stringWithFormat:@"%@ %@", shareText, self.reel.webURL];
        [self presentModalViewController:mfmViewController animated:YES];
    }
    else {
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString:self.reel.webURL];
        [SVProgressHUD showSuccessWithStatus:@"Copied"];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            //            [SVProgressHUD showErrorWithStatus:@"Message Canceled"];
            break;
        case MFMailComposeResultSaved:
            [SVProgressHUD showSuccessWithStatus:@"Message Saved"];
            break;
        case MFMailComposeResultSent:
            [SVProgressHUD showSuccessWithStatus:@"Message Sent"];
            break;
        case MFMailComposeResultFailed:
            [SVProgressHUD showErrorWithStatus:@"Message Failed"];
            break;
        default:
            [SVProgressHUD showErrorWithStatus:@"Message Not Sent"];
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
            [SVProgressHUD showErrorWithStatus:@"Message Failed"];
            break;
        case MessageComposeResultSent:
            [SVProgressHUD showSuccessWithStatus:@"Message Sent"];
            break;
        default:
            break;
    }
    [self dismissModalViewControllerAnimated:YES];
}

@end
