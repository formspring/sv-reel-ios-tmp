//
//  RAAppDelegate.h
//  Reel
//
//  Created by Tim Bowen on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define APPDELEGATE ((RAAppDelegate *)[[UIApplication sharedApplication] delegate])

#import <UIKit/UIKit.h>
#import "RAApiClient.h"
#import "RAAccountData.h"
#import "RAIntroViewController.h"
#import "GAI.h"


@interface RAAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate, ASIHTTPRequestDelegate, UIAppearanceContainer>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabbarController;
@property (strong, nonatomic) RAIntroViewController *introViewController;
@property (strong, nonatomic) RAApiClient *apiClient;
@property (strong, nonatomic) RAAccountData *account;
@property (strong, nonatomic) NSMutableArray *photoQueueArray;
@property (strong, nonatomic) NSString *UUID;
@property (strong, nonatomic) NSMutableDictionary *notificationDict;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (assign) int lastTabbarIndex;

@property BOOL showCameraActionSheet;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)initializeAccount;
-(void)showIntroView:(BOOL)loginOnly;
-(void)showLoggedInAppView;
- (void)updateAccountData:(NSDictionary *)data;

//@property (strong, nonatomic) UINavigationController *navigationController;

//@property (strong, nonatomic) UISplitViewController *splitViewController;

@end
