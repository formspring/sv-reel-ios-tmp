//
//  RASVCommentViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/13/12.
//
//

#import "RASVCommentViewController.h"
#import "RACommentData.h"

@interface RASVCommentViewController ()

@end

@implementation RASVCommentViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //basically the minimum height of a cell right here
    int height = 50;
    //10 padding on right side plus avatar width I guess, unless we want to get super fancy
    //and like put the avatar IN the text view
    int labelWidth = self.tableView.frame.size.width - 60;
    int absurdHeight = 99999;
    CGSize constraintSize = CGSizeMake(labelWidth, absurdHeight);
    RACommentData *comment = [self.items objectAtIndex:indexPath.row];
    
    CGSize adjustedSize = [comment.text sizeWithFont:[RACommentCell commentLabelFont] constrainedToSize:constraintSize];
    
    NSLog(@"returning this : %f", MAX(height, adjustedSize.height));
    
    //30 is the collective height of all the other crap and padding on the cell
    return MAX(height, adjustedSize.height + 30);
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

        static NSString *commentIdentifier = @"CommentCell";
        RACommentCell *commentCell = [tableView dequeueReusableCellWithIdentifier:commentIdentifier];
        if (commentCell == nil) {
            commentCell = [[RACommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:commentIdentifier];
        }
        RACommentData *comment = [self.items objectAtIndex:indexPath.row];
        commentCell.commentData = comment;
        commentCell.delegate = self.cellDelegate;
        return commentCell;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc
{
    self.cellDelegate = nil;
}



@end
