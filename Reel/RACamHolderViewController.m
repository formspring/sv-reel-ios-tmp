//
//  RACamHolderViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/11/12.
//
//

#import "RACamHolderViewController.h"
#import "AVCamCaptureManager.h"
#import "RASavePhotoViewController.h"
#import "RAReminderData.h"
#import "UINavigationController+RATransition.h"
#import "Appirater.h"
#import "GTMUIImage+Resize.h"

@interface RACamHolderViewController () {
    BOOL _fromCamera;
    NSURL *_assetURL;
}
@end

@implementation RACamHolderViewController

@synthesize forProfilePhoto = _forProfilePhoto;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.hidesBottomBarWhenPushed = YES;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.wantsFullScreenLayout = YES;
        self.forProfilePhoto = NO;
    }
    return self;
}

- (void)setReminder:(RAReminderData *)theReminder {
    _reminder = theReminder;
    self.camViewController.reminder = theReminder;
}

- (RAReminderData *)getReminder {
    return _reminder;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.camViewController = [[RACamViewController alloc] init];
    self.camViewController.view.frame = self.view.frame;
    self.camViewController.delegate = self;
    self.camViewController.reminder = self.reminder;
    self.camViewController.newUser = self.isNewUser;
    [self.view addSubview:self.camViewController.view];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if (self.view.window == nil) {
        self.camViewController = nil;
        self.view = nil;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.camViewController = nil;
    // Release any retained subviews of the main view.
}

-(void)dealloc
{
    DLog(@"IMA HERE DEALLOCING");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (picker != nil) {
        // From lib
        [self dismissModalViewControllerAnimated:YES];
        _assetURL = (NSURL *)[info valueForKey:UIImagePickerControllerReferenceURL];
        _fromCamera = NO;
    } else {
        [self.camViewController stopCamera];
        _fromCamera = YES;
    }
    self.photo = [info objectForKey:UIImagePickerControllerOriginalImage];
    RACamPreviewViewController *preview = [[RACamPreviewViewController alloc] initWithPhoto:self.photo];
    preview.delegate = self;
    [self.navigationController pushViewController:preview animated:NO];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if (picker == nil) {
        //From camera
        [self.camViewController stopCamera];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [self.presentingViewController dismissModalViewControllerAnimated:YES];
        [self.delegate photoSaved:NO inReel:self.reminder.reel];
    } else {
        //From photo lib
        [self.camViewController startCamera];
        [self dismissModalViewControllerAnimated:YES];
    }
}

-(void)showPhotoLibrary
{
    [self.camViewController stopCamera];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    [self presentModalViewController:imagePicker animated:NO];
}

-(void)previewAccepted
{
    if(self.forProfilePhoto) {
         UIImage *imageWithProperOrientaiton;
         if (self.photo.size.width > 150 || self.photo.size.height > 150) {
            imageWithProperOrientaiton = [self.photo gtm_imageByResizingToSize:CGSizeMake(150, 150) preserveAspectRatio:YES trimToFit:NO];
         } else {
            imageWithProperOrientaiton = self.photo;
         }
        if([self.delegate respondsToSelector:@selector(profilePhotoDataReady:)]) {
            [self.delegate profilePhotoDataReady:imageWithProperOrientaiton];
        }
        return;
    }
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    RASavePhotoViewController *savePhoto = [RASavePhotoViewController instanceWithPhoto:self.photo fromCamera:_fromCamera withAssetURL:_assetURL];
    savePhoto.delegate = self;
    if (self.reminder != nil) {
        savePhoto.reel = self.reminder.reel;
    }
    [self.navigationController pushViewController:savePhoto animated:YES];
}

-(void)previewDismissed
{
    [self.navigationController popToViewController:self animated:NO];
    [self.camViewController startCamera];
}

-(void)saveCancelled
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self.navigationController popToViewController:self animated:NO];
    [self.camViewController startCamera];
}

-(void)saveCompleted
{
    [self dismissModalViewControllerAnimated:YES];
    [self.delegate photoSaved:YES inReel:self.reminder.reel];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [Appirater userDidSignificantEvent:YES];
}

@end
