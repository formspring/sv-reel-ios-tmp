//
//  RAFriendActivityViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/11/12.
//
//

#import "RAFriendActivityViewController.h"
#import "RAFriendActivityData.h"
#import "RAGroupedPhotosCell.h"
#import <OHAttributedLabel/NSAttributedString+Attributes.h>
#import <OHAttributedLabel/OHAttributedLabel.h>
#import <OHAttributedLabel/OHASBasicHTMLParser.h>
#import <OHAttributedLabel/OHASBasicMarkupParser.h>
#import "RAPhotoActionViewController.h"
#import "RAPhotoBrowser.h"


@interface RAFriendActivityViewController () {
    NSIndexPath *_selectedIndex;
}

@end

@implementation RAFriendActivityViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.endpoint = ACCOUNT_DETAILS;
        self.urlPath = @"friend_activity";
        self.dataClassName = @"RAFriendActivityData";
        self.refreshable = YES;
        self.paginatable = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.title = @"Activity";

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RAFriendActivityData *friendActivityData = [self.items objectAtIndex:indexPath.section];
    if (indexPath.row == 0 && friendActivityData.groupedPhotoArray.count == 1) {
        return 160;
    }
    if ([friendActivityData.groupedPhotoArray count] == indexPath.row + 1 || indexPath.row == 0) {
        return RAGroupedPhotosCellTypeEdgeTop;
    }
    return RAGroupedPhotosCellTypeOther;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    RAFriendActivityData *friendActivityData = [self.items objectAtIndex:section];
    CGSize maxSize = CGSizeMake(self.tableView.frame.size.width - 20, 99999);
    CGSize aSize = [[friendActivityData activityText] sizeConstrainedToSize:maxSize];
    DLog(@"%f", aSize.height);
    if (aSize.height < 35) {
        return 45;
    }
    return aSize.height + 10;
}


- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    RAFriendActivityData *friendActivityData = [self.items objectAtIndex:section];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 15)];
    view.backgroundColor = UIColorFromRGBWithAlpha(0xFFFFFF, 0.9);
    CGSize maxSize = CGSizeMake(self.tableView.frame.size.width - 20, 99999);
    CGSize aSize = [[friendActivityData activityText] sizeConstrainedToSize:maxSize];

    
    OHAttributedLabel *label = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(10, 5, view.frame.size.width - 20, aSize.height + 10)];
    label.backgroundColor = UIColorFromRGBWithAlpha(0xFFFFFF, 0);
    [view addSubview:label];
    label.attributedText = [friendActivityData activityText];
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    RAFriendActivityData *activity = [self.items objectAtIndex:section];
    return activity.groupedPhotoArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RAFriendActivityData *friendActivityData = [self.items objectAtIndex:indexPath.section];
    NSArray *photos = [friendActivityData.groupedPhotoArray objectAtIndex:indexPath.row];

    NSString *CellIdentifier = [NSString stringWithFormat:@"%@_%d_%d", [RAFriendActivityData class], [[UIDevice currentDevice] orientation], photos.count];

    RAGroupedPhotosCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RAGroupedPhotosCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.delegate = self;
        cell.photoSelectable = YES;
    }
    if (indexPath.row == 0) {
        cell.cellType = RAGroupedPhotosCellTypeEdgeTop;
    } else if (indexPath.row == photos.count - 1) {
        cell.cellType = RAGroupedPhotosCellTypeEdgeBottom;
    }else {
        cell.cellType = RAGroupedPhotosCellTypeOther;
    }
    cell.photos = photos;
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLog(@"SELECTED");
}

-(void)photoClicked:(RAPhotoData *)photo forCell:(id)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    _selectedIndex = indexPath;
    
    RAFriendActivityData *friendActivityData = [self.items objectAtIndex:_selectedIndex.section];
    RAPhotoBrowser *browser = [[RAPhotoBrowser alloc] initWithRAPhotoArray:friendActivityData.photos];
    [browser setInitialPageIndex:[friendActivityData.photos indexOfObject:photo]];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    RAFriendActivityData *friendActivityData = [self.items objectAtIndex:_selectedIndex.section];
    return friendActivityData.photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    RAFriendActivityData *friendActivityData = [self.items objectAtIndex:_selectedIndex.section];
    if (index < friendActivityData.photos.count) {
        RAPhotoData *photo = friendActivityData.photos[index];
        return [MWPhoto photoWithRAPhotoData:photo];
    }
    return nil;
}

- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
    RAFriendActivityData *friendActivityData = [self.items objectAtIndex:_selectedIndex.section];
    RAPhotoData *photo = friendActivityData.photos[index];
    RAPhotoActionViewController *photoAction = [[RAPhotoActionViewController alloc] initWithNibName:@"RAPhotoActionViewController" bundle:nil];
    photoAction.photo = photo;
    return (MWCaptionView *)photoAction.view;
}


@end
