//
//  RAPhotoBrowser.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/8/12.
//
//

#import "MWPhotoBrowser.h"
#import "RAPhotoData.h"
#import "RAPhotoActionViewController.h"


@interface RAPhotoBrowser : MWPhotoBrowser <MWPhotoBrowserDelegate, RAPhotoActionDelegate>

-initWithRAPhotoArray:(NSArray*)photoArray;
-initWithRAPhoto:(RAPhotoData*)photo;

@end
