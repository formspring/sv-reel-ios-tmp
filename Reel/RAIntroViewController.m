//
//  RAIntroViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/15/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "RAIntroViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SVProgressHUD.h"
#import "ASIHTTPRequest.h"
#import "RAPhotoOperationQueue.h"
#import "RASignupViewController.h"
#import "RALoginViewController.h"
#import "RALoggedoutExploreViewController.h"

@interface RAIntroViewController ()
- (void)saveOrLoginFacebookUser:(FBSession*) session;
@end

@implementation RAIntroViewController
@synthesize pageControl;
@synthesize firstView;
@synthesize secondView;
@synthesize thirdView;
@synthesize fourthView;
@synthesize scrollView;
@synthesize pageControlBeingUsed;
@synthesize facebookButton;
@synthesize fifthView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self.scrollView addSubview:firstView];
    [secondView setFrame:CGRectMake(0, 20, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
    [self.scrollView addSubview:secondView];

    [thirdView setFrame:CGRectMake(self.scrollView.frame.size.width * 1, 20, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
    [self.scrollView addSubview:thirdView];
    
    [fourthView setFrame:CGRectMake(self.scrollView.frame.size.width * 2, 20, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
    [self.scrollView addSubview:fourthView];
    
    [fifthView setFrame:CGRectMake(self.scrollView.frame.size.width * 3, 20, self.scrollView.frame.size.width, self.scrollView.frame.size.height)];
    [self.scrollView addSubview:fifthView];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * 4, self.scrollView.frame.size.height);
    
    pageControlBeingUsed = NO;
    
    self.trackedViewName = @"Intro";
    
    self.view.backgroundColor = COLOR_BEIGE;
    self.firstView.backgroundColor = COLOR_BEIGE;
    self.secondView.backgroundColor = COLOR_BEIGE;
    self.thirdView.backgroundColor = COLOR_BEIGE;
    self.fourthView.backgroundColor = COLOR_BEIGE;
    self.fifthView.backgroundColor = COLOR_BEIGE;
    self.scrollView.backgroundColor = COLOR_BEIGE;
    DLog(@"%f, %f", self.scrollView.frame.origin.y, secondView.frame.origin.y);

    
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.pageControl.currentPage == 0) {
        UIView *mockSplashPage = [self.secondView viewWithTag:100];
        [UIView animateWithDuration:1.0f
                              delay:0.f
                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut
                         animations:^{
                             mockSplashPage.alpha = 0;
                             mockSplashPage.frame = CGRectMake(mockSplashPage.frame.origin.x - (mockSplashPage.frame.size.width/2),
                                                               mockSplashPage.frame.origin.y - (mockSplashPage.frame.size.height/4),
                                                               mockSplashPage.frame.size.width * 2,
                                                               mockSplashPage.frame.size.height * 2);
                         }
                         completion:nil];        
    }
}

- (void)viewDidUnload
{
    [self setFirstView:nil];
    [self setSecondView:nil];
    [self setThirdView:nil];
    [self setFourthView:nil];
    [self setFifthView:nil];
    [self setScrollView:nil];
    [self setPageControl:nil];
    [self setFacebookButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)loginWithFacebookTouched:(id)sender {
    self.facebookButton.enabled = NO;
    [FBSession openActiveSessionWithPermissions:[NSArray arrayWithObjects:@"publish_actions", @"email", @"user_birthday", nil]
                                   allowLoginUI:YES
                              completionHandler:^(FBSession *session,
                                                  FBSessionState status,
                                                  NSError *error) {
                                  if (session.isOpen) {
                                      [self saveOrLoginFacebookUser:session];
                                  } else {
                                      self.facebookButton.enabled = YES;
                                      RAAlert(@"Error",
                                              @"Facebook connect failed");
                                      
                                      [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"App"
                                                                                         withAction:@"Error"
                                                                                          withLabel:@"Facebook Connect"
                                                                                          withValue:nil];
                                  }
                              }];
    
    [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"App"
                                                       withAction:@"Login"
                                                        withLabel:nil
                                                        withValue:nil];
}

- (IBAction)signupTouched:(id)sender {
    NSLog(@"Signup clicked");
    RASignupViewController *signupVC = [[RASignupViewController alloc] initWithNibName:@"RASignupViewController" bundle:nil];
    [self.navigationController pushViewController:signupVC animated:YES];
}

- (IBAction)loginTouched:(id)sender {
    NSLog(@"login touched");
    RALoginViewController *loginVC = [[RALoginViewController alloc] initWithNibName:@"RASignupViewController" bundle:nil];
    [self.navigationController pushViewController:loginVC animated:YES];
    
}

- (IBAction)exploreTouched:(id)sender {
    RALoggedoutExploreViewController *explore = [[RALoggedoutExploreViewController alloc] init];
    [self.navigationController pushViewController:explore animated:YES];
}

- (void)saveOrLoginFacebookUser:(FBSession *)session {
    [SVProgressHUD show];
    NSDictionary *data = [NSDictionary dictionaryWithObject:session.accessToken forKey:@"facebook_token"];
    [[RAApiClient sharedClient] get:SESSION withQueryParams:data successBlock:^(NSDictionary *data) {
        DLog(@"%@", data);
        //Saving in NSUserdefaults for now
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:data];
        [defaults setObject:archivedData forKey:@"session"];
        [defaults synchronize];
            
        [SVProgressHUD showSuccessWithStatus:@""];
        [APPDELEGATE initializeAccount];
        [APPDELEGATE showLoggedInAppView];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        [SVProgressHUD showErrorWithStatus:@""];
        self.facebookButton.enabled = YES;
        RAAlert(@"Error",
                [error localizedDescription]);
    }];
}

- (IBAction)changePage:(id)sender {
    // update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth);
    [self updateViewForPage:page];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (!pageControlBeingUsed) {
        // Update the page when more than 50% of the previous/next page is visible
        CGFloat pageWidth = self.scrollView.frame.size.width;
        int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pageControl.currentPage = page;
        [self updateViewForPage:page]; 
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)updateViewForPage:(int)page
{
    if (page == 3) {
        [self.pageControl setHidden:YES];
    } else {
        [self.pageControl setHidden:NO];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark RACameraPromptViewDelegate methods

-(void)cameraPromptViewController:(id)controller finishedWithImage:(UIImage *)image inReel:(RAReelData *)reel
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        RAPhotoOperationQueue *queue = [RAPhotoOperationQueue instanceForPhoto:image fromCamera:YES];
        [queue savePhotoToReel:reel comment:@"" share:YES];
    });
    [APPDELEGATE showLoggedInAppView];
}

-(void)cameraPromptViewControllerSkipped:(id)controller
{
    [APPDELEGATE showLoggedInAppView];
}

@end
