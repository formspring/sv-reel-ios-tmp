//
//  UIActivityIndicatorView+Reel.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "UIActivityIndicatorView+Reel.h"

@implementation UIActivityIndicatorView (Reel)

+ (UIActivityIndicatorView *)addSpinnerTo:(UITableViewCell *)cell
{
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] 
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame = CGRectMake(cell.frame.origin.x + 20, cell.frame.size.height/2 - spinner.frame.size.height/2, spinner.frame.size.width, spinner.frame.size.height);
    [spinner startAnimating];
    [spinner setTag: 1];
    [cell addSubview:spinner];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    return spinner;
}

@end
