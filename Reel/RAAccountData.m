//
//  RAAccountData.m
//  Reel
//
//  Created by Stephan Soileau on 8/14/12.
//
//

#import "RAAccountData.h"
#import "RAUtils.h"
#import "RAReminderData.h"

@implementation RAAccountData

@synthesize itemID = _account_id;
@synthesize username = _username;
@synthesize facebookID = _facebook_id;
@synthesize name = _name;
@synthesize email = _email;
@synthesize friends = _friends;
@synthesize created = _created;
@synthesize avatar150 = _avatar150;
@synthesize avatar50 = _avatar50;
@synthesize avatarOriginal = _avatarOriginal;
@synthesize hasFacebook = _hasFacebook;

+ (id) fromHttpRequest:(ASIHTTPRequest *)request {
    NSError *e = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData: [request responseData] options: NSJSONReadingMutableContainers error: &e];
    NSLog(@"%@", e ? [e localizedDescription] :@"no error");
    
    if (!e) {
        return [RAAccountData fromDict:jsonData];
    }
    return nil;
}

+ (id)fromDict:(NSDictionary *)dictionary {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    for(NSString *key in [dict allKeys]) {
        if([dict objectForKey:key] == [NSNull null]){
            [dict removeObjectForKey:key];
        }
    }
    
    RAAccountData *account = [[RAAccountData alloc] init];
    account.itemID = [[dict objectForKey:@"id"] longLongValue];
    account.username = [dict objectForKey:@"username"];
    account.facebookID = [[dict objectForKey:@"facebook_id"] longLongValue];
    account.name = [dict objectForKey:@"name"];
    account.email = [dict objectForKey:@"email"];
    account.friends = [dict objectForKey:@"friends"];
    account.hasFacebook = [[dict objectForKey:@"has_facebook"] boolValue];
    account.created = [[RAUtils getDateFormatter] dateFromString:[dict objectForKey:@"created"]];
    id photoDict = [dict objectForKey:@"photo_urls"];
    if([photoDict isKindOfClass:[NSDictionary class]]) {
        account.avatar150 = [photoDict objectForKey:@"150x150"];
        account.avatar50 = [photoDict objectForKey:@"50x50"];
        account.avatarOriginal = [photoDict objectForKey:@"original"];
    }
    [account setFollowing:[[dict objectForKey:@"is_following"] boolValue]];
    id reminderDict = [dict objectForKey:@"reminder"];
    if (reminderDict != nil) {
        account.reminder = [RAReminderData fromDict:reminderDict];
    }
    account.newAccount = [[dict objectForKey:@"new"] boolValue];
    return account;
    
}
@end
