//
//  RAReelPhotosViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 10/30/12.
//
//

#import "RAPhotosViewController.h"

@interface RAReelPhotosViewController : RAPhotosViewController <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@end
