//
//  RAFriendActivityData.h
//  Reel
//
//  Created by Sandosh Vasudevan on 10/11/12.
//
//

#import <Foundation/Foundation.h>
#import "RADataObject.h"

@class RAAccountData;
typedef enum {
    RAFriendActivityTypeLike = 1
} RAFriendActivityType;

@interface RAFriendActivityData : NSObject <RADataObject>

@property (nonatomic, assign) RAFriendActivityType type;
@property (nonatomic, strong) NSArray *actors;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSArray *groupedPhotoArray;
@property (nonatomic, strong) NSString *verb;
@property (nonatomic, strong) RAAccountData *account;

-(NSAttributedString*)activityText;

@end
