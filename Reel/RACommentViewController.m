//
//  RACommentViewController.m
//  Reel
//
//  Created by Tim Bowen on 8/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RACommentViewController.h"
#import "RAPhotoData.h"
#import "RAApiClient.h"
#import "ASIHTTPRequest.h"
#import "RACommentDataArray.h"
#import "RACommentData.h"
#import "HPGrowingTextView.h"
#import "ASIDownloadCache.h"
#import "RAHomeFeedViewController.h"
#import "RAProfileViewController.h"
#import "SVProgressHud.h"
#import "RASVCommentViewController.h"
#import "UIImageView+AFNetworking.h"

#define COMMENT_BAR_HEIGHT 44

@interface RACommentViewController ()

@end

@implementation RACommentViewController

@synthesize feedItem = _feedItem;
@synthesize commentArray = _commentArray;
@synthesize textView = _textView;
@synthesize avatarView = _avatarView;
@synthesize textViewContainer = _textViewContainer;
@synthesize tableViewController = _tableViewController;
@synthesize textViewMask = _textViewMask;



- (id)initWithStyle:(UITableViewStyle)style forFeedItem:(RAPhotoData *)item
{
    //self = [super initWithStyle:style];
    
    self = [super init];
    if (self) {
        // Custom initialization
        self.commentArray = nil;
        self.feedItem = item;
        self.textView = [[HPGrowingTextView alloc] init];
        self.avatarView = [[UIImageView alloc] init];
        RASVCommentViewController *tvc = [[RASVCommentViewController alloc]initWithStyle:UITableViewStylePlain andEndpoint:PHOTO forClass:@"RACommentData"];
        tvc.cellDelegate = self;
        self.tableViewController = tvc;
        self.tableViewController.urlPath = [NSString stringWithFormat:@"%lld/comments", self.feedItem.itemID];
        self.tableViewController.tableView.backgroundColor = [UIColor whiteColor];
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    self.tableViewController.view.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height - COMMENT_BAR_HEIGHT);
    [self.view addSubview:self.tableViewController.tableView];
    [self fetchAvatar];
    
    CGRect textViewContainerFrame = CGRectMake(0, self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - COMMENT_BAR_HEIGHT, self.view.frame.size.width, COMMENT_BAR_HEIGHT);
    DLog(@"self.veiw %@", self.view);

    self.textViewContainer = [[UIView alloc] initWithFrame:textViewContainerFrame];
    
    DLog(@"%@",self.textViewContainer);
    UIImageView *containerBackground = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"MessageEntryBackground.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:22]];
    containerBackground.frame = self.textViewContainer.bounds;
    containerBackground.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.textViewContainer addSubview:containerBackground];
    self.textViewContainer.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:self.textViewContainer];
    if(self.feedItem.commentCount == 0) {
        [self.textView becomeFirstResponder];
    }
    
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    entryImageView.frame = self.textView.bounds;
    
    //TODO literal values ipad etc
    int xPos = 51;
    int postButtonWidth = 60;
    self.textView.frame = CGRectMake(xPos, 3, self.textViewContainer.frame.size.width - xPos - postButtonWidth - 16, COMMENT_BAR_HEIGHT);
    self.textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.textView.maxNumberOfLines = 2;
    self.textView.delegate = self;
    //  self.textView.internalTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.textViewContainer addSubview:self.textView];
    self.textView.center = CGPointMake(self.textView.center.x, self.textViewContainer.frame.size.height / 2);
    //[self.textView becomeFirstResponder];
    entryImageView.frame = CGRectMake(self.textView.frame.origin.x, 0, self.textView.frame.size.width, self.textViewContainer.frame.size.height);
    self.textViewMask = entryImageView;
    self.textViewContainer.autoresizesSubviews = YES;
    [self.textViewContainer addSubview:self.textViewMask];
    
    self.avatarView.frame = CGRectMake(10, 5, 30, 30);
    self.avatarView.layer.cornerRadius = 5.0;
    self.avatarView.layer.masksToBounds = YES;
    self.avatarView.layer.borderColor = UIColorFromRGB(0x808080).CGColor;
    self.avatarView.layer.borderWidth = 1.0;
    
    [self.textViewContainer addSubview:self.avatarView];
    self.avatarView.center = CGPointMake(self.avatarView.center.x, self.textViewContainer.frame.size.height / 2);
    
    
    
    UIButton *postButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [postButton setTitle:@"Post" forState:UIControlStateNormal];
    [postButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
    [postButton setBackgroundImage:[UIImage imageNamed:@"post_comment.png"] forState:UIControlStateNormal];
    postButton.frame = CGRectMake(self.textViewContainer.frame.size.width - 71, 15, 60, 30);
    [postButton.titleLabel setShadowColor:[UIColor blackColor]];
    [postButton.titleLabel setShadowOffset:CGSizeMake(0, -1)];
    postButton.center = CGPointMake(postButton.center.x, self.textViewContainer.frame.size.height / 2 + 1);
    postButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    [postButton addTarget:self action:@selector(postComment) forControlEvents:UIControlEventTouchUpInside];
    [self.textViewContainer addSubview:postButton];
    self.postButton = postButton;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];

    self.title = @"Comments";
    self.trackedViewName = @"Comments";
}

- (void)keyboardWasShown:(NSNotification *)keyboardNotification {
    NSLog(@"keyboard shown");
    CGSize keyboardSize = [[[keyboardNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    NSLog(@"%@",[[keyboardNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey]);
    NSLog(@"%@", [[keyboardNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey]);
    //CGRect currentFrame = self.textViewContainer.frame;
    //self.textViewContainer.frame = CGRectMake(currentFrame.origin.x, currentFrame.origin.y - keyboardSize.height + self.tabBarController.tabBar.frame.size.height, currentFrame.size.width, currentFrame.size.height);
    self.textViewContainer.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    NSLog(@"%f", self.view.frame.size.height);
    NSLog(@"%f x %f", keyboardSize.height, keyboardSize.width);
    NSLog(@"%f", self.navigationController.navigationBar.frame.size.height);
    
    //width and height are reversed in landscape herp a derp a doo
    int textViewY;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight ) {
        textViewY = self.view.frame.size.height - keyboardSize.width - COMMENT_BAR_HEIGHT;
    } else {
        textViewY = self.view.frame.size.height - keyboardSize.height - COMMENT_BAR_HEIGHT;
    }
    
    
    self.textViewContainer.frame = CGRectMake(0, textViewY, self.view.frame.size.width, COMMENT_BAR_HEIGHT);
    CGRect tableViewFrame = self.tableViewController.tableView.frame;
    tableViewFrame.size.height = textViewY;
    self.tableViewController.tableView.frame = tableViewFrame;
}

-(void)keyboardWillHide:(NSNotification *)keyboardNotification {
    CGRect tableViewFrame = self.tableViewController.tableView.frame;
    tableViewFrame.size.height = self.view.frame.size.height - COMMENT_BAR_HEIGHT;
    self.tableViewController.tableView.frame = tableViewFrame;
    self.textViewContainer.frame = CGRectMake(0, self.view.frame.size.height - COMMENT_BAR_HEIGHT, self.view.frame.size.width, COMMENT_BAR_HEIGHT);
}


- (void)viewDidUnload
{   
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.textView = nil;
    self.textViewContainer = nil;
    self.postButton = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    NSLog(@"%@", self.textViewContainer);
}

- (void)postComment {
    if([self.textView.text isEqualToString:@""]) {
        return;
    }
    [self.postButton setEnabled:NO];
    [SVProgressHUD show];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithLongLong:self.feedItem.itemID], @"photo_id", self.textView.text, @"comment_text", nil];
    NSString *text = self.textView.text;
    [[APPDELEGATE apiClient] post:PHOTO_COMMENT withData:dict successBlock:^(NSDictionary *data) {
        DLog(@"%@", data);
        [SVProgressHUD dismiss];
        RACommentData *comment = [RACommentData fromDict:data];
        [self.tableViewController.items addObject:comment];
        self.textView.text = @"";
        self.feedItem.commentCount += 1;
        [self.tableViewController.tableView reloadData];

        NSIndexPath* ipath = [NSIndexPath indexPathForRow:(self.tableViewController.items.count - 1) inSection: 0];
        [self.tableViewController.tableView scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
        
        [self.postButton setEnabled:YES];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        [self.textView setText:text];
        [self.textView becomeFirstResponder];
        [self.postButton setEnabled:YES];
    }];
    
    [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"Photo"
                                                       withAction:@"Comment"
                                                        withLabel:nil
                                                        withValue:nil];
    [self.textView resignFirstResponder];
}

- (BOOL)growingTextViewShouldReturn:(HPGrowingTextView *)textView {
    NSLog(@"Calling this nonsense %@", self.textView.text);
    //Send comment
    
    [self postComment];
       
    return NO;
}

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView {
    DLog(@"called");
    if([growingTextView.text length] > 255) {
        growingTextView.text = [[growingTextView text] substringToIndex:255];
    }
}

#pragma mark - Table view data sourcer

- (void)showProfile:(RAAccountData *)account {
    RAProfileViewController *profileVC = [RAProfileViewController instanceForAccount:account];
    [self.navigationController pushViewController:profileVC animated:YES];
}

- (void)fetchAvatar {
    NSURL *avatarURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", APPDELEGATE.account.avatar50]];
    [self.avatarView setImageWithURL:avatarURL];    
}


- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = self.textViewContainer.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	self.textViewContainer.frame = r;
    self.avatarView.center = CGPointMake(self.avatarView.center.x, self.textViewContainer.frame.size.height / 2);
}


@end
