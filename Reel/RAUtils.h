//
//  RAUtils.h
//  Reel
//
//  Created by Tim Bowen on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  Conviencne methods and stuff to avoid copy pasting tons of code

#import <Foundation/Foundation.h>

@interface RAUtils : NSObject

+ (NSDateFormatter *)getDateFormatter;
+ (NSMutableDictionary *)sanitizedDictionary:(NSDictionary *)inputDict;
+ (NSString *)prettyStringForDate:(NSDate *)creationDate;
+ (unsigned long long)decimalIDfromBase36:(NSString *)base36String;
+ (NSArray *)groupedPhotosForStyle:(NSArray *)input;
@end
