//
//  RARefreshTableHeaderView.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/31/12.
//
//

#import "RARefreshTableHeaderView.h"


@interface RARefreshTableHeaderView () {
    CALayer *_colorBarView;
    CALayer *_colorBarView2;
}

-(CALayer*)getColoredLayer;
-(void)startColorBarAnimation;
-(void)stopColorBarAnimation;
-(void)resizeSubLayersFor:(CALayer *)layer;
@end

@implementation RARefreshTableHeaderView

-(id)initWithFrame:(CGRect)frame arrowImageName:(NSString *)arrow textColor:(UIColor *)textColor
{
    self = [super initWithFrame:frame arrowImageName:arrow textColor:textColor];
    if (self) {
        
        _colorBarView = [self getColoredLayer];
		_colorBarView.frame = CGRectMake(0, frame.size.height - COLOR_BAR_HEIGHT, frame.size.width, COLOR_BAR_HEIGHT);
		
        
        _colorBarView2 = [self getColoredLayer];
        _colorBarView2.frame = CGRectMake(-1 * frame.size.width, frame.size.height - COLOR_BAR_HEIGHT, frame.size.width, COLOR_BAR_HEIGHT);
        
    }
    return self;
}

-(id)initForFooterViewWithFrame:(CGRect)frame
{
    if((self = [super initCustomWithFrame:frame])) {
        _colorBarView = [self getColoredLayer];
		_colorBarView.frame = CGRectMake(0, frame.size.height - COLOR_BAR_HEIGHT, frame.size.width, COLOR_BAR_HEIGHT);
		
        
        _colorBarView2 = [self getColoredLayer];
        _colorBarView2.frame = CGRectMake(-1 * frame.size.width, frame.size.height - COLOR_BAR_HEIGHT, frame.size.width, COLOR_BAR_HEIGHT);
    }
    return self;
}

-(CALayer *)getColoredLayer
{
    CALayer *layer = [CALayer layer];
    layer.contentsGravity = kCAGravityResize;
    layer.backgroundColor = [[UIColor blackColor] CGColor];
    #if __IPHONE_OS_VERSION_MAX_ALLOWED >= 40000
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        layer.contentsScale = [[UIScreen mainScreen] scale];
    }
    #endif
    
    CALayer *blue = [CALayer layer];
    blue.backgroundColor = [UIColorFromRGB(0x3464A3) CGColor];
    blue.shouldRasterize = YES;
    [layer addSublayer:blue];
    
    CALayer *orange = [CALayer layer];
    orange.backgroundColor = [UIColorFromRGB(0xF57900) CGColor];
    orange.shouldRasterize = YES;
    [layer addSublayer:orange];
    
    CALayer *red = [CALayer layer];
    red.backgroundColor = [UIColorFromRGB(0xCC0000) CGColor];
    red.shouldRasterize = YES;
    [layer addSublayer:red];
    
    CALayer *yellow = [CALayer layer];
    yellow.backgroundColor = [UIColorFromRGB(0xC49F00) CGColor];
    yellow.shouldRasterize = YES;
    [layer addSublayer:yellow];
    
    layer.shouldRasterize = YES;
    
    [self resizeSubLayersFor:layer];
    
    [[self layer] addSublayer:layer];
    return layer;
}


- (void)setState:(EGOPullRefreshState)aState{
    
    [super setState:aState];
    if (aState == EGOOPullRefreshLoading) {
        [self stopColorBarAnimation];
        [self startColorBarAnimation];
    } else {
        [self stopColorBarAnimation];
    }
}

-(void)startColorBarAnimation {
    CABasicAnimation* animation;
    animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(self.frame.size.width, 0, 0)];
    animation.duration = 1;
    animation.cumulative = NO;
    animation.repeatCount = FLT_MAX;
    animation.removedOnCompletion = NO;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:1];
    [_colorBarView addAnimation:animation forKey:@"move"];
    [_colorBarView2 addAnimation:animation forKey:@"move"];
    [CATransaction commit];
}
-(void)stopColorBarAnimation {
    [_colorBarView2 removeAnimationForKey:@"move"];
    [_colorBarView removeAnimationForKey:@"move"];
}


-(void)layoutSubviews
{
    [super layoutSubviews];
    
    _colorBarView.frame = CGRectMake(0, self.frame.size.height - COLOR_BAR_HEIGHT, self.frame.size.width, COLOR_BAR_HEIGHT);
    _colorBarView2.frame = CGRectMake(-1 * self.frame.size.width, self.frame.size.height - COLOR_BAR_HEIGHT, self.frame.size.width, COLOR_BAR_HEIGHT);
    
    [self resizeSubLayersFor:_colorBarView];
    [self resizeSubLayersFor:_colorBarView2];
    [self setState:_state];
}

-(void)resizeSubLayersFor:(CALayer *)layer
{
    [[layer sublayers] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CALayer *sublayer = obj;
        CGFloat width = self.frame.size.width/4;
        sublayer.frame = CGRectMake(idx * width, 0, width, COLOR_BAR_HEIGHT);
    }];
}

@end
