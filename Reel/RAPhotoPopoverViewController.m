//
//  RAPhotoPopoverViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/30/12.
//
//

#import "RAPhotoPopoverViewController.h"
#import "RAPhotoImageView.h"
#import "RALikeListController.h"

@interface RAPhotoPopoverViewController () {
    RAPhotoImageView *imageView;
}

@end

@implementation RAPhotoPopoverViewController

+(id)instanceForPhoto:(RAPhotoData *)photo withThumbnail:(UIImage *)image
{
    RAPhotoPopoverViewController *controller = [[RAPhotoPopoverViewController alloc] initWithNibName:@"RAPhotoPopoverViewController" bundle:nil];
    controller.photo = photo;
    controller.thumb = image;
    return controller;
}

-(void)closeClikedFor:(id)view
{
    [self dismissModalViewControllerAnimated:NO];
    //RALikeListController *likeList = [[RALikeListController alloc] initWithStyle:UITableViewStylePlain];
    //[self.navigationController pushViewController:likeList animated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        imageView = [[RAPhotoImageView alloc] initWithFrame:CGRectMake(0, 0, APPDELEGATE.window.frame.size.width, APPDELEGATE.window.frame.size.height)];
        imageView.delegate = self;
        [self.view addSubview:imageView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.trackedViewName = @"Photo";
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    DLog(@"PHOTO: %@", self.photo);
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    [imageView setPhoto:self.photo];
    [imageView setThumb:self.thumb];
    [imageView loadPhoto];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    imageView = nil;
    self.thumb = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [imageView rotateToOrientation:toInterfaceOrientation];
}

@end
