//
//  RAThumbnailGridView.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "RAThumbnailGridView.h"

@implementation RAThumbnailGridView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
