//
//  RAProfileSettingsTableViewController.m
//  Reel
//
//  Created by Tim Bowen on 10/17/12.
//
//

#import "RAProfileSettingsTableViewController.h"
#import "RAApiClient.h"
#import "SVProgressHUD.h"
#import "RAAccountData.h"
#import "UIImageView+AFNetworking.h"

@interface RAProfileSettingsTableViewController () 

@end

@implementation RAProfileSettingsTableViewController

@synthesize accountInfo = _accountInfo;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.accountInfo = [[NSMutableDictionary alloc] init];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_BEIGE;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:self.tableView.frame];
    self.tableView.backgroundView.backgroundColor = COLOR_BEIGE;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(submit)];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 2) {
        return 54;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cellIdentifyingString";
    RAAccountInfoTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    switch (indexPath.row) {
            DLog(@"\n\n\n\n SWITCHING YOUR CELL FOOL");
            //Title is user facing string
            //Type is param name
        case 0:
            if(cell == nil)
                cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"email"];
            cell.title = @"Email";
            [(UITextField *)cell.infoView setReturnKeyType:UIReturnKeyDone];
            [(UITextField *)cell.infoView setText:[[APPDELEGATE account] email]];
            cell.type = @"email";
            break;
        case 1:
            NSLog(@"Name (Optional)");
            if(cell == nil)
                cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"name"];
            cell.title = @"Name";
            cell.type = @"name";
            [(UITextField *)cell.infoView setReturnKeyType:UIReturnKeyDone];
            cell.optional = YES;
            if([[APPDELEGATE account] name]) {
                [(UITextField *)cell.infoView setText:[[APPDELEGATE account] name]];
            }
            break;
        case 2:
            cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"photo"];
            cell.title = @"Picture";
            cell.type = @"photo";
            cell.optional = YES;
            
            if([self.accountInfo objectForKey:@"photo"]) {
                cell.infoView = [[UIImageView alloc] initWithImage:[self.accountInfo objectForKey:@"photo"]];
            } else if([[APPDELEGATE account] avatar50]) {
                NSURL *avatarURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", APPDELEGATE.account.avatar50]];
                cell.infoView = [[UIImageView alloc] init];
                [(UIImageView *)cell.infoView setImageWithURL:avatarURL];
            }
            NSLog(@"Picture (optional");
            break;
        default:
            break;
    }
    
    NSLog(@"Returning a cell or something %@", cell);
    cell.tag = indexPath.row;
    cell.delegate = self;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)submit {
    //DO the network callin'
    
    for(NSString *key in [self.accountInfo allKeys]) {
        if([[self.accountInfo objectForKey:key] isKindOfClass:[NSString class]]
           && [[self.accountInfo objectForKey:key] isEqualToString:@""]) {
            [self.accountInfo removeObjectForKey:key];
        }
    }
    [SVProgressHUD show];
    [[RAApiClient sharedClient] post:ACCOUNT_DETAILS withPath:[NSString stringWithFormat:@"%llu", [APPDELEGATE account].itemID] data:self.accountInfo successBlock:^(NSDictionary *data) {
        DLog(@"YAY DATA %@", data);
        [APPDELEGATE updateAccountData:data];
        [SVProgressHUD showSuccessWithStatus:@""];
    } errorBlock:^(NSError *error, NSURLRequest *request){
        DLog(@"Failing to do this as per ususal %@", [error localizedDescription]);
        [SVProgressHUD showErrorWithStatus:@"Error"];
        RAAlert(@"Error", @"Your request could not be completed at this time, please try again later");
        //DLog(@"%@", [request responseString]);
    }];
}

- (void)accountInfoReady:(id)accountInfo withType:(NSString *)type atIndex:(int)index {
    DLog(@"Account info ready: %@ %i",accountInfo, index);
    [self.accountInfo setObject:accountInfo forKey:type];
    if(index >= 0) {
        [self submit];
    }
}



- (void)takeProfilePhoto {
    RACamHolderViewController *camView = [[RACamHolderViewController alloc] init];
    camView.delegate = self;
    camView.forProfilePhoto = YES;
    UINavigationController *camNav = [[UINavigationController alloc] initWithRootViewController:camView];
    [camNav setNavigationBarHidden:YES];
    [self presentModalViewController:camNav animated:YES];
}

- (void)profilePhotoDataReady:(UIImage *)photoData {
    //TODO change thumbnail
    [self.accountInfo setObject:photoData forKey:@"photo"];
    [self dismissModalViewControllerAnimated:YES];
    [self.tableView reloadData];
}

- (void)photoSaved:(BOOL)save inReel:(RAReelData *)reel {
    //Not used, but called since this delegate method is not currently optional.
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



@end
