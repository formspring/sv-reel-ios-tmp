//
//  RAActivityItemArray.m
//  Reel
//
//  Created by Stephan Soileau on 8/15/12.
//
//

#import "RAActivityItemArray.h"
#import "RAActivityItemData.h"

@implementation RAActivityItemArray
+ (NSArray *) fromHttpRequest:(ASIHTTPRequest *)request {
    NSError *e = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData: [request responseData] options: NSJSONReadingMutableContainers error: &e];
    if (!e) {
        return [RAActivityItemArray fromDict:jsonData];
    }
    return nil;
}

+ (NSArray *) fromDict:(NSDictionary *)jsonData {
    NSMutableArray *itemArray = [[NSMutableArray alloc] init];
    for (NSDictionary *jsonDict in jsonData[@"matches"]) {
        [itemArray addObject:[RAActivityItemData fromDict:jsonDict]];
    }
    return [NSArray arrayWithArray:itemArray];

    
    
    
    
    return nil;
}
@end
