//
//  RAActivityItemData.m
//  Reel
//
//  Created by Stephan Soileau on 8/15/12.
//
//

#import "RAActivityItemData.h"
#import "RAAccountData.h"
#import "RAUtils.h"

@implementation RAActivityItemData
@synthesize itemID = _itemId;
@synthesize accountId = _accountId;
@synthesize friendId = _friendId;
@synthesize targetId = _targetId;
@synthesize type = _type;
@synthesize message = _message;
@synthesize created = _created;
@synthesize friendAccount;

+ (RAActivityItemData *)fromDict:(NSDictionary *)dict {
    RAActivityItemData *item = [[RAActivityItemData alloc] init];
    
    NSDateFormatter *dateFormatter = [RAUtils getDateFormatter];
    item.itemID = [[dict objectForKey:@"id"] longLongValue];
    item.accountId = [[dict objectForKey:@"account_id"] longLongValue];
    item.friendId = [[dict objectForKey:@"friend_id"] longLongValue];
    item.targetId = [[dict objectForKey:@"target"] longLongValue];
    item.type = [dict objectForKey:@"type"];
    item.message = [dict objectForKey:@"message"];
    item.created = [dateFormatter dateFromString:[dict objectForKey:@"created"]];
    item.friendAccount = [RAAccountData fromDict:[dict objectForKey:@"friend"]];
    return item;
}
@end
