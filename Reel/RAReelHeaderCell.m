//
//  RAReelHeaderCell.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/24/12.
//
//

#import "RAReelHeaderCell.h"
#import "UIImageView+AFNetworking.h"
#import "RAPhotoData.h"

@interface RAReelHeaderCell () {
    UILabel *_usernameLabel;
    UILabel *_titleLabel;
    UIImageView *_avatarview;
    UIButton *_reelButton;
}

@end

@implementation RAReelHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _usernameLabel = [[UILabel alloc] init];
        _usernameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        _usernameLabel.textColor = UIColorFromRGB(0x7b9d1f);
        [self.contentView addSubview:_usernameLabel];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
        _titleLabel.textColor = UIColorFromRGB(0x808080);
        _titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_titleLabel];
        
        _avatarview = [[UIImageView alloc] init];
        [self.contentView addSubview:_avatarview];
        
        _reelButton = [[UIButton alloc] init];
        [[_reelButton imageView] setContentMode:UIViewContentModeCenter];
        [self.contentView addSubview:_reelButton];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    if (!APPDELEGATE.account) {
        _reelButton.hidden = YES;
    }
    
    UIImage *gridBtn;
    if (self.type == RAReelHeaderCellTypePlain) {
        gridBtn = [UIImage imageNamed:@"chevron"];
        _reelButton.userInteractionEnabled = NO;
    } else {
        gridBtn = [UIImage imageNamed:@"grid_btn"];
        [_reelButton setImage:gridBtn forState:UIControlStateSelected];
        [_reelButton setImage:gridBtn forState:UIControlStateHighlighted];
        [_reelButton addTarget:self action:@selector(reelBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    [_reelButton setImage:gridBtn forState:UIControlStateNormal];

    
    [_avatarview setFrame:CGRectMake(10, 10, 30, 30)];
    [_usernameLabel setFrame:CGRectMake(50,
                                        7,
                                        self.contentView.frame.size.width - 60 - 40,
                                        20)];
    [_titleLabel setFrame:CGRectMake(50,
                                     23,
                                     self.contentView.frame.size.width - 60 - 40,
                                     20)];
    [_reelButton setFrame:CGRectMake(self.contentView.frame.size.width - 50,
                                     0,
                                     50,
                                     50)];
}

-(void)reelBtnClicked:(id)sender
{
    if (!APPDELEGATE.account) {
        return;
    }
    [sender setImage:[UIImage imageNamed:@"grid_btn_selected"] forState:UIControlStateHighlighted];
    [self.delegate showReel:self.photo.reel];
}

-(void)setPhoto:(RAPhotoData *)photo
{
    _photo = photo;
    [_avatarview setImageWithURL:[NSURL URLWithString:self.photo.account.avatar150]];
    _usernameLabel.text = photo.account.name;
    _titleLabel.text = photo.reel.name;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
//        [self.contentView setBackgroundColor:COLOR_OLIVE];
    } else {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    }
   
}

@end
