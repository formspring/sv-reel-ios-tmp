//
//  RACommentCell.m
//  Reel
//
//  Created by Tim Bowen on 8/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RACommentCell.h"
#import "RACommentData.h"
#import "RAAccountData.h"
#import "ASIDownloadCache.h"
#import "RAUtils.h"
#import "UIImageView+AFNetworking.h"

@implementation RACommentCell

@synthesize commentData = _commentData;
@synthesize nameLabel = _nameLabel;
@synthesize dateLabel = _dateLabel;
@synthesize commentTextLabel = _commentTextLabel;


+ (UIFont *)nameLabelFont {
    return [UIFont fontWithName:@"Helvetica-Bold" size:14];
}

+ (UIFont *)commentLabelFont {
    return [UIFont fontWithName:@"Helvetica" size:12];
}

+ (UIFont *)dateLabelFont {
    return [UIFont fontWithName:@"Helvetica" size:12];
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UITapGestureRecognizer *theTapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
        [self addGestureRecognizer:theTapper];

        self.commentTextLabel = [[UILabel alloc] init];
        self.commentTextLabel.lineBreakMode = UILineBreakModeWordWrap;
        self.commentTextLabel.numberOfLines = 0;
        self.commentTextLabel.font = [RACommentCell commentLabelFont];
        
        self.nameLabel = [[UILabel alloc] init];
        self.nameLabel.font = [RACommentCell nameLabelFont];
        self.nameLabel.textColor = UIColorFromRGB(0x7b9d1f);
        self.dateLabel = [[UILabel alloc] init];
        self.dateLabel.font = [RACommentCell dateLabelFont];
        
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    int padding = 5;
    
    
    //[self addGestureRecognizer:[UITapGestureRecognizer alloc] initW]
    
    self.imageView.frame = CGRectMake(padding, padding, 40, 40);
    //self.imageView.center = CGPointMake(self.imageView.center.x, (int)self.frame.size.height/2);
    [self.contentView addSubview:self.imageView];
    //self.textLabel.frame = CGRectMake(self.imageView.frame.size.width + 2*padding, self.textLabel.frame.origin.y, self.frame.size.width - self.imageView.frame.size.width - 2* padding, self.textLabel.frame.size.height);
    CGSize constrainedNameSize = [self.commentData.commenter.name sizeWithFont:self.nameLabel.font constrainedToSize:CGSizeMake(self.frame.size.width - 2 * padding + self.imageView.frame.size.width, 10) lineBreakMode:UILineBreakModeTailTruncation];
    self.nameLabel.frame = CGRectMake(3 * padding + self.imageView.frame.size.width, padding, constrainedNameSize.width, constrainedNameSize.height);
    [self addSubview:self.nameLabel];
    //self.nameLabel.backgroundColor = [UIColor orangeColor];
    self.nameLabel.text = self.commentData.commenter.name;
    
    self.dateLabel.text = [RAUtils prettyStringForDate:self.commentData.created];
    self.dateLabel.textColor = UIColorFromRGB(0x808080);
    self.dateLabel.frame = 
    CGRectMake(self.nameLabel.frame.origin.x + padding + self.nameLabel.frame.size.width,
               self.nameLabel.frame.origin.y, 
               self.frame.size.width - (self.nameLabel.frame.origin.x + self.nameLabel.frame.size.width + padding), 
               self.nameLabel.frame.size.height);
    
    [self.contentView addSubview:self.dateLabel];
    
    //main text label - the -2 is to get the first line of text to pleasantly align with the bottom of the picture
    int yPosition = self.nameLabel.frame.origin.y + self.nameLabel.frame.size.height +2;
    NSLog(@"%d", yPosition);
    //self.nameLabel.backgroundColor = [UIColor yellowColor];
    //self.commentTextLabel.backgroundColor = [UIColor redColor];
    
    //self.commentTextLabel.backgroundColor = [UIColor purpleColor];
    
    CGSize constrainedTextSize = [self.commentData.text sizeWithFont:[RACommentCell commentLabelFont] constrainedToSize:CGSizeMake(  self.frame.size.width - self.imageView.frame.size.width - 3 * padding,
                                                                                                                                   self.frame.size.height - yPosition - padding) lineBreakMode:UILineBreakModeTailTruncation];
    
    
    
    self.commentTextLabel.frame = CGRectMake(self.nameLabel.frame.origin.x,
                                             yPosition, 
                                             constrainedTextSize.width,
                                             constrainedTextSize.height);
    self.commentTextLabel.textColor = UIColorFromRGB(0x808080);
    self.commentTextLabel.baselineAdjustment = UIBaselineAdjustmentNone;
    self.commentTextLabel.text = self.commentData.text;
    NSLog(@"%@", self.commentTextLabel);
    [self addSubview:self.commentTextLabel];
}

- (void)tapDetected:(UITapGestureRecognizer *)sender {
    CGPoint location = [(UITapGestureRecognizer *)sender locationInView:self.contentView];
    if(CGRectContainsPoint(self.imageView.frame, location)) {
        NSLog(@"lol");
        [self.delegate showProfile:self.commentData.commenter];
    }
}

- (void)setCommentData:(RACommentData *)commentData {
    _commentData = commentData;
    //self.textLabel.text = self.commentData.text;
    self.commentTextLabel.text = commentData.text;
    self.nameLabel.text = commentData.commenter.name;
    
    //Get a picture of the guy;
    NSURL *avatarURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", self.commentData.commenter.avatar50]];
    [self.imageView setImageWithURL:avatarURL];

}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.imageView.image = nil;
    self.commentTextLabel.text = @"";
    self.nameLabel.text = @"";
    self.dateLabel.text = @"";
}

@end
