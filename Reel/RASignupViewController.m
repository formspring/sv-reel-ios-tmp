//
//  RASignupViewController.m
//  Reel
//
//  Created by Tim Bowen on 10/2/12.
//
//

#import "RASignupViewController.h"
#import "RAApiClient.h"
#import "SVProgressHUD.h"
@interface RASignupViewController ()

@end

@implementation RASignupViewController

@synthesize tableView = _tableView;
@synthesize signupInfo = _signupInfo;

const int _numberOfRows = 4;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Signup";
        self.signupInfo = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)signup {
    
    NSLog(@"signup: %@", self.signupInfo);
    [SVProgressHUD show];

    [[RAApiClient sharedClient] post:ACCOUNT_DETAILS withData:self.signupInfo successBlock:^(NSDictionary * data) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:data];
        [defaults setObject:archivedData forKey:@"session"];
        [defaults synchronize];
        
        [SVProgressHUD showSuccessWithStatus:@""];
        [APPDELEGATE initializeAccount];
        [APPDELEGATE showLoggedInAppView];

        DLog(@"Data: %@", data);
    }errorBlock:^(NSError *error, NSURLRequest *request) {
        [SVProgressHUD dismiss];
        RAAlert(@"Signup Failed", [error localizedDescription]);
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _numberOfRows;
}

- (void)loadView {
    [super loadView];
    [self.signupButton setTitle:@"Sign Up" forState:UIControlStateNormal];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(signup)];
    self.navigationItem.rightBarButtonItem = doneButton;
    self.view.backgroundColor = COLOR_BEIGE;
    
    bool isiPhone5 = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size,CGSizeMake(640, 1136));
    if (!isiPhone5) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShown) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHidden) name:UIKeyboardWillHideNotification object:nil];
    }
    [self.signupButton setBackgroundImage:[[UIImage imageNamed:@"green_btn.png"]stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
    
}

- (void)viewWillUnload {
    [super viewWillUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void) keyboardShown {
    self.view.center = CGPointMake(self.view.center.x, self.view.center.y - 90);
}

- (void) keyboardHidden {
    NSLog(@"No");    self.view.center = CGPointMake(self.view.center.x, self.view.center.y + 90);

    
}

- (IBAction)signupTouched:(id)sender {
    NSLog(@"Signup touched");
    [self signup];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)fbTouched:(id)sender {
    [(RAIntroViewController *)[[self.navigationController viewControllers] objectAtIndex:0] loginWithFacebookTouched:nil];
}

- (void)accountInfoReady:(id)accountInfo withType:(NSString *)type atIndex:(int)index{
    if (accountInfo == nil || type == nil) {
        NSLog(@"You did it wrong");
        return;
    }
    NSLog(@"%@ %@", type, accountInfo);
    [self.signupInfo setObject:accountInfo forKey:type];
    if(index >= 0 && index < _numberOfRows - 1) {
        index += 1;
        [(RAAccountInfoTableCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]] activate];
    }
}

- (void)takeProfilePhoto {
    RACamHolderViewController *camView = [[RACamHolderViewController alloc] init];
    camView.delegate = self;
    camView.forProfilePhoto = YES;
    UINavigationController *camNav = [[UINavigationController alloc] initWithRootViewController:camView];
    [camNav setNavigationBarHidden:YES];
    camNav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:camNav animated:YES];
}

- (void)profilePhotoDataReady:(UIImage *)photoData {
    [self.signupInfo setObject:photoData forKey:@"photo"];
    [self dismissModalViewControllerAnimated:YES];
    [self.tableView reloadData];
    
}

- (void)photoSaved:(BOOL)save inReel:(RAReelData *)reel {
    //Not used, but called since this delegate method is not currently optional. 
}

#pragma mark UITableViewDatasource Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"I am going for a CELL RIGHT NOW");
    
    static NSString *cellID = @"CellID";
    
    RAAccountInfoTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    switch (indexPath.row) {
            DLog(@"\n\n\n\n SWITCHING YOUR CELL FOOL");
            //Title is user facing string
            //Type is param name
        case 0:
            if(cell == nil)
                cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"email"];
            cell.title = @"Email";
            cell.type = @"email";
            break;
        case 1:
            NSLog(@"Password");
            if(cell == nil) 
                cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"password"];
            cell.title = @"Password";
            cell.type = @"password";
            break;
        case 2:
            NSLog(@"Name (Optional)");
            if(cell == nil)
                cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"name"];
            cell.title = @"Name";
            cell.type = @"name";
            cell.optional = YES;
            break;
        case 3:
            cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"photo"];
            cell.title = @"Picture";
            cell.type = @"photo";
            cell.optional = YES;
            if([self.signupInfo objectForKey:@"photo"]) {
                cell.infoView = [[UIImageView alloc] initWithImage:[self.signupInfo objectForKey:@"photo"]];
            }
            NSLog(@"Picture (optional");
            break;
        default:
            break;
    }
    
    NSLog(@"Returning a cell or something %@", cell);
    cell.tag = indexPath.row;
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 3) {
        return 54;
    }
    return 44;
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"Should return? %@", textField) ;
    return NO;
}

@end
