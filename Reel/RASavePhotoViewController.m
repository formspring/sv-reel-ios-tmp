//
//  RASavePhotoViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "RASavePhotoViewController.h"
#import "RAAddReelViewController.h"
#import "RAPhotoOperationQueue.h"
#import <QuartzCore/QuartzCore.h>
#import "GTMUIImage+Resize.h"
#import "SVProgressHUD.h"
#import <FacebookSDK/FacebookSDK.h>


@interface RASavePhotoViewController ()

@end

@implementation RASavePhotoViewController
@synthesize selectReelButton;
@synthesize previewImageButton = _previewImageButton;
@synthesize shareButton = _shareButton;
@synthesize commentTextView = _commentTextView;
@synthesize reel = _reel;
@synthesize photo = _photo;
@synthesize share = _share;
@synthesize queue = _queue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reelSelected:) 
                                                     name:@"ReelSelectedForSavingPhoto"
                                                object:nil];
    }
    return self;
}

+ (id)instanceWithPhoto:(UIImage *)photo fromCamera:(BOOL)fromCamera withAssetURL:(NSURL *)url
{
    RASavePhotoViewController *instance = [[RASavePhotoViewController alloc] initWithNibName:@"RASavePhotoViewController" bundle:nil];
    instance.photo = photo;
    instance.fromCamera = fromCamera;
    instance.assetURL = url;
    return instance;
}

- (void)reelSelected:(NSNotification *)notification
{
    DLog(@"%@", [notification object]);
    self.reel = [RAReelData fromDict:[notification object]];
    [self.selectReelButton setTitle:self.reel.name forState:UIControlStateNormal];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:[notification object]];

    [defaults setObject:archivedData forKey:@"last_used_reel"];
    [defaults synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Save photo";
    self.trackedViewName = self.title;
    self.commentTextView.layer.borderColor = UIColorFromRGB(0xABABAB).CGColor;
    self.commentTextView.layer.borderWidth = 1.0;
    self.commentTextView.layer.cornerRadius = 5.0;
    self.commentTextView.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.commentTextView.layer.shadowOffset = CGSizeMake(0, 1);
    self.commentTextView.layer.shadowOpacity = 1;
    self.commentTextView.layer.shadowRadius = 1;
    self.commentTextView.clipsToBounds = NO;
    
    self.previewImageButton.layer.borderColor = UIColorFromRGB(0xBFBFBF).CGColor;
    self.previewImageButton.layer.borderWidth = 1.0;
    self.previewImageButton.layer.masksToBounds = YES;
    self.previewImageButton.layer.cornerRadius = 2.0;
    self.previewImageButton.imageView.layer.cornerRadius = 2.0;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (self.reel == nil) {
        NSData *lastReel =  [defaults objectForKey:@"last_used_reel"];
        NSDictionary *lastReelDict;
        if (lastReel == nil) {
            lastReelDict = [NSDictionary dictionaryWithObject:@"Random" forKey:@"name"];
        } else {
            lastReelDict = [NSKeyedUnarchiver unarchiveObjectWithData:lastReel];
        }

        self.reel = [RAReelData fromDict:lastReelDict];
    }
    NSNumber *shareValue = [defaults objectForKey:@"last_share_value"];
    if(![[APPDELEGATE account] hasFacebook]) {
        self.share = NO;
    }
    else if (shareValue) {
        self.share = [shareValue boolValue];        
    } else {
        self.share = YES;
    }
    
    [self.selectReelButton setTitle:self.reel.name forState:UIControlStateNormal];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage* buttonImage = [self.photo gtm_imageByResizingToSize:CGSizeMake(30, 30) preserveAspectRatio:YES trimToFit:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.previewImageButton setImage:buttonImage forState:UIControlStateNormal];
        });
    });

        
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(savePhoto)];
    self.navigationItem.rightBarButtonItem = save;
    [self setShareButtonState];
    
    //Start upload the photo
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        self.queue = [RAPhotoOperationQueue instanceForPhoto:self.photo fromCamera:self.isFromCamera];
        self.queue.assetURL = self.assetURL;
    });
    
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelSave)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [self.commentTextView becomeFirstResponder];
}

- (void)keyboardWasShown:(NSNotification *)keyboardNotification {
    CGSize keyboardSize = [[[keyboardNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect frame = self.accessoryView.frame;
    frame.origin.y = self.view.frame.size.height -  keyboardSize.height - frame.size.height - 10;
    self.accessoryView.frame = frame;
    
    CGRect commentViewFrame = self.commentTextView.frame;
    commentViewFrame.size.height = frame.origin.y - commentViewFrame.origin.y - 10;
    self.commentTextView.frame = commentViewFrame;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)setShareButtonState
{
    self.shareButton.selected = self.share;
}

-(void)savePhoto
{
    [self.delegate saveCompleted];
    if (![self.queue savePhotoToReel:self.reel comment:self.commentTextView.text share:self.share]) {
        RAAlert(@"Error", @"Upload failed");
    }
    [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"Photo"
                                                       withAction:@"Save"
                                                        withLabel:nil
                                                        withValue:nil];
}

-(void)cancelSave
{
    [self.delegate saveCancelled];
    [self.queue cancelAllOperations];
    [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"Photo"
                                                       withAction:@"Cancel"
                                                        withLabel:nil
                                                        withValue:nil];
}

- (void)viewDidUnload
{
    [self setSelectReelButton:nil];
    [self setPreviewImageButton:nil];
    [self setReel:nil];
    [self setPhoto:nil];
    [self setShareButton:nil];
    [self setCommentTextView:nil];
    [self setQueue:nil];
    [self setDelegate:nil];
    [self setAccessoryView:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    DLog(@"IMA HERE DEALLOCING");
}

- (IBAction)selectReelButtonTouched:(id)sender {
    RAAddReelViewController *addReelView = [[RAAddReelViewController alloc] initWithStyle:UITableViewStyleGrouped];
    addReelView.selectedReel = self.reel;
    [self.navigationController pushViewController:addReelView animated:YES];
}

- (IBAction)shareButtonTouched:(id)sender {
    if(![[APPDELEGATE account] hasFacebook]) {
        [SVProgressHUD show];
        [FBSession openActiveSessionWithPermissions:[NSArray arrayWithObjects:@"publish_actions", @"email", @"user_birthday", nil]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState status,
                                                      NSError *error) {
                                      
                                      if (session.isOpen) {
                                          NSDictionary *data = [NSDictionary dictionaryWithObject:session.accessToken forKey:@"facebook_token"];
                                          [[RAApiClient sharedClient] post:ACCOUNT_DETAILS withPath:[NSString stringWithFormat:@"%llu", [[APPDELEGATE account] itemID]]  data:data
                                                              successBlock: ^(NSDictionary *data) {
                                                                  [SVProgressHUD showSuccessWithStatus:@"OK"];
                                                                  NSLog(@"%@", data);
                                                                  self.share = YES;
                                                                  [self setShareButtonState];
                                                                  [APPDELEGATE updateAccountData:data];
                                                              } errorBlock:^(NSError *error, NSURLRequest *request) {
                                                                  [SVProgressHUD showErrorWithStatus:@""];
                                                                  RAAlert(@"Error", @"We could not connect your Facebook account.");
                                                              }];
                                      } else {
                                          [SVProgressHUD showErrorWithStatus:@""];
                                          RAAlert(@"Error",
                                                  @"Facebook connect failed");
                                          
                                          [[[GAI sharedInstance] defaultTracker] trackEventWithCategory:@"App"
                                                                                             withAction:@"Error"
                                                                                              withLabel:@"Facebook Connect Settings"
                                                                                              withValue:nil];
                                      }
                                  }];



        return;
    }
    self.share = !self.share;
    [self setShareButtonState];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:self.share] forKey:@"last_share_value"];
    [defaults synchronize];
}
@end
