//
//  RAThumbnailGridViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RAReelData.h"
#import "RAPhotoPopoverViewController.h"
#import "GAITrackedViewController.h"
#import "MWPhotoBrowser.h"

@interface RAThumbnailGridViewController : GAITrackedViewController <UIScrollViewDelegate, MWPhotoBrowserDelegate>

@property(strong) RAReelData *reel;
@property(strong) RAPhotoPopoverViewController *photoController;
@property(strong, nonatomic) RAAccountData *account;

+(id)controllerForReel:(RAReelData *)reel;
+(id)instanceForAccount:(RAAccountData*)account;

@end
