//
//  RAActivityTableCell.m
//  Reel
//
//  Created by Stephan Soileau on 8/16/12.
//
//

#import "RAActivityTableCell.h"
#import "RAAccountData.h"
#import "RAActivityItemData.h"

@interface RAActivityTableCell ()
- (UIFont *)activityFont;
@end

@implementation RAActivityTableCell
@synthesize friendAvatarView = _friendAvatarView;
@synthesize feedItem = _feedItem;
@synthesize delegate = _delegate;

- (UIFont *)activityFont {
    return [UIFont fontWithName:@"Helvetica" size:14];
}

- (id) initWIthItem:(RAActivityItemData *)item andReuseIdentifier:(NSString *)reuseIdentifier {
    self.feedItem = item;
    return [self initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.friendAvatarView = [[UIImageView alloc] init];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect contentFrame = self.contentView.frame;
    int padding;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    [self addGestureRecognizer:tapRecognizer];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        padding = 5;
    } else {
        padding = 10;
    }
    self.friendAvatarView.frame = CGRectMake(padding * 1.5, padding, contentFrame.size.height - (2 * padding), contentFrame.size.height - (2 * padding));
    
    DLog(@"%@", self.friendAvatarView);
    
    self.textLabel.center = CGPointMake(self.textLabel.center.x + self.frame.size.height - padding, self.textLabel.center.y);
    self.detailTextLabel.center = CGPointMake(self.detailTextLabel.center.x + self.frame.size.height - padding, self.detailTextLabel.center.y);
    self.detailTextLabel.frame = CGRectMake(self.detailTextLabel.frame.origin.x,
                                            self.detailTextLabel.frame.origin.y,
                                            contentFrame.size.width - self.friendAvatarView.frame.size.width - (3*padding),
                                            self.detailTextLabel.frame.size.height);

    [self.contentView addSubview:self.friendAvatarView];
}

- (void)tapDetected:(UITapGestureRecognizer *)sender {
    CGPoint location = [(UITapGestureRecognizer *)sender locationInView:self.contentView];
    DLog(@"%@", self.feedItem.type);

    if(CGRectContainsPoint(self.friendAvatarView.frame, location)) {
        //Show profile
        [self.delegate showProfile:self.feedItem.friendAccount];
    } else {
        //[self.delegate showPhoto:<#(RAPhotoData *)#>]
        //show photo
        NSRange range = [self.feedItem.type rangeOfString:@"photo"];
        if(range.location != NSNotFound) {
            //show photo
            [self.delegate showPhotoWithID:self.feedItem.targetId];
        } else {
            //How many actions do we actually have?
            //Follow currently has no type
            [self.delegate showProfile:self.feedItem.friendAccount];
        }
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.feedItem = nil;
    self.friendAvatarView.image = nil;
}

@end
