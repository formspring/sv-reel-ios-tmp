//
//  RAApiClient.m
//  Reel
//
//  Created by Stephan Soileau on 8/14/12.
//
//

#import "RAApiClient.h"
#import "NSDictionary+QueryStringBuilder.h"
#import "Reachability.h"
#import "RAAppDelegate.h"
#import "RAIntroViewController.h"
#import "AFJSONRequestOperation.h"

NSString *const DEV_HTTP_PASSWD = @"knowledge original spread disappear";
NSString *const DEV_HTTP_USER = @"ios-dev";


//NSString *const API_BASE = @"http://app.dev.staff.makereel.com:1081/";
NSString *const API_BASE = @"https://internal-api.makereel.com/";
//NSString *const API_BASE = @"https://dev.staff.formspring.me/~tim/reel/default/app/";

NSString *const PHOTO_BASE_URL = @"http://s3.amazonaws.com/photos.dev.reel/";

static RAApiClient *_sharedClient = nil;

@interface RAApiClient ()
-(NSString*)absoluteURLForEndPoint:(NSString *)endpoint withPath:(NSString*)path andParam:(NSDictionary *)params;
@end

@implementation RAApiClient
@synthesize sessionID = _sessionID;

#define TEST_SESSION_ID @"111-3a0cc992-e7d3-11e1-b896-123140ff3e83"

-(id)init
{
    if (self = [super init]) {
        self.reachability = [Reachability reachabilityForInternetConnection];
    }
    return self;
}

+ (id)sharedClient {
    if (_sharedClient == nil) {
        @synchronized(self) {
            [ASIHTTPRequest setDefaultTimeOutSeconds:30];
            _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:API_BASE]];
        }
    }
    return _sharedClient;
}

-(void) setSession:(NSString *)sessionID {
    self.sessionID = sessionID;
    [self setDefaultHeader:@"Session-Id" value:self.sessionID];
}

-(void) makeRequest:(API_ENDPOINT) endpoint withPath:(NSString*)path data:(NSDictionary *)data queryParams:(NSDictionary*)params method:(NSString*)method successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    __block NSMutableURLRequest *request;
    NSString *rpath = [self getPath:endpoint withPath:path];
    DLog(@"%@", rpath);
    
    for(NSString *key in [data allKeys]) {
        if([[data objectForKey:key] isKindOfClass:[UIImage class]]) {
            NSData *imageData = UIImageJPEGRepresentation([data objectForKey:key], 1);
            request = [self multipartFormRequestWithMethod:method path:rpath parameters:data constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
                [formData appendPartWithFileData:imageData name:key fileName:[NSString stringWithFormat:@"%@.png", key] mimeType:@"image/png"];
            }];
            data = [NSMutableDictionary dictionaryWithDictionary:data];
            [(NSMutableDictionary *)data removeObjectForKey:key];
        }
    }
    if(request == nil) {
        if ([method isEqual:@"POST"]) {
            request = [self requestWithMethod:method path:rpath parameters:data];
        } else {
            request = [self requestWithMethod:method path:rpath parameters:params];
        }
    }
    [self setAuthorizationHeaderWithUsername:DEV_HTTP_USER password:DEV_HTTP_PASSWD];
    
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        successBlock(JSON);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIViewController *rootViewController = [APPDELEGATE window].rootViewController;
        if ([response statusCode] == 401 &&  [rootViewController class] != [RAIntroViewController class] && self.sessionID != nil) {
            self.sessionID = nil;
            [APPDELEGATE showIntroView:YES];
        }
        if ([JSON objectForKey:@"error_message"]) {
            NSError *RAError = [NSError errorWithDomain:@"com.makereel.reel" code:[response statusCode] userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[JSON objectForKey:@"error_message"], NSLocalizedDescriptionKey, nil]];
            errorBlock(RAError, request);
        } else {
            errorBlock(error, request);
        }
    }];
    [self enqueueHTTPRequestOperation:requestOperation];
}

-(void) get:(API_ENDPOINT)endpoint successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    [self makeRequest:endpoint withPath:nil data:nil queryParams:nil method:@"GET" successBlock:successBlock errorBlock:errorBlock];
}

-(void) get:(API_ENDPOINT)endpoint withQueryParams:(NSDictionary*)params successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    [self makeRequest:endpoint withPath:nil data:nil queryParams:params method:@"GET" successBlock:successBlock errorBlock:errorBlock];
}

-(void)get:(API_ENDPOINT)endpoint withPath:(NSString *)path queryParams:(NSDictionary *)params successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    [self makeRequest:endpoint withPath:path data:nil queryParams:params method:@"GET" successBlock:successBlock errorBlock:errorBlock];
}

-(void) post:(API_ENDPOINT)endpoint withData:(NSDictionary *)data successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    [self makeRequest:endpoint withPath:nil data:data queryParams:nil method:@"POST" successBlock:successBlock errorBlock:errorBlock];
}

-(void) put:(API_ENDPOINT)endpoint withData:(NSDictionary *)data successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    [self makeRequest:endpoint withPath:nil data:data queryParams:nil method:@"PUT" successBlock:successBlock errorBlock:errorBlock];
}

-(void) post:(API_ENDPOINT)endpoint withPath:(NSString*)path data:(NSDictionary *)data successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    [self makeRequest:endpoint withPath:path data:data queryParams:nil method:@"POST" successBlock:successBlock errorBlock:errorBlock];
}


-(void) delete:(API_ENDPOINT)endpoint successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    [self makeRequest:endpoint withPath:nil data:nil queryParams:nil method:@"DELETE" successBlock:successBlock errorBlock:errorBlock];
}

-(void) delete:(API_ENDPOINT)endpoint withPath:(NSString*)path queryParams:(NSDictionary*)params successBlock:(SuccessBlock)successBlock errorBlock:(ErrorBlock)errorBlock {
    [self makeRequest:endpoint withPath:path data:nil queryParams:params method:@"DELETE" successBlock:successBlock errorBlock:errorBlock];
}


- (NSString *)getPath:(API_ENDPOINT) endpoint withPath:(NSString*)path {
    switch (endpoint) {
        case ACCOUNT_DETAILS:
            return [self absoluteURLForEndPoint:@"account" withPath:path andParam:nil];
        case ACCOUNT_PHOTOS:
            return [self absoluteURLForEndPoint:@"account/photos" withPath:path andParam:nil];
        case ACCOUNT_REELS:
            return [self absoluteURLForEndPoint:@"account/reels" withPath:path andParam:nil];
        case ACCOUNT_SETTINGS:
            return [self absoluteURLForEndPoint:@"account/settings" withPath:path andParam:nil];
        case FOLLOW:
            return [self absoluteURLForEndPoint:@"follow" withPath:path andParam:nil];
        case HOME_FEED:
            return [self absoluteURLForEndPoint:@"account/feed" withPath:path andParam:nil];
        case SESSION:
            return [self absoluteURLForEndPoint:@"session" withPath:path andParam:nil];
        case PHOTO:
            return [self absoluteURLForEndPoint:@"photo" withPath:path andParam:nil];
        case ACTIVITY_FEED:
            return [self absoluteURLForEndPoint:@"account/activity" withPath:path andParam:nil];
        case PHOTO_LIKE:
            return  [self absoluteURLForEndPoint:@"photolike" withPath:path andParam:nil];
        case PHOTO_COMMENT:
            return [self absoluteURLForEndPoint:@"photocomment" withPath:path andParam:nil];
        case REEL:
            return [self absoluteURLForEndPoint:@"reel" withPath:path andParam:nil];
        case REEL_POPULAR:
            return [self absoluteURLForEndPoint:@"reel/popular" withPath:path andParam:nil];
        case REMINDER:
            return [self absoluteURLForEndPoint:@"reminder" withPath:path andParam:nil];
        case REMINDER_FEED:
            return [self absoluteURLForEndPoint:@"account/reminders" withPath:path andParam:nil];
        case REGISTER_UUID:
            return [self absoluteURLForEndPoint:@"session/register" withPath:path andParam:nil];
        case PASSWORD_RESET:
            return [self absoluteURLForEndPoint:@"account/password_reset" withPath:path andParam:nil];
    }
}
                    
-(NSString*)absoluteURLForEndPoint:(NSString *)endpoint withPath:(NSString*)path andParam:(NSDictionary *)params {
    NSString *queryString;
    if (params != nil) {
        queryString = [params queryString];
    } else {
        queryString = @"";
    }
    NSString *pathToAppend;
    if (path != nil) {
        pathToAppend = [NSString stringWithFormat:@"/%@", path];
    } else {
        pathToAppend = @"";
    }
    return [NSString stringWithFormat:@"%@%@%@", endpoint, pathToAppend, queryString];
}

    
@end
