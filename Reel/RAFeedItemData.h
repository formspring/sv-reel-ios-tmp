//
//  RAFeedItemData.h
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RAAccountData;
@class RAReelData;
@class RAPhotoData;

@interface RAFeedItemData : NSObject <RADataObject>

@property (strong, nonatomic) RAAccountData *account;
@property (assign) int commentCount;
@property (strong, nonatomic) NSDate *created;
@property (assign) int likeCount;
@property (strong, nonatomic) RAReelData *reel;
@property (assign) unsigned long long itemID;
@property (strong, nonatomic) NSString *url150;
@property (strong, nonatomic) NSString *url150l;
@property (strong, nonatomic) NSString *url300;
@property (strong, nonatomic) NSString *url300l;
@property (strong, nonatomic) NSString *url600;
@property (strong, nonatomic) NSString *url600l;
@property (strong, nonatomic) NSString *urlOriginal;
@property (assign) BOOL hasLiked;
@property (strong, nonatomic) RAPhotoData *photo;
@property (strong, nonatomic) NSString *imageUrlString;

+ (RAFeedItemData *)fromDict:(NSDictionary *)dict;

@end
