//
//  RACommentData.m
//  Reel
//
//  Created by Tim Bowen on 8/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RACommentData.h"
#import "RAAccountData.h"
#import "RAUtils.h"

@implementation RACommentData


@synthesize commenter = _commenter;
@synthesize created = _created;
@synthesize itemID = _commentID;
@synthesize photoID = _photoID;
@synthesize text = _text;

/*
 //Example JSON:
{
 "account_id" = 100137;
 commenter =     {
     birthday = "1984-09-25";
     created = "2012-08-21 02:01:02";
     email = "timfbowen@gmail.com";
     "facebook_id" = 545368624;
     friends = "";
     gender = male;
     id = 100137;
     "last_used" = "2012-08-20 19:00:50";
     locale = "en_US";
     name = "Timothy Bowen";
     "notification_settings" = "";
     photo = "account/20120820/5032ebd138b6b0.08898993.original.jpg";
     "photo_urls" =         {
         150x150 = "https://s3.amazonaws.com/photos.dev.reel/account/20120820/5032ebd138b6b0.08898993.150x150.jpg";
         50x50 = "https://s3.amazonaws.com/photos.dev.reel/account/20120820/5032ebd138b6b0.08898993.50x50.jpg";
         original = "https://s3.amazonaws.com/photos.dev.reel/account/20120820/5032ebd138b6b0.08898993.original.jpg";
    };
    username = timfbowen81716;
 };
 "commenter_id" = 100137;
 created = "2012-08-21 02:15:51";
 id = 20813;
 "photo_id" = 182743;
 text = "This is comment text";
}
 
 */

+ (RACommentData *)fromDict:(NSDictionary *)dict {
    
    NSMutableDictionary *dictionary = [RAUtils sanitizedDictionary:dict];
    NSLog(@"dict: %@", dictionary);
    RACommentData *commentData = [[RACommentData alloc] init];
    commentData.commenter = [RAAccountData fromDict:[dictionary objectForKey:@"commenter"]];
    commentData.created =[[RAUtils getDateFormatter] dateFromString:[dictionary objectForKey:@"created"]];
    commentData.itemID = [[dictionary objectForKey:@"id"] longLongValue];
    commentData.photoID = [[dictionary objectForKey:@"photo_id"] longLongValue];
    commentData.text = [dictionary objectForKey:@"text"];
    return commentData;
}

+ (RACommentData *)fromHttpRequest:(ASIHTTPRequest *)request  {
    
    NSError *e = nil;
    id jsonData = [NSJSONSerialization JSONObjectWithData: [request responseData] options: NSJSONReadingMutableContainers error: &e];
    if (!e) {
        return [RACommentData fromDict:jsonData];
    }
    return nil;
}

@end
