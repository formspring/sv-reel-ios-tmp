//
//  RAFriendActivityData.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/11/12.
//
//

#import "RAFriendActivityData.h"
#import "RAAccountData.h"
#import "RAPhotoData.h"
#include <stdlib.h>
#import <OHAttributedLabel/NSAttributedString+Attributes.h>
#import <OHAttributedLabel/OHASBasicHTMLParser.h>
#import <OHAttributedLabel/OHASBasicMarkupParser.h>
#import "RAUtils.h"

@interface RAFriendActivityData () {
    NSAttributedString *_activityText;
}

@end

@implementation RAFriendActivityData

+(id) fromDict:(NSDictionary *)dict
{
    RAFriendActivityData *instance = [[RAFriendActivityData alloc] init];
    instance.type = RAFriendActivityTypeLike;

    NSArray *photos = dict[@"photos"];
    NSMutableArray *photosData = [NSMutableArray arrayWithCapacity:[photos count]];
    [photos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [photosData addObject:[RAPhotoData fromDict:obj]];
    }];
    instance.photos = [NSArray arrayWithArray:photosData];

    NSArray *actors = dict[@"actors"];
    NSMutableArray *actorsData = [NSMutableArray arrayWithCapacity:[actors count]];
    [actors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [actorsData addObject:[RAAccountData fromDict:obj]];
    }];
    instance.actors = [NSArray arrayWithArray:actorsData];
    instance.verb = dict[@"verb"];
    
    if (NILIFNULL(dict[@"account"]) != nil) {
        instance.account = [RAAccountData fromDict:dict[@"account"]];
    }

    instance.groupedPhotoArray = [RAUtils groupedPhotosForStyle:instance.photos];
    
    return instance;
}

-(unsigned long long)itemID
{
    return 0;
}

-(NSAttributedString *)activityText
{
    if (_activityText != nil) {
        return _activityText;
    }
    NSMutableArray *textComponents = [NSMutableArray array];
    [self.actors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ((self.actors.count > 1 && idx == self.actors.count - 1) || (self.actors.count > 2 && idx == 2)) {
            [textComponents addObject:@"<font color='#bbb'>and</font>"];
        }
        if (self.actors.count > 3 && idx == 2) {
            int remainder = self.actors.count - 2;
            NSString *es = @"";
            if (remainder > 0) {
                es = @"s";
            }
            [textComponents addObject:[NSString stringWithFormat:@"<font color='#7b9d1f'><b>%d other%@</b></font>", remainder, es]];
            *stop = YES;
        } else {
            RAAccountData *account = [self.actors objectAtIndex:idx];
            if (self.actors.count > 2 && idx == 0) {
                [textComponents addObject:[NSString stringWithFormat:@"<font color='#7b9d1f'><b>%@</b></font><font color='#bbb'>,</font>", account.name]];
            } else {
                [textComponents addObject:[NSString stringWithFormat:@"<font color='#7b9d1f'><b>%@</b></font>", account.name]];
            }
        }
    }];
    [textComponents addObject:[NSString stringWithFormat:@"<font color='#bbb'>%@</font>", self.verb]];
    if (self.photos.count > 1) {
        [textComponents addObject:[NSString stringWithFormat:@"<font color='#bbb'>%d photos</font>", self.photos.count]];
    } else {
        [textComponents addObject:[NSString stringWithFormat:@"<font color='#bbb'>a photo by </font><font color='#7b9d1f'><b>%@</b></font>", self.account.name]];
    }
    NSMutableAttributedString *tmpString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithString:[textComponents componentsJoinedByString:@" "]]];
    [tmpString setFontName:@"Helvetica" size:14];
    _activityText = [OHASBasicHTMLParser attributedStringByProcessingMarkupInAttributedString:tmpString];
    return _activityText;
}

@end
