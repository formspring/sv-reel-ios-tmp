//
//  RAThumbnailGridViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "RAThumbnailGridViewController.h"
#import "GMGridView.h"
#import "SVProgressHUD.h"
#import "RAPhotoData.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+AFNetworking.h"
#import <SDWebImage/SDImageCache.h>
#import "RARefreshTableHeaderView.h"
#import "RAPhotoActionViewController.h"
#import "RAPhotoBrowser.h"

#define EMPTY_VIEW_TAG 156

@interface RAThumbnailGridViewController () <GMGridViewDataSource, GMGridViewTransformationDelegate, GMGridViewActionDelegate>
{
    __gm_weak GMGridView *_gmGridView;
    NSMutableArray *_photos;
    NSInteger _lastDeleteItemIndexAsked;
    
    NSString *_urlPath;
    API_ENDPOINT _endpoint;
    
    RARefreshTableHeaderView *_headerRefreshView;
    BOOL _loading;
}
-(void)fetchDataWithParams:(NSDictionary *)params;
@end

@implementation RAThumbnailGridViewController

@synthesize reel = _reel;

+(id)controllerForReel:(RAReelData *)reel
{
    RAThumbnailGridViewController *controller = [[RAThumbnailGridViewController alloc] init];
    controller.reel = reel;
    return controller;
}

+(id)instanceForAccount:(RAAccountData *)account
{
    RAThumbnailGridViewController *controller = [[RAThumbnailGridViewController alloc] init];
    controller.account = account;
    return controller;
}

- (id)init
{
    self = [super initWithNibName:@"RAThumbnailGridViewController" bundle:nil];
    return self;
}

-(void)loadView
{
    [super loadView];
    NSInteger spacing = INTERFACE_IS_PHONE ? 5 : 15;
    

    
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:self.view.bounds];
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:gmGridView];
    _gmGridView = gmGridView;
    
    _gmGridView.bounces = YES;
    _gmGridView.style = GMGridViewStyleSwap;
    _gmGridView.itemSpacing = spacing;
    _gmGridView.minEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    _gmGridView.centerGrid = YES;
    _gmGridView.actionDelegate = self;
    _gmGridView.transformDelegate = self;
    _gmGridView.dataSource = self;
    if (self.parentViewController == [self.parentViewController.navigationController.viewControllers objectAtIndex:0]) {
        _gmGridView.enableEditOnLongPress = YES;
    } else {
        _gmGridView.enableEditOnLongPress = NO;
    }
    _gmGridView.centerGrid = NO;
    _gmGridView.backgroundColor = COLOR_BEIGE;
    _gmGridView.delegate = self;
    _gmGridView.minimumPressDuration = 1;

    _headerRefreshView = [[RARefreshTableHeaderView alloc] initForFooterViewWithFrame:CGRectMake(0, - COLOR_BAR_HEIGHT, self.view.frame.size.width, COLOR_BAR_HEIGHT)];
    [_gmGridView addSubview:_headerRefreshView];


}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_headerRefreshView setState:EGOOPullRefreshLoading];
    [_gmGridView setContentInset:UIEdgeInsetsMake(COLOR_BAR_HEIGHT, 0, 0, 0)];
    if (self.reel != nil) {
        _endpoint = REEL;
        _urlPath = [NSString stringWithFormat:@"%llu/photos", self.reel.itemID];
        self.title = self.reel.name;
        self.trackedViewName = @"Reel";
    }
    else if (self.account.itemID > 0) {
        _endpoint = ACCOUNT_DETAILS;
        _urlPath = [NSString stringWithFormat:@"%lld/photos", self.account.itemID];
        if (self.account.itemID == APPDELEGATE.account.itemID) {
            self.title = @"Me";
        } else {
            self.title = self.account.name;
        }
    }
    else {
        _endpoint = ACCOUNT_PHOTOS;
        _urlPath = nil;
    }

    _gmGridView.mainSuperView = self.navigationController.view; //[UIApplication sharedApplication].keyWindow.rootViewController.view;
    _photos = [NSMutableArray array];
    [_gmGridView addObserver:self forKeyPath:@"editing" options:NSKeyValueObservingOptionNew  context:NULL];
    if (self.parentViewController == [self.parentViewController.navigationController.viewControllers objectAtIndex:0]) {
        self.parentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(setEdittingModeOff:)];;
    }

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_photos.count == 0) {
        [self fetchDataWithParams:nil];
    }
}

-(void)fetchDataWithParams:(NSDictionary *)params
{
    if (_loading) {
        return;
    }
    if([_photos count] == 0) {
        UIImageView *emptyView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"empty_profile.png"]];
        emptyView.tag = EMPTY_VIEW_TAG;
        emptyView.center = CGPointMake(self.view.frame.size.width /2, 138);
        emptyView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self.view addSubview:emptyView];
    }
    _loading = YES;
    SuccessBlock block = ^(NSDictionary *data) {
        DLog(@"%@", data);
        NSArray *photoDicts = [NSMutableArray arrayWithArray:data[@"matches"]];
        [photoDicts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [_photos addObject:[RAPhotoData fromDict:obj]];
        }];
        if (photoDicts.count > 0) {
            [_gmGridView reloadData];            
        }
        [_gmGridView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        [_headerRefreshView setHidden:YES];
        _loading = NO;
        [[self.view viewWithTag:EMPTY_VIEW_TAG] removeFromSuperview];
    };


    
    
    
    [[RAApiClient sharedClient] get:_endpoint
                           withPath:_urlPath
                        queryParams:params
                       successBlock:block
                         errorBlock:^(NSError *error, NSURLRequest *request) {
                             [_gmGridView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
                             RAAlert(@"Error", [error localizedDescription]);
                             [_headerRefreshView setHidden:YES];
                             _loading = NO;
                         }
     ];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    _gmGridView = nil;
    self.account = nil;
}

- (void)dealloc
{
    [_gmGridView removeObserver:self forKeyPath:@"editing" context:NULL];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [_photos count];
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    //NSLog(@"Creating view indx %d", index);
    
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    if (!cell)
    {
        cell = [[GMGridViewCell alloc] init];
        cell.deleteButtonIcon = [UIImage imageNamed:@"delete_x.png"];
        cell.deleteButtonOffset = CGPointMake(0, 0);
        UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"photo_cell.png"]];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, size.width - 10, size.height - 10)];
        [view addSubview:imageView];
        view.clipsToBounds = YES;
        imageView.clipsToBounds = YES;
        cell.contentView = view;
    }
    
    UIImageView *imageView = [(UIImageView *)cell.contentView subviews][0];
    RAPhotoData *photo = [_photos objectAtIndex:index];
    [imageView setImageWithURL:[NSURL URLWithString:photo.url150]];
    return cell;
}



- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (INTERFACE_IS_PHONE)
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(100, 100);
        }
        else
        {
            return CGSizeMake(100, 100);
        }
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(285, 205);
        }
        else
        {
            return CGSizeMake(230, 175);
        }
    }
}

- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return self.account.itemID == APPDELEGATE.account.itemID;
}

#pragma mark DraggableGridViewTransformingDelegate

- (CGSize)GMGridView:(GMGridView *)gridView sizeInFullSizeForCell:(GMGridViewCell *)cell atIndex:(NSInteger)index inInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (INTERFACE_IS_PHONE)
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(320, 210);
        }
        else
        {
            return CGSizeMake(300, 310);
        }
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(700, 530);
        }
        else
        {
            return CGSizeMake(600, 500);
        }
    }
}

- (UIView *)GMGridView:(GMGridView *)gridView fullSizeViewForCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    UIView *fullView = [[UIView alloc] init];
    fullView.backgroundColor = [UIColor yellowColor];
    fullView.layer.masksToBounds = NO;
    fullView.layer.cornerRadius = 8;
    
    CGSize size = [self GMGridView:gridView sizeInFullSizeForCell:cell atIndex:index inInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    fullView.bounds = CGRectMake(0, 0, size.width, size.height);
    
    UILabel *label = [[UILabel alloc] initWithFrame:fullView.bounds];
    label.text = [NSString stringWithFormat:@"Fullscreen View for cell at index %d", index];
    label.textAlignment = UITextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if (INTERFACE_IS_PHONE)
    {
        label.font = [UIFont boldSystemFontOfSize:15];
    }
    else
    {
        label.font = [UIFont boldSystemFontOfSize:20];
    }
    
    [fullView addSubview:label];
    
    
    return fullView;
}

- (void)GMGridView:(GMGridView *)gridView didStartTransformingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor blueColor];
                         cell.contentView.layer.shadowOpacity = 0.7;
                     }
                     completion:nil];
}

- (void)GMGridView:(GMGridView *)gridView didEndTransformingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor redColor];
                         cell.contentView.layer.shadowOpacity = 0;
                     }
                     completion:nil];
}

- (void)GMGridView:(GMGridView *)gridView didEnterFullSizeForCell:(UIView *)cell
{
    
}


//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    
    RAPhotoBrowser *browser = [[RAPhotoBrowser alloc] initWithRAPhotoArray:_photos];
    [browser setInitialPageIndex:position];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
}


- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count) {
        RAPhotoData *photo = _photos[index];
        return [MWPhoto photoWithRAPhotoData:photo];
    }
    return nil;
}

- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
    RAPhotoData *photo = _photos[index];
    RAPhotoActionViewController *photoAction = [[RAPhotoActionViewController alloc] initWithNibName:@"RAPhotoActionViewController" bundle:nil];
    photoAction.photo = photo;
    return (MWCaptionView *)photoAction.view;
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm" message:@"Are you sure you want to delete this item?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
    
    [alert show];
    _lastDeleteItemIndexAsked = index;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        __block RAPhotoData *photo = [_photos objectAtIndex:_lastDeleteItemIndexAsked];
        __block NSUInteger index = _lastDeleteItemIndexAsked;
        [[RAApiClient sharedClient] delete:PHOTO withPath:[NSString stringWithFormat:@"%lld", photo.itemID] queryParams:nil successBlock:^(NSDictionary *data) {
            DLog(@"SUCCESS DELETING FILE");
        } errorBlock:^(NSError *error, NSURLRequest *request) {
            RAAlert([error localizedDescription], [error localizedFailureReason]);
            if (index >= [_photos count]) {
                index = [_photos count] - 1;
            }
            [_photos insertObject:photo atIndex:index];
            [_gmGridView insertObjectAtIndex:index withAnimation:GMGridViewItemAnimationFade];
        }];
        [_photos removeObjectAtIndex:_lastDeleteItemIndexAsked];
        [_gmGridView removeObjectAtIndex:_lastDeleteItemIndexAsked withAnimation:GMGridViewItemAnimationFade];
        
    }
}


#pragma mark UIScrollviewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
   if (_gmGridView.contentSize.height > self.view.bounds.size.height + 100
       && _gmGridView.contentOffset.y >= _gmGridView.contentSize.height - (self.view.bounds.size.height * 2)
       && [_photos count] > 0
       && !_loading) {
       RAPhotoData *lastPhoto = [_photos lastObject];
       [self fetchDataWithParams:@{@"max_id": [NSString stringWithFormat:@"%lld", lastPhoto.itemID]}];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (_gmGridView.editing) {
        self.parentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(setEdittingModeOff:)];
    } else {
        self.parentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(setEdittingModeOff:)];;
    }
}

-(void)setEdittingModeOff:(id)sender
{
    _gmGridView.editing = !_gmGridView.editing;
}

@end
