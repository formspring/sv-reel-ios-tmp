//
//  RATabbyToolBarThing.h
//  Reel
//
//  Created by Stephan Soileau on 8/15/12.
//
//

#import <UIKit/UIKit.h>

@interface RATabToolbar : UIToolbar
-(void)withItems:(NSArray *)items;
@end
