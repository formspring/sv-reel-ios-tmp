//
//  RAActivityTableCell.h
//  Reel
//
//  Created by Stephan Soileau on 8/16/12.
//
//

#import <UIKit/UIKit.h>

@class RAActivityItemData;
@class RAPhotoData;

@protocol RAActivityCellDelegate <NSObject>

- (void)showProfile:(RAAccountData *)account;
- (void)showPhotoWithID:(unsigned long long)photoID;
@end

@interface RAActivityTableCell : UITableViewCell
@property (strong, nonatomic) UIImageView *friendAvatarView;
@property (strong, nonatomic) RAActivityItemData *feedItem;
@property (weak, nonatomic) id delegate;

- (id) initWIthItem:(RAActivityItemData *)item andReuseIdentifier:(NSString *)reuseIdentifier;

@end
