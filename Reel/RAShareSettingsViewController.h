//
//  RAShareSettingViewController.h
//  Reel
//
//  Created by Ade Olonoh on 9/8/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "JMStaticContentTableViewController.h"

@interface RAShareSettingsViewController : JMStaticContentTableViewController

@end
