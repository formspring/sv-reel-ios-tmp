//
//  RACamHolderViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/11/12.
//
//

#import <UIKit/UIKit.h>
#import "RACamViewController.h"
#import "RACamPreviewViewController.h"
#import "RASavePhotoViewController.h"

@class RAReelData;

@protocol RACamHolderViewDelegate <NSObject>

-(void)photoSaved:(BOOL)save inReel:(RAReelData*) reel;

@optional
- (void)profilePhotoDataReady:(UIImage *)photoData;

@end

@interface RACamHolderViewController : UIViewController <RACamViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RACamPreviewViewDelegate, RASavePhotoViewDelegate>

@property (strong, nonatomic) RACamViewController *camViewController;
@property (strong, nonatomic) UIImage *photo;

@property(assign, getter=isNewUser) BOOL newUser;
@property(strong, nonatomic) RAReminderData *reminder;
@property(strong, nonatomic) id<RACamHolderViewDelegate> delegate;
@property (assign) BOOL forProfilePhoto;


@end
