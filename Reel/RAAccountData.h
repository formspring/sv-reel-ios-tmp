//
//  RAAccountData.h
//  Reel
//
//  Created by Stephan Soileau on 8/14/12.
//
//

#import <Foundation/Foundation.h>
#import "RADataObject.h"

@class RAReminderData;

@interface RAAccountData : NSObject <RADataObject>
    
@property (assign) unsigned long long itemID;
@property (strong, nonatomic) NSString *username;
@property (assign, nonatomic) unsigned long long facebookID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSArray *friends;
@property (strong, nonatomic) NSDate *created;
@property (strong, nonatomic) NSString *avatar150;
@property (strong, nonatomic) NSString *avatar50;
@property (strong, nonatomic) NSString *avatarOriginal;
@property (assign, getter=isFollowing) BOOL following;
@property (strong, nonatomic) RAReminderData *reminder;
@property (assign, getter = isNew) BOOL newAccount;
@property (assign) BOOL hasFacebook;

+ (id) fromDict:(NSDictionary *)dict;
+ (id) fromHttpRequest:(ASIHTTPRequest *)request;
    
    

@end
