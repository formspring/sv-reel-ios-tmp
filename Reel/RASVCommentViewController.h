//
//  RASVCommentViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/13/12.
//
//

#import "RAPaginatingTableViewController.h"
#import "RACommentCell.h"

@interface RASVCommentViewController : RAPaginatingTableViewController <RACommentCellDelegate>

@property (nonatomic, strong) id <RACommentCellDelegate> cellDelegate;

@end
