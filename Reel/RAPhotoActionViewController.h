//
//  RAPhotoActionViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/30/12.
//
//

#import <UIKit/UIKit.h>
#import "RAPhotoData.h"
#import <MessageUI/MessageUI.h>

@protocol RAPhotoActionDelegate <NSObject>

-(void)likeListClickedForPhoto:(RAPhotoData *)photo;
-(void)commentClickedForPhoto:(RAPhotoData *)photo;
@optional
-(void)accountClickedForPhoto:(RAPhotoData *)photo;

@end

@interface RAPhotoActionViewController : UIViewController <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *likeIconButton;
@property (strong, nonatomic) IBOutlet UIButton *commentButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) RAPhotoData *photo;
@property (strong, nonatomic) id<RAPhotoActionDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *avatarView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *reelLabel;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *footerView;

- (IBAction)likeIconButtonClicked:(id)sender;
- (IBAction)likeListButtonClicked:(id)sender;
- (IBAction)commentButtonClicked:(id)sender;
- (IBAction)shareButtonClicked:(id)sender;
-(void)setupUI;

@end
