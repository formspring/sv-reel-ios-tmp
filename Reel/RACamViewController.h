//
//  RACamViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/11/12.
//
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import "AVCamCaptureManager.h"

@class AVCamCaptureManager, AVCamPreviewView, AVCaptureVideoPreviewLayer, RAReminderData;

@protocol RACamViewDelegate <UIImagePickerControllerDelegate>
-(void)showPhotoLibrary;
@end

@interface RACamViewController : GAITrackedViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,strong) AVCamCaptureManager *captureManager;
@property (nonatomic,strong) IBOutlet UIView *videoPreviewView;
@property (nonatomic,strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic,strong) IBOutlet UIButton *cameraToggleButton;
@property (nonatomic,strong) IBOutlet UIButton *flashToggleButton;
@property (nonatomic,strong) IBOutlet UIButton *takePictureButton;
@property (strong, nonatomic) IBOutlet UIView *photoBar;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *switchToLibraryButton;
@property (strong, nonatomic) id<RACamViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *infoButton;

@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UILabel *infoTitleLabel;
@property (strong, nonatomic) IBOutlet UITextView *infoTextView;

@property(assign, getter=isNewUser) BOOL newUser;
@property(strong, nonatomic) RAReminderData *reminder;
@property(assign, getter = isInfoHidden) BOOL infoHidden;
@property(assign) AVCaptureVideoOrientation orientation;
@property (strong, nonatomic) IBOutlet UIView *accessoryView;

-(void)stopCamera;
-(void)startCamera;

@end
