//
//  RAPhotoBrowser.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/8/12.
//
//

#import "RAPhotoBrowser.h"
#import "MWPhoto.h"
#import "RACaptionView.h"
#import "RALikesAndCommentsViewController.h"
#import "UINavigationController+RATransition.h"
#import "RAProfileViewController.h"

@interface RAPhotoBrowser () {
    NSArray *_photoDataArray;
}

@end

@implementation RAPhotoBrowser

-(id)initWithRAPhotoArray:(NSArray *)photoArray
{
    if (self = [super initWithDelegate:self]) {
        _photoDataArray = photoArray;
        self.wantsFullScreenLayout = YES;
        self.displayActionButton = NO;
    }
    return self;
}

-(id)initWithRAPhoto:(RAPhotoData *)photo
{
    if (self = [self initWithRAPhotoArray:@[photo]]) {

    }
    return self;
}

-(MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index
{
    RAPhotoData *photo = _photoDataArray[index];
    RAPhotoActionViewController *photoAction = [[RAPhotoActionViewController alloc] initWithNibName:@"RAPhotoActionViewController" bundle:nil];
    photoAction.photo = photo;
    photoAction.delegate = self;
    RACaptionView *view = (RACaptionView *)photoAction.view;
    view.delegate = photoAction;
    
    return view;
}

-(id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    return [MWPhoto photoWithRAPhotoData:_photoDataArray[index]];
}

-(NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [_photoDataArray count];
}

-(void)likeListClickedForPhoto:(RAPhotoData *)photo
{
    RALikesAndCommentsViewController *vc = [RALikesAndCommentsViewController instanceForPhoto:photo];
    vc.presentedAsModal = YES;
    [self.navigationController pushController:vc withTransition:UIViewAnimationTransitionFlipFromLeft];
}

-(void)commentClickedForPhoto:(RAPhotoData *)photo
{
    RALikesAndCommentsViewController *vc = [RALikesAndCommentsViewController instanceForPhoto:photo];
    vc.presentedAsModal = YES;
    vc.showComments = YES;
    [self.navigationController pushController:vc withTransition:UIViewAnimationTransitionFlipFromLeft];
}

-(void)accountClickedForPhoto:(RAPhotoData *)photo
{
    RAProfileViewController *vc = [RAProfileViewController instanceForAccount:photo.account];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)doneButtonPressed:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];

//    [self.presentingViewController dismissModalViewControllerAnimated:YES];
}

-(void)dealloc
{
    DLog(@"DEALOCCING RA PHOTO BROWSER");
}

@end
