//
//  RACommentViewController.h
//  Reel
//
//  Created by Tim Bowen on 8/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
#import "RACommentCell.h"
#import "GAITrackedViewController.h"

@class RAPaginatingTableViewController;
@class RAPhotoData;
@class HPGrowingTextView;


@interface RACommentViewController : GAITrackedViewController<HPGrowingTextViewDelegate, UITableViewDelegate, UITableViewDataSource, RACommentCellDelegate>

@property (strong, nonatomic) RAPhotoData *feedItem;
@property (strong, nonatomic) NSMutableArray *commentArray;
@property (strong, nonatomic) HPGrowingTextView *textView;
@property (strong, nonatomic) UIImageView *avatarView;
@property (strong, nonatomic) UIView *textViewContainer;
@property (strong, nonatomic) RAPaginatingTableViewController *tableViewController;
@property (strong, nonatomic) UIImageView *textViewMask;
@property (strong, nonatomic) UIButton *postButton;

- (id)initWithStyle:(UITableViewStyle)style forFeedItem:(RAPhotoData *)item;



@end
