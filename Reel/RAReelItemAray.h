//
//  RAReelItemAray.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//
//

#import <Foundation/Foundation.h>
#import "RADataObject.h"

@interface RAReelItemAray : NSObject

+ (NSMutableArray *) fromJSONData:(id)jsonData;

@end
