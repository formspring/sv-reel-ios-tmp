//
//  RALoggedoutExploreViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 10/24/12.
//
//

#import "RAExploreViewController.h"

@interface RALoggedoutExploreViewController : UIViewController

@property (nonatomic, strong) RAExploreViewController *exploreVC;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *signupBtn;
- (IBAction)loginTouched:(id)sender;
- (IBAction)signupTouched:(id)sender;

@end
