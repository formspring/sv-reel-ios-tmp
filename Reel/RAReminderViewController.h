//
//  RAReminderViewController.h
//  Reel
//
//  Created by Tim Bowen on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RAPaginatingTableViewController.h"
#import "RACamHolderViewController.h"

@interface RAReminderViewController : RAPaginatingTableViewController <UIActionSheetDelegate, RACamHolderViewDelegate>

- (void)showCameraForReminder:(RAReminderData *)reminderItem;

@property (strong, nonatomic) RACamHolderViewController *camHolder;

@end
