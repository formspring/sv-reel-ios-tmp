//
//  RALikesAndCommentsViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/9/12.
//
//

#import "RALikesAndCommentsViewController.h"
#import "RAPhotoData.h"
#import "RALikeListController.h"
#import "RACommentViewController.h"
#import "UINavigationController+RATransition.h"

@interface RALikesAndCommentsViewController ()

@end

@implementation RALikesAndCommentsViewController

+(id)instanceForPhoto:(RAPhotoData *)photo
{
    RALikesAndCommentsViewController *instance = [[RALikesAndCommentsViewController alloc] initWithNibName:@"RALikesAndCommentsViewController" bundle:nil];
    instance.photo = photo;
    return instance;
}

-(void)viewDidLoad
{
    self.wantsFullScreenLayout = NO;
    if (self.photo.likeCount == 0) {
        self.showComments = YES;
        self.title = @"Comments";
    } else {
        self.navigationItem.titleView = self.segmentedControl;
        self.title = @"Comments";
    }
    
    if (self.shouldShowComments) {
        self.segmentedControl.selectedSegmentIndex = 1;
    }
    UIViewController *vc = [self viewControllerForSegmentIndex:self.segmentedControl.selectedSegmentIndex];
    [self addChildViewController:vc];
    vc.view.frame = self.contentView.bounds;
    [self.contentView addSubview:vc.view];
    self.currentViewController = vc;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    if (self.isPresentedAsModal) {
        [self.navigationItem setHidesBackButton:YES animated:NO];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(goBack:)];
        self.navigationItem.rightBarButtonItem = backButton;
    }
}

-(void)goBack:(id)sender
{
    [self.navigationController popControllerWithTransition:UIViewAnimationTransitionFlipFromLeft];
}

- (UIViewController *)viewControllerForSegmentIndex:(NSInteger)index {
    UIViewController *vc;
    switch (index) {
        case 0:
            vc = [[RALikeListController alloc] initWithStyle:UITableViewStylePlain forFeedItem:self.photo];
            break;
        case 1:
            vc = [[RACommentViewController alloc] initWithStyle:UITableViewStylePlain forFeedItem:self.photo];
            break;
    }
    return vc;
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender {
    UIViewController *vc = [self viewControllerForSegmentIndex:sender.selectedSegmentIndex];
    [self addChildViewController:vc];
    
    [self.currentViewController.view removeFromSuperview];
    vc.view.frame = self.contentView.bounds;
    [self.contentView addSubview:vc.view];
    [vc didMoveToParentViewController:self];
    [self.currentViewController removeFromParentViewController];
    self.currentViewController = vc;
    self.navigationItem.title = vc.title;
}

@end
