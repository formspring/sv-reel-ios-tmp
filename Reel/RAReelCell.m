//
//  RAReelCell.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//
//

#import "RAReelCell.h"
#import "RAPhotoData.h"
#import "UIImageView+AFNetworking.h"
#import <QuartzCore/QuartzCore.h>

const int PADDING = 10;
const float ASPECT_FACTOR = 0.47;

@interface RAReelCell ()
{
    
    UIButton *_nameLabel;
}

-(UIImageView *)getImageView;

@end

@implementation RAReelCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        int numOfPreviews = [[reuseIdentifier substringFromIndex:([reuseIdentifier length] - 1)] intValue];

        
        UIImageView *bgView = (UIImageView*)self.backgroundView;
        bgView.contentMode = UIViewContentModeBottom;
        UIImageView *sbgView = (UIImageView*)self.selectedBackgroundView;
        sbgView.contentMode = UIViewContentModeBottom;
        

        if (numOfPreviews > 2) {
            [bgView setImage:[UIImage imageNamed:@"reel_cell_preview"]];
            [sbgView setImage:[UIImage imageNamed:@"reel_cell_preview_selected"]];
        }
    }
    return self;
}


-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.photos.count > 2) {
        self.imageViewOne.frame = CGRectMake(PADDING, 10, 300, 140);
        self.imageViewTwo.frame = CGRectMake(PADDING, 0 + 156, 147, 140);
        self.imageViewThree.frame = CGRectMake(PADDING + 147 + 6, 0 + 156, 147, 140);
    }

}



-(UIImageView *)getImageView
{
    UIImageView *view = [[UIImageView alloc] init];
    view.contentMode = UIViewContentModeScaleAspectFill;
    view.clipsToBounds = YES;
    [self.contentView addSubview:view];
    return view;
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


@end
