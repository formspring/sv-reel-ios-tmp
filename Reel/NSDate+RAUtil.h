//
//  NSDate+RAUtil.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/4/12.
//
//

#import <Foundation/Foundation.h>

@interface NSDate (RAUtil)

-(NSString *)timeAgo;
    
@end
