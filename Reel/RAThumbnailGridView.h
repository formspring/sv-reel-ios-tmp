//
//  RAThumbnailGridView.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "GMGridView.h"

@interface RAThumbnailGridView : GMGridView

@end
