//
//  UINavigationBar+RADropShadow.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/4/12.
//
//

#import "UINavigationBar+RADropShadow.h"
#import <QuartzCore/QuartzCore.h>

@implementation UINavigationBar (RADropShadow)

-(void)willMoveToWindow:(UIWindow *)newWindow{
    [super willMoveToWindow:newWindow];
    if (SYSTEM_VERSION_LESS_THAN(@"6.0")) {
        [self applyDefaultStyle];   
    }
}

- (void)applyDefaultStyle {
    // add the drop shadow
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.layer.shadowRadius = 2.0;
    self.layer.shadowOpacity = 0.5;
    self.layer.shouldRasterize = YES;
    self.clipsToBounds = NO;
}

@end
