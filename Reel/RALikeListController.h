//
//  RALikeListController.h
//  Reel
//
//  Created by Tim Bowen on 9/4/12.
//
//

#import <UIKit/UIKit.h>
#import "RAPaginatingTableViewController.h"

@class RAAccountData;
@class RAPhotoData;



@interface RALikeListController : RAPaginatingTableViewController

@property (strong, nonatomic) RAPhotoData *feedItem;

- (id)initWithStyle:(UITableViewStyle)style forFeedItem:(RAPhotoData *)item;

@end
