//
//  RAReelItemAray.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//
//

#import "RAReelItemAray.h"
#import "RAReelData.h"

@implementation RAReelItemAray

+ (NSMutableArray *) fromJSONData:(id)jsonData
{
    DLog(@"%@", jsonData);

    NSMutableArray *feedItemArray = [[NSMutableArray alloc] init];
    for (NSDictionary *jsonDict in jsonData[@"matches"]) {
        [feedItemArray addObject:[RAReelData fromDict:jsonDict]];
    }
    return feedItemArray;
}

@end
