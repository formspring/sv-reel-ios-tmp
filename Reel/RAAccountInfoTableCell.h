//
//  RAAccountInfoTableCell.h
//  Reel
//
//  Created by Tim Bowen on 10/4/12.
//
//

#import <UIKit/UIKit.h>

@protocol RAAccountInfoDelegate <NSObject>

@required
- (void)accountInfoReady:(id)accountInfo withType:(NSString *)type atIndex:(int)index;

@optional
- (void)takeProfilePhoto;

@end

@interface RAAccountInfoTableCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIView *infoView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, assign) BOOL optional;
@property (nonatomic, weak) id <RAAccountInfoDelegate> delegate;

- (void)activate;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier forType:(NSString *)theType;

@end
