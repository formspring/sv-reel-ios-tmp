//
//  RAGroupedPhotosCell.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/11/12.
//
//

#import "RAGroupedPhotosCell.h"
#import "RAPhotoData.h"
#import "UIImageView+AFNetworking.h"
#import <QuartzCore/QuartzCore.h>


@interface RAGroupedPhotosCell () {
    NSArray *_imageViews;
}

@end

@implementation RAGroupedPhotosCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.opaque = YES;
        int numOfPreviews = [[reuseIdentifier substringFromIndex:([reuseIdentifier length] - 1)] intValue];
        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reel_cell_1.png"]];
        bgView.contentMode = UIViewContentModeTop;
        bgView.clipsToBounds = YES;
        
        UIImageView *bgView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reel_cell_2.png"]];
        bgView2.contentMode = UIViewContentModeTop;
        bgView2.clipsToBounds = YES;
        
        UIImageView *bgView3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reel_cell_3.png"]];
        bgView3.contentMode = UIViewContentModeTop;
        bgView3.clipsToBounds = YES;
        
        UIImageView *sbgiew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reel_cell_1_selected.png"]];
        sbgiew.contentMode = UIViewContentModeTop;
        sbgiew.clipsToBounds = YES;
        
        UIImageView *sbgView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reel_cell_2_selected.png"]];
        sbgView2.contentMode = UIViewContentModeTop;
        sbgView2.clipsToBounds = YES;
        
        UIImageView *sbgView3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reel_cell_3_selected.png"]];
        sbgView3.contentMode = UIViewContentModeTop;
        sbgView3.clipsToBounds = YES;

        switch (numOfPreviews) {
            case 1:
                _imageViewOne = [self getImageView];
                _imageViews = [NSArray arrayWithObject:_imageViewOne];
                _deleteButtonOne = [self getDeleteButton];
                self.backgroundView = bgView;
                self.selectedBackgroundView = sbgiew;
                break;
            case 2:
                _imageViewOne = [self getImageView];
                _imageViewTwo = [self getImageView];
                _imageViews = [NSArray arrayWithObjects:_imageViewOne, _imageViewTwo, nil];
                _deleteButtonOne = [self getDeleteButton];
                _deleteButtonTwo = [self getDeleteButton];
                self.backgroundView = bgView2;
                self.selectedBackgroundView = sbgView2;
                break;
            case 3:
                _imageViewOne = [self getImageView];
                _imageViewTwo = [self getImageView];
                _imageViewThree = [self getImageView];
                _imageViews = [NSArray arrayWithObjects:_imageViewOne, _imageViewTwo, _imageViewThree, nil];
                _deleteButtonOne = [self getDeleteButton];
                _deleteButtonTwo = [self getDeleteButton];
                _deleteButtonThree = [self getDeleteButton];
                self.backgroundView = bgView3;
                self.selectedBackgroundView = sbgView3;
                break;
            default:
                break;
        }
    }
    return self;
}

-(void)setPhotos:(NSArray *)photos
{
    _photos = photos;

    if ([self.photos count]) {
        if ([self.photos count] == 1) {
            RAPhotoData *photo = [self.photos objectAtIndex:0];
            [_imageViewOne setImageWithURL:[NSURL URLWithString:photo.url600x600l]];
            
        } else if([self.photos count] == 2) {
            RAPhotoData *photo = [self.photos objectAtIndex:0];
            [_imageViewOne setImageWithURL:[NSURL URLWithString:photo.url300x300l]];
            
            RAPhotoData *photo2 = [self.photos objectAtIndex:1];
            [_imageViewTwo setImageWithURL:[NSURL URLWithString:photo2.url300x300l]];
            
        } else if([self.photos count] > 2) {
            RAPhotoData *photo = [self.photos objectAtIndex:0];
            [_imageViewOne setImageWithURL:[NSURL URLWithString:photo.url600x600l]];
            RAPhotoData *photo2 = [self.photos objectAtIndex:1];
            [_imageViewTwo setImageWithURL:[NSURL URLWithString:photo2.url150]];
            RAPhotoData *photo3 = [self.photos objectAtIndex:2];
            [_imageViewThree setImageWithURL:[NSURL URLWithString:photo3.url150]];
        }
    }
}

-(UIImageView *)getImageView
{
    UIImageView *view = [[UIImageView alloc] init];
    view.userInteractionEnabled = YES;
    view.contentMode = UIViewContentModeScaleAspectFill;
    view.clipsToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
    [view addGestureRecognizer:tap];
    [self.contentView addSubview:view];
    return view;
}

-(UIButton *)getDeleteButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:@"delete_photo_off"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"delete_photo"] forState:UIControlStateSelected];
    button.userInteractionEnabled = NO;
    [self.contentView addSubview:button];
    return button;
}

-(void)positionButton:(UIButton *)button forImageView:(UIImageView *)imageView
{
    CGRect origFrame = CGRectMake(0, 0, 30, 30);
    origFrame.origin.x = imageView.frame.origin.x + imageView.frame.size.width - 40;
    origFrame.origin.y = imageView.frame.origin.y + imageView.frame.size.height - 40;
    button.frame = origFrame;
}

-(void)imageViewClicked:(UIGestureRecognizer*)gesture
{
    if (!APPDELEGATE.account) {
        return;
    }
    CGPoint location = [(UITapGestureRecognizer *)gesture locationInView:self.contentView];
    if(CGRectContainsPoint(self.imageViewOne.frame, location)) {
        self.selectedPhoto = [self.photos objectAtIndex:0];
    } else if (CGRectContainsPoint(self.imageViewTwo.frame, location)) {
        self.selectedPhoto = [self.photos objectAtIndex:1];
    } else if(CGRectContainsPoint(self.imageViewThree.frame, location)) {
        self.selectedPhoto = [self.photos objectAtIndex:2];
    }
    if (self.editing) {
        self.selectedPhoto.selected = !self.selectedPhoto.selected;
        //[self.delegate photo:self.selectedPhoto selected:!self.selectedPhoto forCell:self];
        [self layoutSubviews];
    } else {
        [self.delegate photoClicked:self.selectedPhoto forCell:self];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    if (self.isPhotoSelectable) {
        UITouch *touch  = [touches anyObject];
        CGPoint location = [touch locationInView:self.contentView];
        if(CGRectContainsPoint(self.imageViewOne.frame, location)) {
            self.imageViewOne.layer.opacity = 0.5;
        } else if (CGRectContainsPoint(self.imageViewTwo.frame, location)) {
            self.imageViewTwo.layer.opacity = 0.5;
        } else if(CGRectContainsPoint(self.imageViewThree.frame, location)) {
            self.imageViewThree.layer.opacity = 0.5;
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    if (self.isPhotoSelectable) {
        self.imageViewOne.layer.opacity = 1;
        self.imageViewTwo.layer.opacity = 1;
        self.imageViewThree.layer.opacity = 1;
    }
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    if (self.isPhotoSelectable) {
        self.imageViewOne.layer.opacity = 1;
        self.imageViewTwo.layer.opacity = 1;
        self.imageViewThree.layer.opacity = 1;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self layoutSubviews];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGRect frame = self.contentView.frame;
    frame.origin.x = 0;
    frame.size.width = self.frame.size.width;
    self.contentView.frame = frame;

    if (!self.selectable) {
        _deleteButtonOne.hidden = YES;
        _deleteButtonTwo.hidden = YES;
        _deleteButtonThree.hidden = YES;
    } else {
        [_deleteButtonOne setHidden:!self.editing];
        [_deleteButtonTwo setHidden:!self.editing];
        [_deleteButtonThree setHidden:!self.editing];
    }
    [[_imageViewOne layer] setOpacity:(_deleteButtonOne.selected || !self.editing ? 1 : 0.5)];
    [[_imageViewTwo layer] setOpacity:(_deleteButtonTwo.selected || !self.editing ? 1 : 0.5)];
    [[_imageViewThree layer] setOpacity:(_deleteButtonThree.selected || !self.editing ? 1 : 0.5)];

    const int PADDING = 10;
    int width = self.contentView.frame.size.width;
    int topMargin = self.cellType == RAGroupedPhotosCellTypeEdgeTop ? 10 : 3;
    
    UIImageView *bgView = (UIImageView*)self.backgroundView;
    UIImageView *sbgView = (UIImageView*)self.selectedBackgroundView;
    if (self.cellType == RAGroupedPhotosCellTypeEdgeTop) {
        bgView.contentMode = UIViewContentModeTop;
        sbgView.contentMode = UIViewContentModeTop;
    } else if (self.cellType == RAGroupedPhotosCellTypeEdgeBottom) {
        bgView.contentMode = UIViewContentModeBottom;
        sbgView.contentMode = UIViewContentModeBottom;
    } else {
        bgView.contentMode = UIViewContentModeCenter;
        sbgView.contentMode = UIViewContentModeCenter;
    }
    
    switch (_photos.count) {
        case 1:
            [_imageViewOne setFrame:CGRectMake(PADDING, topMargin, width - (2*PADDING), 140)];
            [self positionButton:_deleteButtonOne forImageView:_imageViewOne];
            [_deleteButtonOne setSelected:[[_photos objectAtIndex:0] selected]];
            break;
        case 2:
            [_imageViewOne setFrame:CGRectMake(PADDING, topMargin, 147, 140)];
            [_imageViewTwo setFrame:CGRectMake(163, topMargin, 147, 140)];
            [self positionButton:_deleteButtonOne forImageView:_imageViewOne];
            [self positionButton:_deleteButtonTwo forImageView:_imageViewTwo];
            [_deleteButtonOne setSelected:[[_photos objectAtIndex:0] selected]];
            [_deleteButtonTwo setSelected:[[_photos objectAtIndex:1] selected]];
            break;
        case 3:
            [_imageViewOne setFrame:CGRectMake(PADDING, topMargin, 96, 140)];
            [_imageViewTwo setFrame:CGRectMake(112, topMargin, 96, 140)];
            [_imageViewThree setFrame:CGRectMake(214, topMargin, 96, 140)];
            [self positionButton:_deleteButtonOne forImageView:_imageViewOne];
            [self positionButton:_deleteButtonTwo forImageView:_imageViewTwo];
            [self positionButton:_deleteButtonThree forImageView:_imageViewThree];
            [_deleteButtonOne setSelected:[[_photos objectAtIndex:0] selected]];
            [_deleteButtonTwo setSelected:[[_photos objectAtIndex:1] selected]];
            [_deleteButtonThree setSelected:[[_photos objectAtIndex:2] selected]];
            break;
        default:
            break;
    }

}

-(void)prepareForReuse
{
    [super prepareForReuse];
}


@end
