//
//  RAReelViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//
//

#import <UIKit/UIKit.h>
#import "RAPaginatingTableViewController.h"
#import "RAReelCell.h"

@class RAAccountData;

@interface RAReelViewController : RAPaginatingTableViewController <RAGroupedPhotoCellDelegate>

@property (strong, nonatomic) RAAccountData *account;

+(id)instanceForAccount:(RAAccountData *)account;

@end
