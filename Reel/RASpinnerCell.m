//
//  RASpinnerCell.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "RASpinnerCell.h"

@interface RASpinnerCell ()
@property (retain) UIActivityIndicatorView *spinner;
@end

@implementation RASpinnerCell
@synthesize spinner = _spinner;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.spinner.frame = CGRectMake(self.contentView.frame.origin.x, 
                                    self.contentView.frame.origin.y, 
                                    self.contentView.frame.size.width, 
                                    self.contentView.frame.size.height);
    [self.contentView addSubview:self.spinner];
}

@end
