//
//  RAActivityItemArray.h
//  Reel
//
//  Created by Stephan Soileau on 8/15/12.
//
//

#import <Foundation/Foundation.h>
#import "RADataObject.h"


@interface RAActivityItemArray : NSObject <RADataObject>

+ (NSArray *) fromHttpRequest:(ASIHTTPRequest *)request;
+ (NSArray *) fromDict:(NSDictionary *)jsonData;
@end
