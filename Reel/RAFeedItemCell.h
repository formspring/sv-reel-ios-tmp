//
//  RAFeedItemCell.h
//  Reel
//
//  Created by Tim Bowen on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RAPhotoActionViewController.h"

@class ASIHTTPRequest;
@class RAReelData;
@class RAPhotoData;
@class RAPhotoOperationQueue;
@class YLProgressBar;
@class DDProgressView;

@protocol RAFeedItemCellDelegate <NSObject>

@required
- (void)likeClicked:(RAPhotoData *)feedItem atIndexPath:(NSIndexPath *)indexPath;
- (void)commentClicked:(RAPhotoData *)feedItem;
- (void)showProfile:(RAAccountData *)account;
- (void)showReel:(RAReelData *)reel;
- (void)showLikeList:(RAPhotoData *)feedItem;
- (void)showPhoto:(RAPhotoData *)photo;
@end


@interface RAFeedItemCell : UITableViewCell <RAPhotoActionDelegate>


@property (strong, nonatomic) RAPhotoData *feedItem;

@property (strong, nonatomic) UIImageView *photoView;
@property (strong, nonatomic) UIButton *heartView;
@property (strong, nonatomic) UIButton *likeButton;
@property (strong, nonatomic) UIButton *commentButton;
@property (strong, nonatomic) UILabel *timestampLabel;
@property (strong, nonatomic) UILabel *likeLabel;
@property (strong, nonatomic) UILabel *commentLabel;
@property (strong, nonatomic) UIView *optionsContainer;
@property (weak, nonatomic) id<RAFeedItemCellDelegate> delegate;
@property (assign, nonatomic, getter = isUploading) BOOL uploading;
@property (strong, nonatomic) DDProgressView *progressView;
@property (strong, nonatomic) RAPhotoOperationQueue *photoQueue;
@property (strong, nonatomic) RAPhotoActionViewController *photoActionVC;


@property (strong, nonatomic) NSIndexPath *indexPath;

- (id)initWithFeedItem:(RAPhotoData *)feedItemData andReuseIdentifier:(NSString *)reuseIdentifier;

@end
