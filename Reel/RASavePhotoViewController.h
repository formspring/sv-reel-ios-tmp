//
//  RASavePhotoViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RAReelData.h"
#import "RAPhotoOperationQueue.h"
#import "GAITrackedViewController.h"

@protocol RASavePhotoViewDelegate <NSObject>

-(void)saveCancelled;
-(void)saveCompleted;

@end

@interface RASavePhotoViewController : GAITrackedViewController

@property (strong, nonatomic) UIImage *photo;
@property (strong, nonatomic) RAReelData *reel;
@property (strong, nonatomic) IBOutlet UIButton *selectReelButton;
@property (strong, nonatomic) IBOutlet UIButton *previewImageButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UITextView *commentTextView;
@property (nonatomic, assign) BOOL share;
@property (assign, nonatomic) RAPhotoOperationQueue *queue;
@property (strong, nonatomic) id<RASavePhotoViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *accessoryView;
@property (assign, nonatomic, getter = isFromCamera) BOOL fromCamera;
@property (strong, nonatomic) NSURL *assetURL;


- (IBAction)selectReelButtonTouched:(id)sender;
- (IBAction)shareButtonTouched:(id)sender;
+ (id)instanceWithPhoto:(UIImage *)photo fromCamera:(BOOL)fromCamera withAssetURL:(NSURL*)url;
@end
