//
//  RACommentData.h
//  Reel
//
//  Created by Tim Bowen on 8/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RAAccountData;

@interface RACommentData : NSObject <RADataObject>

+ (RACommentData *)fromDict:(NSDictionary *)dict;
+ (RACommentData *)fromHttpRequest:(ASIHTTPRequest *)request;

@property (strong, nonatomic) RAAccountData *commenter;
@property (strong, nonatomic) NSDate *created;
@property (assign) unsigned long long itemID;
@property (assign) unsigned long long photoID;
@property (strong, nonatomic) NSString *text;

@end
