//
//  RAAddReelViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import "RAAddReelViewController.h"
#import "UIActivityIndicatorView+Reel.h"
#import "SVProgressHUD.h"

@interface RAAddReelViewController ()
@property (strong, nonatomic) UIBarButtonItem *saveButton;
@property (strong, nonatomic) UITextField *reelTextField;
@property (strong, nonatomic) NSArray *myReels;
@property (strong, nonatomic) NSArray *popularReels;
@end

@implementation RAAddReelViewController
@synthesize saveButton = _saveButton;
@synthesize reelTextField = _reelTextField;
@synthesize myReels = _myReels;
@synthesize popularReels = _popularReels;
@synthesize selectedReel = _selectedReel;

-(void)viewDidLoad
{
    [super viewDidLoad];
    //self.tableView.backgroundColor = COLOR_BEIGE;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:self.tableView.frame];
    self.tableView.backgroundView.backgroundColor = COLOR_BEIGE;
    
    self.title = @"Add to reel";
    self.saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleDone target:self action:@selector(addNewReel)];

    
    self.reelTextField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 185, 30)];
    self.reelTextField.adjustsFontSizeToFitWidth = YES;
    self.reelTextField.placeholder = @"New Reel";
    self.reelTextField.returnKeyType = UIReturnKeyDone;
    self.reelTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    //            reelTextField.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
    self.reelTextField.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
    self.reelTextField.textAlignment = UITextAlignmentLeft;
    self.reelTextField.tag = 0;
    self.reelTextField.clearButtonMode = UITextFieldViewModeWhileEditing; // no clear 'x' button to the right
    [self.reelTextField setEnabled: YES];
        
    [self.reelTextField setDelegate:self];
    [self.reelTextField addTarget:self action:@selector(reelTextFielddDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self addSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.reuseIdentifier = @"TextFieldCell";
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setAccessoryType:UITableViewCellAccessoryNone];

            CGRect textFieldFrame = self.reelTextField.frame;
            textFieldFrame.origin.y = (cell.contentView.frame.size.height - 31)/2;
            textFieldFrame.origin.x = 20;
            textFieldFrame.size.height = 31;
            textFieldFrame.size.width = cell.contentView.frame.size.width - 20;
            self.reelTextField.frame = textFieldFrame;

            [cell addSubview:self.reelTextField];
        }];
    }];
    
    [self addSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
        section.title = @"My reels";
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.reuseIdentifier = @"SpinnerCell";
            [UIActivityIndicatorView addSpinnerTo:cell];
        }];
    }];
    
    [self addSection:^(JMStaticContentTableViewSection *section, NSUInteger sectionIndex) {
        section.title = @"Popular Reels";
        [section addCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            staticContentCell.reuseIdentifier = @"SpinnerCell";
            [UIActivityIndicatorView addSpinnerTo:cell];
        }];
    }];
    

    [[RAApiClient sharedClient] get:ACCOUNT_REELS successBlock:^(NSDictionary *data) {
        self.myReels = data[@"matches"];
        [self displayMyReels];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        RAAlert(@"Unable to fetch reels", [error localizedDescription]);
    }];

    [[RAApiClient sharedClient] get:REEL_POPULAR successBlock:^(NSDictionary *data) {
        self.popularReels = data[@"matches"];
        [self displayPopularReels];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        RAAlert(@"Unable to fetch reels", [error localizedDescription]);
    }];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[[GAI sharedInstance] defaultTracker] trackView:@"Add to Reel"];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

-(void)displayMyReels
{
    JMStaticContentTableViewSection *section = [self sectionAtIndex:1];
    [section removeAllCells];
    if (self.myReels.count == 0) {
        [self removeSectionAtIndex:1];
    }
    [self.myReels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) { 
        [section insertCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            cell.textLabel.text = [obj valueForKey:@"name"];
            RAReelData *reel = [RAReelData fromDict:obj];
            if ([reel.name isEqual:self.selectedReel.name] &&
                (reel.itemID > 0 && reel.itemID == self.selectedReel.itemID)) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        } whenSelected:^(NSIndexPath *indexPath) {
            [self reelSelectedAtIndexPath:indexPath];
        } atIndexPath:[NSIndexPath indexPathForRow:idx inSection:1] animated:NO];
    }];
}

-(void)displayPopularReels
{
    JMStaticContentTableViewSection *section = [[self staticContentSections] lastObject];
    [section removeAllCells];
    [self.popularReels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {    
        [section insertCell:^(JMStaticContentTableViewCell *staticContentCell, UITableViewCell *cell, NSIndexPath *indexPath) {
            cell.textLabel.text = [obj objectForKey:@"name"];
            RAReelData *reel = [RAReelData fromDict:obj];
            if ([reel.name isEqual:self.selectedReel.name] ||
                (reel.itemID > 0 && reel.itemID == self.selectedReel.itemID)) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        } whenSelected:^(NSIndexPath *indexPath) {
            [self reelSelectedAtIndexPath:indexPath];
        } atIndexPath:[NSIndexPath indexPathForRow:idx inSection:2] animated:NO];
    }];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"]){
        [textField resignFirstResponder];
        if (textField.text.length > 0) {
            [self addNewReel];
        }
        return NO;
    }else{
        return YES;
    }
}

-(void)reelTextFielddDidChange:(id)sender
{
    if ([[sender text] length] > 0) {
        self.navigationItem.rightBarButtonItem = self.saveButton;        
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

-(void)addNewReel
{
    [self.reelTextField resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReelSelectedForSavingPhoto" object:@{@"name": self.reelTextField.text}];
}

-(void)reelSelectedAtIndexPath:(NSIndexPath *)path
{
    NSDictionary *reel;
    if (self.staticContentSections.count == 3 && path.section == 1) {
        // My reels
        reel = [self.myReels objectAtIndex:path.row];
        DLog(@"Selected REELS: %@", reel);
    } else if((self.staticContentSections.count == 3 && path.section == 2)
              || (self.staticContentSections.count == 2 && path.section == 1)) {
        DLog(@"Selected REELS: %@", [self.popularReels objectAtIndex:path.row]);
        reel = [self.popularReels objectAtIndex:path.row];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReelSelectedForSavingPhoto" object:reel];
}


@end
