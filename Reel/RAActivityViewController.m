//
//  RAActivityViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/12/12.
//
//

#import "RAActivityViewController.h"
#import "RANotificationTableViewController.h"
#import "RAFriendActivityViewController.h"

@interface RAActivityViewController ()

@end

@implementation RAActivityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.segmentedControl setTitle:@"Notifications" forSegmentAtIndex:0];
    [self.segmentedControl setTitle:@"Activity" forSegmentAtIndex:1];
    self.title = @"Activity";

}


- (UIViewController *)viewControllerForSegmentIndex:(NSInteger)index {
    UIViewController *vc;
    switch (index) {
        case 0:
            vc = [[RANotificationTableViewController alloc] initWithStyle:UITableViewStylePlain andEndpoint:ACTIVITY_FEED forClass:@"RAActivityItemData"];
            break;
        case 1:
            vc = [[RAFriendActivityViewController alloc] initWithStyle:UITableViewStylePlain];
            break;
    }
    return vc;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
