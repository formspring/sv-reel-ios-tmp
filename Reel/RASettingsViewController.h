//
//  RASettingsViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/17/12.
//  Copyright (c) 2012 Formspring.me, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMStaticContentTableViewController.h"

typedef enum {
    SETTINGS_SECTION_NOTIFICATIONS,
    SETTINGS_SECTION_ACCOUNT,
    SETTINGS_SECTION_APP
} SETTINGS_SECTION;

@interface RASettingsViewController : JMStaticContentTableViewController

@end
