//
//  RALikesAndCommentsViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/9/12.
//
//

#import <UIKit/UIKit.h>
@class RAPhotoData;


@interface RALikesAndCommentsViewController : UIViewController
@property (strong, nonatomic) RAPhotoData *photo;

+ (id)instanceForPhoto:(RAPhotoData *)photo;
@property (strong, nonatomic) UIViewController *currentViewController;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (assign, getter = shouldShowComments) BOOL showComments;
@property (assign, getter = isPresentedAsModal) BOOL presentedAsModal;

@end
