//
//  RAProfileViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/27/12.
//
//

#import "RAProfileViewController.h"
#import "RAReelViewController.h"
#import "RAPhotosViewController.h"
#import "RASettingsViewController.h"
#import "SVProgressHUD.h"

@interface RAProfileViewController ()

-(void)setRightButton;

@end

@implementation RAProfileViewController
@synthesize segmentedControl;
@synthesize contentView;
@synthesize account;
@synthesize currentViewController;

+ (id)instanceForAccount:(RAAccountData *)account
{
    RAProfileViewController *controller = [[RAProfileViewController alloc] initWithNibName:@"RAProfileViewController" bundle:nil];
    controller.account = account;
    return controller;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    UIViewController *vc = [self viewControllerForSegmentIndex:self.segmentedControl.selectedSegmentIndex];
    [self addChildViewController:vc];
    vc.view.frame = self.contentView.bounds;
    [self.contentView addSubview:vc.view];
    self.currentViewController = vc;
    if (self.account) {        
        if (self.account.itemID == APPDELEGATE.account.itemID) {
            self.title = @"Me";
            UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings_gear.png"] style:UIBarButtonItemStylePlain target:self action:@selector(showSettings)];
            
            self.navigationItem.rightBarButtonItem = settingsButton;
        } else {
            self.title = self.account.name;
            [[RAApiClient sharedClient] get:ACCOUNT_DETAILS withPath:[NSString stringWithFormat:@"%lld", self.account.itemID] queryParams:nil successBlock:^(NSDictionary *data) {
                self.account = [RAAccountData fromDict:data];
                [self setRightButton];
            } errorBlock:^(NSError *error, NSURLRequest *request) {
                RAAlert(@"Error", [error localizedDescription]);
            }];
        }
        [self.segmentedControl setTitle:@"Reels" forSegmentAtIndex:0];
        [self.segmentedControl setTitle:@"Photos" forSegmentAtIndex:1];
    }

    [self.segmentedControl setTintColor:UIColorFromRGB(0xEFEBDF)];

    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)setRightButton
{
    if (self.account.isFollowing) {
        UIBarButtonItem *followButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showUnFollow:)];
        self.navigationItem.rightBarButtonItem = followButton;
    } else {
        UIBarButtonItem *followButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(follow:)];
        self.navigationItem.rightBarButtonItem = followButton;
    }
}

-(void)showUnFollow:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:self.account.name delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Unfollow" otherButtonTitles:nil];
    [sheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [sheet showFromTabBar:APPDELEGATE.tabbarController.tabBar];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self unFollow:nil];
    }
}

- (void)follow:(id)sender
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    __unused UIBarButtonItem *activityItem = [self.navigationItem.rightBarButtonItem initWithCustomView:activity];
    [activity startAnimating];
    
    [[RAApiClient sharedClient] post:FOLLOW
                            withPath:[NSString stringWithFormat:@"%lld", self.account.itemID]
                                data:nil
                        successBlock:^(NSDictionary *data) {
                                self.account.following = YES;
                                [self setRightButton];
                                [SVProgressHUD showSuccessWithStatus:@"Follow successful"];
                            }
                          errorBlock:^(NSError *error, NSURLRequest *request) {
                                RAAlert(@"Error", [error localizedDescription]);
                            }];
}

- (void)unFollow:(id)sender
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    __unused UIBarButtonItem *activityItem = [self.navigationItem.rightBarButtonItem initWithCustomView:activity];
    [activity startAnimating];
    
    [[RAApiClient sharedClient] delete:FOLLOW
                              withPath:[NSString stringWithFormat:@"%lld", self.account.itemID]
                           queryParams:nil
                          successBlock:^(NSDictionary *data) {
                                self.account.following = NO;
                                [self setRightButton];
                                [SVProgressHUD showSuccessWithStatus:@"UnFollow successful"];
                            }
                            errorBlock:^(NSError *error, NSURLRequest *request) {
                                RAAlert(@"Error", [error localizedDescription]);
                            }];
}

- (void)showSettings {
    RASettingsViewController *settingsController = [[RASettingsViewController alloc] initWithStyle:UITableViewStyleGrouped];
    [self.navigationController pushViewController:settingsController animated:YES];
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender {
    UIViewController *vc = [self viewControllerForSegmentIndex:sender.selectedSegmentIndex];
    [self addChildViewController:vc];
    self.navigationItem.leftBarButtonItem = nil;
    
    [self.currentViewController.view removeFromSuperview];
    vc.view.frame = self.contentView.bounds;
    [self.contentView addSubview:vc.view];
    [vc didMoveToParentViewController:self];
    [self.currentViewController removeFromParentViewController];
    self.currentViewController = vc;
    self.navigationItem.title = vc.title;
    if ([self class] == [RAProfileViewController class]) {
        UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings_gear.png"] style:UIBarButtonItemStylePlain target:self action:@selector(showSettings)];
        
        self.navigationItem.rightBarButtonItem = settingsButton;        
    }
}

- (UIViewController *)viewControllerForSegmentIndex:(NSInteger)index {
    UIViewController *vc;
    switch (index) {
        case 0:
            vc = [RAReelViewController instanceForAccount:self.account];
            break;
        case 1:
            vc = [RAPhotosViewController instanceForAccount:self.account];
            break;
    }
    return vc;
}

- (void)viewDidUnload
{
    [self setSegmentedControl:nil];
    [self setContentView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")
        && self.isViewLoaded
        && [self.view window] == nil)
    {
        self.view = nil;
        self.contentView = nil;
        self.segmentedControl = nil;
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
