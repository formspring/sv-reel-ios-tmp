//
//  RAExploreViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 10/23/12.
//
//

#import "RAPhotosViewController.h"
@class RALoggedoutExploreViewController;

@interface RAExploreViewController : RAPhotosViewController

@property (nonatomic, strong) RALoggedoutExploreViewController *parentController;

@end
