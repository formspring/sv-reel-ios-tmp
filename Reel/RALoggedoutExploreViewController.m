//
//  RALoggedoutExploreViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 10/24/12.
//
//

#import "RALoggedoutExploreViewController.h"
#import "RALoginViewController.h"
#import "RASignupViewController.h"

@interface RALoggedoutExploreViewController ()

@end

@implementation RALoggedoutExploreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.exploreVC = [[RAExploreViewController alloc] init];
        self.exploreVC.parentController = self;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.exploreVC.view];
    [self.toolbar setBackgroundImage:[[UIImage imageNamed:@"tabbar"] stretchableImageWithLeftCapWidth:0 topCapHeight:0] forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
    UIImage *login = [[UIImage imageNamed:@"green_btn"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    [self.loginBtn setBackgroundImage:login forState:UIControlStateNormal];
    [self.signupBtn setBackgroundImage:login forState:UIControlStateNormal];
    
    CGRect frame = self.view.bounds;
    frame.size.height -= self.toolbar.frame.size.height;
    self.exploreVC.view.frame = frame;
    [self.view addSubview:self.exploreVC.view];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setLoginBtn:nil];
    [self setSignupBtn:nil];
    [super viewDidUnload];
}



- (IBAction)signupTouched:(id)sender {
    RASignupViewController *signupVC = [[RASignupViewController alloc] initWithNibName:@"RASignupViewController" bundle:nil];
    [self.navigationController pushViewController:signupVC animated:YES];
}

- (IBAction)loginTouched:(id)sender {
    RALoginViewController *loginVC = [[RALoginViewController alloc] initWithNibName:@"RASignupViewController" bundle:nil];
    [self.navigationController pushViewController:loginVC animated:YES];
    
}

@end
