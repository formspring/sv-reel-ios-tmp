//
//  RAPhotoImageView.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/30/12.
//
//

#import "RAPhotoImageView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+AFNetworking.h"
#import "AFImageRequestOperation.h"


#define ZOOM_VIEW_TAG 0x101

@interface RotateGesture : UIRotationGestureRecognizer {}
@end

@implementation RotateGesture
- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer*)gesture{
	return NO;
}
- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer{
	return YES;
}
@end

@interface RAPhotoImageView (Private)
- (void)layoutScrollViewAnimated:(BOOL)animated;
- (void)setupImageViewWithImage:(UIImage *)aImage;
- (CABasicAnimation*)fadeAnimation;
@end



@implementation RAPhotoImageView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
		
		self.backgroundColor = [UIColor blackColor];
		self.userInteractionEnabled = NO;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		self.opaque = YES;
		
		RAPhotoScrollView *scrollView = [[RAPhotoScrollView alloc] initWithFrame:self.bounds];
		scrollView.backgroundColor = [UIColor blackColor];
		scrollView.opaque = YES;
		scrollView.delegate = self;
		[self addSubview:scrollView];
        _scrollView = scrollView;
        
		UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
		imageView.opaque = YES;
		imageView.contentMode = UIViewContentModeScaleAspectFit;
		imageView.tag = ZOOM_VIEW_TAG;
		[_scrollView addSubview:imageView];
        _imageView = imageView;
        self.thumb = [UIImage imageNamed:@"close_x.png"];

		
		UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		activityView.center = self.center;
		activityView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
		[self addSubview:activityView];
        _activityView = activityView;
		
        self.photoActionController = [[RAPhotoActionViewController alloc] init];
        self.photoActionController.photo = self.photo;
        self.photoActionController.view.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [self addSubview:self.photoActionController.view];
        
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 60, 0, 50, 20)];
        [closeButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
        [closeButton setTitle:@"Close" forState:UIControlStateNormal];
        closeButton.layer.backgroundColor = [UIColorFromRGBWithAlpha(0x000000, 0.3) CGColor];
        closeButton.layer.cornerRadius = 5;
        [closeButton addTarget:self action:@selector(closeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:closeButton];

	}
    return self;
}

-(void)closeClicked:(id)sender
{
    [self.delegate closeClikedFor:self];
}

-(void)loadPhoto{
    _loading = YES;
    [_activityView startAnimating];
    self.userInteractionEnabled= NO;
    __block RAPhotoImageView *_self = self;
    DLog("@%@", self.photo.url600x600);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.photo.urlOriginal]];
    [AFImageRequestOperation imageRequestOperationWithRequest:request success:^(UIImage *image) {
        [_self setupImageViewWithImage:image];
        _self = nil;        
    }];
    [self layoutScrollViewAnimated:NO];

}

- (void)layoutSubviews{
	[super layoutSubviews];
    
	if (_scrollView.zoomScale == 1.0f) {
		[self layoutScrollViewAnimated:YES];
	}
    self.userInteractionEnabled = YES;
}

- (void)setupImageViewWithImage:(UIImage*)aImage {
	if (!aImage) return;
    
	_loading = NO;
	[_activityView stopAnimating];
	[self layoutScrollViewAnimated:NO];
	
//	[[self layer] addAnimation:[self fadeAnimation] forKey:@"opacity"];
//	self.userInteractionEnabled = YES;
	
}


#pragma mark Parent Controller Fading

- (void)fadeView{
	
	self.backgroundColor = [UIColor clearColor];
	self.superview.backgroundColor = self.backgroundColor;
	self.superview.superview.backgroundColor = self.backgroundColor;
	
	CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
	[animation setValue:[NSNumber numberWithInt:101] forKey:@"AnimationType"];
	animation.delegate = self;
	animation.fromValue = (id)[UIColor clearColor].CGColor;
	animation.toValue = (id)[UIColor blackColor].CGColor;
	animation.duration = 0.4f;
	[self.layer addAnimation:animation forKey:@"FadeAnimation"];
	
	
}

- (void)resetBackgroundColors{
	
	self.backgroundColor = [UIColor blackColor];
	self.superview.backgroundColor = self.backgroundColor;
	self.superview.superview.backgroundColor = self.backgroundColor;
    
}

#pragma mark Layout

- (void)rotateToOrientation:(UIInterfaceOrientation)orientation{
    
	if (self.scrollView.zoomScale > 1.0f) {
		
		CGFloat height, width;
		height = MIN(CGRectGetHeight(self.imageView.frame) + self.imageView.frame.origin.x, CGRectGetHeight(self.bounds));
		width = MIN(CGRectGetWidth(self.imageView.frame) + self.imageView.frame.origin.y, CGRectGetWidth(self.bounds));
		self.scrollView.frame = CGRectMake((self.bounds.size.width / 2) - (width / 2), (self.bounds.size.height / 2) - (height / 2), width, height);
	} else {
		[self layoutScrollViewAnimated:NO];
	}
    self.photoActionController.view.frame = CGRectMake(10, self.frame.size.height - 60, self.frame.size.width, 40);

}

- (void)layoutScrollViewAnimated:(BOOL)animated{
    
	if (animated) {
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.0001];
	}
    
    DLog(@"%f, %f", self.imageView.image.size.width, self.imageView.image.size.height);
    
	CGFloat hfactor = self.imageView.image.size.width / self.frame.size.width;
	CGFloat vfactor = self.imageView.image.size.height / self.frame.size.height;
	
	CGFloat factor = MAX(hfactor, vfactor);
    if (factor > 0) {
        CGFloat newWidth = self.imageView.image.size.width / factor;
        CGFloat newHeight = self.imageView.image.size.height / factor;
        
        CGFloat leftOffset = (self.frame.size.width - newWidth) / 2;
        CGFloat topOffset = (self.frame.size.height - newHeight) / 2;
        
        self.scrollView.frame = CGRectMake(leftOffset, topOffset, newWidth, newHeight);
        self.scrollView.layer.position = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
        self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width, self.scrollView.bounds.size.height);
        self.scrollView.contentOffset = CGPointMake(0.0f, 0.0f);
        self.imageView.frame = self.scrollView.bounds;
    }
	
    
    
	if (animated) {
		[UIView commitAnimations];
	}
}

#pragma mark Animation

- (CABasicAnimation*)fadeAnimation{
	
	CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
	animation.fromValue = [NSNumber numberWithFloat:0.0f];
	animation.toValue = [NSNumber numberWithFloat:1.0f];
	animation.duration = .3f;
//	animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
	
	return animation;
}

#pragma mark UIScrollView Delegate Methods

- (void)killZoomAnimationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context{
	
	if([finished boolValue]){
		
		[self.scrollView setZoomScale:1.0f animated:NO];
		self.imageView.frame = self.scrollView.bounds;
		[self layoutScrollViewAnimated:NO];
		
	}
	
}

- (void)killScrollViewZoom{
	
	if (!self.scrollView.zoomScale > 1.0f) return;
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDidStopSelector:@selector(killZoomAnimationDidStop:finished:context:)];
	[UIView setAnimationDelegate:self];
    
	CGFloat hfactor = self.imageView.image.size.width / self.frame.size.width;
	CGFloat vfactor = self.imageView.image.size.height / self.frame.size.height;
	
	CGFloat factor = MAX(hfactor, vfactor);
    
	CGFloat newWidth = self.imageView.image.size.width / factor;
	CGFloat newHeight = self.imageView.image.size.height / factor;
    
	CGFloat leftOffset = (self.frame.size.width - newWidth) / 2;
	CGFloat topOffset = (self.frame.size.height - newHeight) / 2;
    
	self.scrollView.frame = CGRectMake(leftOffset, topOffset, newWidth, newHeight);
	self.imageView.frame = self.scrollView.bounds;
	[UIView commitAnimations];
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
	return [self.scrollView viewWithTag:ZOOM_VIEW_TAG];
}

- (CGRect)frameToFitCurrentView{
	
	CGFloat heightFactor = self.imageView.image.size.height / self.frame.size.height;
	CGFloat widthFactor = self.imageView.image.size.width / self.frame.size.width;
	
	CGFloat scaleFactor = MAX(heightFactor, widthFactor);
	
	CGFloat newHeight = self.imageView.image.size.height / scaleFactor;
	CGFloat newWidth = self.imageView.image.size.width / scaleFactor;
	
	
	CGRect rect = CGRectMake((self.frame.size.width - newWidth)/2, (self.frame.size.height-newHeight)/2, newWidth, newHeight);
	
	return rect;
	
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
    
	if (scrollView.zoomScale > 1.0f) {
		
		
		CGFloat height, width, originX, originY;
		height = MIN(CGRectGetHeight(self.imageView.frame) + self.imageView.frame.origin.x, CGRectGetHeight(self.bounds));
		width = MIN(CGRectGetWidth(self.imageView.frame) + self.imageView.frame.origin.y, CGRectGetWidth(self.bounds));
        
		
		if (CGRectGetMaxX(self.imageView.frame) > self.bounds.size.width) {
			width = CGRectGetWidth(self.bounds);
			originX = 0.0f;
		} else {
			width = CGRectGetMaxX(self.imageView.frame);
			
			if (self.imageView.frame.origin.x < 0.0f) {
				originX = 0.0f;
			} else {
				originX = self.imageView.frame.origin.x;
			}
		}
		
		if (CGRectGetMaxY(self.imageView.frame) > self.bounds.size.height) {
			height = CGRectGetHeight(self.bounds);
			originY = 0.0f;
		} else {
			height = CGRectGetMaxY(self.imageView.frame);
			
			if (self.imageView.frame.origin.y < 0.0f) {
				originY = 0.0f;
			} else {
				originY = self.imageView.frame.origin.y;
			}
		}
        
		CGRect frame = self.scrollView.frame;
		self.scrollView.frame = CGRectMake((self.bounds.size.width / 2) - (width / 2), (self.bounds.size.height / 2) - (height / 2), width, height);
		self.scrollView.layer.position = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
		if (!CGRectEqualToRect(frame, self.scrollView.frame)) {
			
			CGFloat offsetY, offsetX;
            
			if (frame.origin.y < self.scrollView.frame.origin.y) {
				offsetY = self.scrollView.contentOffset.y - (self.scrollView.frame.origin.y - frame.origin.y);
			} else {
				offsetY = self.scrollView.contentOffset.y - (frame.origin.y - self.scrollView.frame.origin.y);
			}
			
			if (frame.origin.x < self.scrollView.frame.origin.x) {
				offsetX = self.scrollView.contentOffset.x - (self.scrollView.frame.origin.x - frame.origin.x);
			} else {
				offsetX = self.scrollView.contentOffset.x - (frame.origin.x - self.scrollView.frame.origin.x);
			}
            
			if (offsetY < 0) offsetY = 0;
			if (offsetX < 0) offsetX = 0;
			
			self.scrollView.contentOffset = CGPointMake(offsetX, offsetY);
		}
        
	} else {
		[self layoutScrollViewAnimated:YES];
	}
}

-(void)dealloc
{
    DLog(@"I AM HERE");
}

@end
