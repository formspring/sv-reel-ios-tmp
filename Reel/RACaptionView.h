//
//  RACaptionView.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/9/12.
//
//

#import "MWCaptionView.h"
@class RAPhotoActionViewController;

@interface RACaptionView : MWCaptionView

@property (strong, nonatomic) RAPhotoActionViewController *delegate;

@end
