//
//  ASIHTTPRequest+Reel.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/22/12.
//
//

#import "ASIHTTPRequest.h"

@interface ASIHTTPRequest (Reel)
- (id)jsonData;
@end
