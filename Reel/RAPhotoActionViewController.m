//
//  RAPhotoActionViewController.m
//  Reel
//
//  Created by Sandosh Vasudevan on 8/30/12.
//
//

#import "RAPhotoActionViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Social/Social.h"
#import "SVProgressHUD.h"
#import "RAPhotoBrowser.h"
#import <Twitter/Twitter.h>
#import "UIImageView+AFNetworking.h"

const int padding = 10;

@interface RAPhotoActionViewController () {
    NSMutableArray *shareButtons;
}

@end

@implementation RAPhotoActionViewController
@synthesize likeButton;
@synthesize likeIconButton;
@synthesize commentButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];

    self.view.layer.backgroundColor = [UIColor clearColor].CGColor;
    self.likeIconButton.backgroundColor = UIColorFromRGBWithAlpha(0x000000, 0.30);
    self.likeIconButton.layer.cornerRadius = 5;
    self.likeIconButton.frame = CGRectMake(self.likeIconButton.frame.origin.x, 10, self.likeIconButton.frame.size.width, 25);

    
    self.likeButton.backgroundColor = UIColorFromRGBWithAlpha(0x000000, 0.30);
    self.likeButton.layer.cornerRadius = 5;

    
    self.commentButton.backgroundColor = UIColorFromRGBWithAlpha(0x000000, 0.30);
    self.commentButton.layer.cornerRadius = 5;
    
    self.shareButton.backgroundColor = UIColorFromRGBWithAlpha(0x000000, 0.30);
    self.shareButton.layer.cornerRadius = 5;

    [self addObserver:self forKeyPath:@"photo.likeCount" options:NSKeyValueObservingOptionNew  context:NULL];
    [self addObserver:self forKeyPath:@"photo.commentCount" options:NSKeyValueObservingOptionNew  context:NULL];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerTapped:)];
    [self.headerView addGestureRecognizer:tap];

}

- (void)headerTapped:(id)sender {
    [self.delegate accountClickedForPhoto:self.photo];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    [self setupUI];
}

-(void)setupUI
{
    if (!self.headerView.isHidden) {
        self.nameLabel.text = self.photo.account.name;
        self.reelLabel.text = self.photo.reel.name;
        [self.avatarView setImageWithURL:[NSURL URLWithString:self.photo.account.avatar150]];
    }
    self.likeIconButton.selected = self.photo.hasLiked;
    if (self.photo.likeCount > 0) {
        NSString *likeString;
        if (self.photo.likeCount > 1) {
            likeString = @"Likes";
        } else {
            likeString = @"Like";
        }
        [self.likeButton setTitle:[NSString stringWithFormat:@"%d %@", self.photo.likeCount, likeString] forState:UIControlStateNormal];
    }
    
    if (self.photo.commentCount > 0) {
        NSString *commentString;
        if (self.photo.commentCount > 1) {
            commentString = @"Comments";
        } else {
            commentString = @"Comment";
        }
        [self.commentButton setTitle:[NSString stringWithFormat:@"%d %@", self.photo.commentCount, commentString] forState:UIControlStateNormal];
    } else {
        [self.commentButton setTitle:@"Comment" forState:UIControlStateNormal];
    }
    
    [self.likeButton sizeToFit];
    CGRect likeButtonRect = CGRectMake(self.likeButton.frame.origin.x, 10, self.likeButton.frame.size.width + 20, 25);
    CGFloat commentButtonX;
    if (self.photo.likeCount == 0) {
        likeButtonRect.size.width = 0;
        self.likeButton.frame = likeButtonRect;
        commentButtonX = self.likeIconButton.frame.origin.x + self.likeIconButton.frame.size.width + 10;
    } else {
        self.likeButton.frame = likeButtonRect;
        commentButtonX = self.likeButton.frame.origin.x + self.likeButton.frame.size.width + 10;
    }
    
    [self.commentButton sizeToFit];
    self.commentButton.frame = CGRectMake(commentButtonX, 10, self.commentButton.frame.size.width + 20, 25);
    
    self.shareButton.frame = CGRectMake(self.view.frame.size.width - self.shareButton.frame.size.width - 10, 10, self.shareButton.frame.size.width, 25);
}

- (void)viewDidUnload
{
    [self setCommentButton:nil];
    [self setLikeButton:nil];
    [self setLikeIconButton:nil];
    [self removeObserver:self forKeyPath:@"photo.likeCount" context:NULL];
    [self removeObserver:self forKeyPath:@"photo.commentCount" context:NULL];

    [self setShareButton:nil];
    [self setAvatarView:nil];
    [self setNameLabel:nil];
    [self setReelLabel:nil];
    [self setHeaderView:nil];
    [self setFooterView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (IBAction)likeIconButtonClicked:(id)sender {
    [self.photo toggleLike];
    [self setupUI];
}

- (IBAction)likeListButtonClicked:(id)sender {
    [self.delegate likeListClickedForPhoto:self.photo];
}

- (IBAction)commentButtonClicked:(id)sender {
    [self.delegate commentClickedForPhoto:self.photo];
}

- (IBAction)shareButtonClicked:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc] init];
    sheet.title = @"Share";
    shareButtons = [NSMutableArray array];
    if (SYSTEM_VERSION_LESS_THAN(@"6.0")) {
        if ([TWTweetComposeViewController canSendTweet]) {
            [shareButtons addObject:@"Twitter"];
        }
    } else {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            [shareButtons addObject:@"Facebook"];
        }
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            [shareButtons addObject:@"Twitter"];
        }
    }
    if ([MFMailComposeViewController canSendMail]) {
        [shareButtons addObject:@"Email"];
    }
    if ([MFMessageComposeViewController canSendText]) {
        [shareButtons addObject:@"Message"];
    }
    [shareButtons addObject:@"Copy Link"];
    [shareButtons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [sheet addButtonWithTitle:obj];
    }];
    [sheet addButtonWithTitle:@"Cancel"];
    sheet.destructiveButtonIndex = [shareButtons count];
    sheet.delegate = self;
    [sheet showFromTabBar:[APPDELEGATE.tabbarController tabBar]];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSURL *photoWebURL = [NSURL URLWithString:self.photo.webURL];
    NSString *shareText = ([self.photo.reel.name length] == 0 ? @"Photo" : self.photo.reel.name);
    NSString *shareTextHTML = [NSString stringWithFormat:@"<a href=\"%@\"><img src=\"%@\" /></a><br /><br /><a href=\"%@\">See the full photo on Reel</a>", self.photo.webURL, self.photo.url300x300, self.photo.webURL];
    
    if (buttonIndex == [shareButtons count]) {
        
    }
    else if ([shareButtons[buttonIndex] isEqual:@"Facebook"]) {
        SLComposeViewController *FBComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [FBComposer setInitialText:shareText];
        [FBComposer addURL:photoWebURL];
        [[self topViewController] presentViewController:FBComposer animated:YES completion:nil];
    }
    else if ([shareButtons[buttonIndex] isEqual:@"Twitter"]) {
        if (SYSTEM_VERSION_LESS_THAN(@"6.0")) {
            TWTweetComposeViewController *TWComposer = [[TWTweetComposeViewController alloc] init];
            [TWComposer setInitialText:shareText];
            [TWComposer addURL:photoWebURL];
            [[self topViewController] presentViewController:TWComposer animated:YES completion:nil];
        } else {
            SLComposeViewController *TWComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [TWComposer setInitialText:shareText];
            [TWComposer addURL:photoWebURL];
            [[self topViewController] presentViewController:TWComposer animated:YES completion:nil];
        }
    }
    else if ([shareButtons[buttonIndex] isEqual:@"Email"]) {
        MFMailComposeViewController *mfViewController = [[MFMailComposeViewController alloc] init];
        mfViewController.mailComposeDelegate = self;
        [mfViewController setSubject:shareText];
        [mfViewController setMessageBody:shareTextHTML isHTML:YES];
        [[self topViewController] presentModalViewController:mfViewController animated:YES];
    }
    else if ([shareButtons[buttonIndex] isEqual:@"Message"]) {
        MFMessageComposeViewController *mfmViewController = [[MFMessageComposeViewController alloc] init];
        mfmViewController.messageComposeDelegate = self;
        mfmViewController.body = [NSString stringWithFormat:@"%@ %@", shareText, self.photo.webURL];
        [[self topViewController] presentModalViewController:mfmViewController animated:YES];
    }
    else {
        UIPasteboard *pb = [UIPasteboard generalPasteboard];
        [pb setString:self.photo.webURL];
        [SVProgressHUD showSuccessWithStatus:@"Copied"];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {    
    switch (result) {
        case MFMailComposeResultCancelled:
//            [SVProgressHUD showErrorWithStatus:@"Message Canceled"];
            break;
        case MFMailComposeResultSaved:
            [SVProgressHUD showSuccessWithStatus:@"Message Saved"];
            break;
        case MFMailComposeResultSent:
            [SVProgressHUD showSuccessWithStatus:@"Message Sent"];
            break;
        case MFMailComposeResultFailed:
            [SVProgressHUD showErrorWithStatus:@"Message Failed"];
            break;
        default:
            [SVProgressHUD showErrorWithStatus:@"Message Not Sent"];
            break;
    }
    
    [[self topViewController] dismissModalViewControllerAnimated:YES];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
            [SVProgressHUD showErrorWithStatus:@"Message Failed"];
            break;
        case MessageComposeResultSent:
            [SVProgressHUD showSuccessWithStatus:@"Message Sent"];
            break;
        default:
            break;
    }
    [[self topViewController] dismissModalViewControllerAnimated:YES];
}

- (UIViewController *)topViewController
{
    if ([self.delegate isKindOfClass:[RAPhotoBrowser class]]) {
        return (UIViewController*)self.delegate;
    } else {
        return APPDELEGATE.tabbarController;
    }
}

@end
