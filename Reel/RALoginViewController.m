//
//  RALoginViewController.m
//  Reel
//
//  Created by Tim Bowen on 10/3/12.
//
//

#import "RALoginViewController.h"
#import "RAApiClient.h"
#import "SVProgressHUD.h"
#import "RAAppDelegate.h"
#import "RAForgotPasswordViewController.h"

@interface RALoginViewController ()

@end

@implementation RALoginViewController

@synthesize viewConfigured = _viewConfigured;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.viewConfigured = NO;
        self.title = @"Login";
    }
    return self;
}


//U FIX
- (IBAction)signupTouched:(id)sender {
    [SVProgressHUD show];
    
    [[RAApiClient sharedClient] get:SESSION withPath:nil queryParams:self.signupInfo  successBlock:^(NSDictionary *data) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:data];
            [defaults setObject:archivedData forKey:@"session"];
            [defaults synchronize];
            [SVProgressHUD showSuccessWithStatus:@""];
            [APPDELEGATE initializeAccount];
            [APPDELEGATE showLoggedInAppView];
        }
        errorBlock:^(NSError *error, NSURLRequest *request) {
                             DLog(@"%@", [error localizedDescription]);
            [SVProgressHUD dismiss];
            RAAlert(@"Error", @"There was a problem logging you in");
        }];

     
     
}

- (void)accountInfoReady:(id)accountInfo withType:(NSString *)type atIndex:(int)index {
    [super accountInfoReady:accountInfo withType:type atIndex:index];
    if(index == 1) {
        [self signupTouched:nil];
    }
}
- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    if (!self.viewConfigured) {
        self.signupButton.center = CGPointMake(self.signupButton.center.x, self.signupButton.center.y - 95);
        UIButton *forgotPassword = [UIButton buttonWithType:UIButtonTypeCustom];
        forgotPassword.frame = CGRectMake(self.view.frame.size.width - 100 - 10, self.signupButton.frame.origin.y + self.signupButton.frame.size.height + 5, 100, self.signupButton.frame.size.height/2);
        forgotPassword.titleLabel.textAlignment = UITextAlignmentRight;
        [self.view addSubview:forgotPassword];
        forgotPassword.titleLabel.frame = forgotPassword.frame;
        forgotPassword.titleLabel.textAlignment = UITextAlignmentRight;
        forgotPassword.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
        [forgotPassword setTitle:@"Forgot Password?" forState:UIControlStateNormal];
        [forgotPassword setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [forgotPassword addTarget:self action:@selector(forgotPasswordTouched) forControlEvents:UIControlEventTouchUpInside];
        self.viewConfigured = YES;
    }
}

- (void)viewWillUnload {
    self.viewConfigured = NO;
}

- (void)forgotPasswordTouched {
    RAForgotPasswordViewController *forgotVC = [[RAForgotPasswordViewController alloc]  initWithNibName:@"RASignupViewController" bundle:nil];
    [self.navigationController pushViewController:forgotVC animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"CellID";
    
    RAAccountInfoTableCell *cell = nil;
    
    switch (indexPath.row) {
            //Title is user facing string
            //Type is param name
        case 0:
            cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"email"];
            cell.title = @"Email";
            break;
        case 1:
            NSLog(@"Password");
            cell = [[RAAccountInfoTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID forType:@"password"];
            cell.title = @"Password";
            break;
        default:
            
            break;
    }
    
    NSLog(@"Returning a cell or something %@", cell);
    cell.tag = indexPath.row;
    cell.delegate = self;
    return cell;
    
}

- (void)loadView {
    [super loadView];
    [self.signupButton setTitle:@"Login" forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
