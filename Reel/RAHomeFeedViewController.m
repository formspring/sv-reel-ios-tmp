//
//  RAHomeFeedViewController.m
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RAHomeFeedViewController.h"
#import "RAAppDelegate.h"
#import "RAFeedItemArray.h"
#import "RAReelData.h"
#import "RAAccountData.h"
#import "RAFeedItemCell.h"
#import "RASettingsViewController.h"
#import "RAApiClient.h"
#import "RACommentViewController.h"
#import "RAPhotoOperationQueue.h"
#import "SVProgressHUD.h"
#import "RAPhotosViewController.h"
#import "RALikeListController.h"
#import "RALikesAndCommentsViewController.h"
#import "RACamHolderViewController.h"
#import "RAReelHeaderCell.h"

@implementation RAHomeFeedViewController

@synthesize uploadQueue = _uploadQueue;
@synthesize loadingMore = _loadingMore;

- (id)initWithStyle:(UITableViewStyle)style andEndpoint:(API_ENDPOINT)endpoint forClass:(NSString *)className
{
    self = [super initWithStyle:style andEndpoint:endpoint forClass:className];
    if (self) {
        self.loadingMore = NO;
        // Custom initialization
        self.uploadQueue = [[NSMutableArray alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(photoUploaded:)
                                                     name:@"PhotoUploaded"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(photoUploading:)
                                                     name:@"PhotoUploading"
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(photoDeleted:)
                                                     name:@"PhotoDeleted"
                                                   object:nil];
        self.endpoint = HOME_FEED;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Home";
    self.trackedViewName = self.title;
 
    
    UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"topbar_logo.png"]];
    view.contentMode = UIViewContentModeCenter;
    self.navigationItem.titleView = view;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    UIImageView *titleView = (UIImageView*)self.navigationItem.titleView;
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        titleView.image = [UIImage imageNamed:@"topbar_logo.png"];
    } else {
        titleView.image = [UIImage imageNamed:@"topbar_logo_landscape.png"];        
    }
}


- (void)requestFinished:(ASIHTTPRequest *)request {
    NSMutableArray *items = [RAFeedItemArray fromHttpRequest:request];
    self.items = items; 
    [self.tableView reloadData];
    [self hideLoadingIndicator];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSLog(@"%@", [request requestHeaders]);
    NSLog(@"%@", [[request url] absoluteString]);
    NSLog(@"API Failure: %@ %@",[self class], [[request error] localizedDescription]);
    [self hideLoadingIndicator];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.uploadQueue = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (APPDELEGATE.account.reminder != nil) {
        RACamHolderViewController *camHolder = [[RACamHolderViewController alloc] init];
        camHolder.delegate = self;
        camHolder.reminder = APPDELEGATE.account.reminder;
        camHolder.newUser = APPDELEGATE.account.isNew;
        camHolder.camViewController.newUser = APPDELEGATE.account.isNew;
        camHolder.camViewController.infoHidden = NO;
        UINavigationController *cameraNav = [[UINavigationController alloc] initWithRootViewController:camHolder];
        cameraNav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.tabBarController presentModalViewController:cameraNav animated:YES];
        [camHolder.camViewController viewDidAppear:NO];

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [defaults objectForKey:@"session"];
        if (data != nil) {
            NSDictionary *session = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            NSMutableDictionary *sessionM = [NSMutableDictionary dictionaryWithDictionary:session];
            NSMutableDictionary *account = [NSMutableDictionary dictionaryWithDictionary:[sessionM objectForKey:@"account"]];
            [account setObject:[NSNumber numberWithBool:NO] forKey:@"new"];
            [account removeObjectForKey:@"reminder"];
            APPDELEGATE.account.reminder = nil;
            
            [sessionM setObject:account forKey:@"account"];

            NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:sessionM];
            [defaults setObject:archivedData forKey:@"session"];
            [defaults synchronize];

        }

    }
}

-(void)photoSaved:(BOOL)save inReel:(RAReelData *)reel
{
    [self.tabBarController dismissModalViewControllerAnimated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ([APPDELEGATE.photoQueueArray count] > 0) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([APPDELEGATE.photoQueueArray count] > 0 && section == 0) {
        return [APPDELEGATE.photoQueueArray count] * 2;
    }
    return [self.items count] * 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        RAReelHeaderCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"ReelHeaderCell"];
        if (headerCell == nil) {
            headerCell = [[RAReelHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReelHeaderCell"];
            headerCell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        headerCell.type = RAReelHeaderCellTypeReel;
        if ([APPDELEGATE.photoQueueArray count] > 0 && indexPath.section == 0) {
            RAPhotoOperationQueue *q = [APPDELEGATE.photoQueueArray objectAtIndex:indexPath.row/2];
            headerCell.photo = q.mockPhotoData;
        } else {
            headerCell.photo = [self.items objectAtIndex:indexPath.row/2];
        }
        headerCell.delegate = self;
        return headerCell;
    }
    static NSString *homeFeedCellIdentifier = @"HomeFeedCell";
    RAFeedItemCell *homeFeedCell = [tableView dequeueReusableCellWithIdentifier:homeFeedCellIdentifier];
    if(homeFeedCell == nil) {
        homeFeedCell = [[RAFeedItemCell alloc] initWithFeedItem:nil andReuseIdentifier:homeFeedCellIdentifier];
    }
    
    RAPhotoData *feedItem;
    if ([APPDELEGATE.photoQueueArray count] > 0 && indexPath.section == 0) {
        RAPhotoOperationQueue *q = [APPDELEGATE.photoQueueArray objectAtIndex:indexPath.row/2];
        feedItem = q.mockPhotoData;
        homeFeedCell.photoQueue = q;
        homeFeedCell.uploading = YES;
    } else {
        feedItem = [self.items objectAtIndex:floor(indexPath.row/2)];
        homeFeedCell.uploading = NO;
    }
    homeFeedCell.feedItem = feedItem;
    homeFeedCell.delegate = self;
    homeFeedCell.indexPath = indexPath;
    return homeFeedCell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //iPhone size, need iPad design. 
    //Ask Mira what to do in Landscape mode - same height?
    
    //Portrait height:
    //return self.navigationController.visibleViewController.view.frame.size.height + 1;
    
    //Evaluates to:
    if (indexPath.row %2 == 0) {
        return 50;
    }
    return 318;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!([APPDELEGATE.photoQueueArray count] > 0 && indexPath.section == 0)
        && indexPath.row % 2 == 0) {
        RAPhotoData *photo = [self.items objectAtIndex:indexPath.row/2];
        [self showProfile:photo.account];
    }
}



#pragma mark - Upload indicators
- (void)photoUploaded:(NSNotification *)notification
{
    RAPhotoOperationQueue *queue = notification.object;
    if (queue.result != nil) {
        RAPhotoData *item = [RAPhotoData fromDict:queue.result];
        [self.items insertObject:item atIndex:0];
        [SVProgressHUD showSuccessWithStatus:@"Saved!"];
    } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                RAAlert(@"Error - Failed photo request", [queue.error localizedDescription]);
                NSError *error = queue.error;
                DLog(@"Error: %@", [error localizedDescription]);
            });
    }
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (void)photoUploading:(NSNotification *)notification
{
    [self.tableView reloadData];
    self.tabBarController.selectedIndex = 0;
    [self.tableView setContentOffset:CGPointZero animated:NO];
}

- (void)photoDeleted:(NSNotification *)notification
{
    NSArray *photoIds = notification.object;
    NSMutableIndexSet *foundIds = [[NSMutableIndexSet alloc] init];
    NSMutableArray *foundIndexPaths = [NSMutableArray array];
    NSUInteger section = (APPDELEGATE.photoQueueArray.count > 0) ? 1 : 0;
    [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        RAPhotoData *photo = obj;
        if ([photoIds indexOfObject: @(photo.itemID)] != NSNotFound) {
            [foundIndexPaths addObject:[NSIndexPath indexPathForRow:idx*2 inSection:section]];
            [foundIndexPaths addObject:[NSIndexPath indexPathForRow:(idx*2)+1 inSection:section]];
            [foundIds addIndex:idx];
        }
    }];
    [self.tableView beginUpdates];
    [self.items removeObjectsAtIndexes:foundIds];
    [self.tableView deleteRowsAtIndexPaths:foundIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    if (self.items.count == 0) {
        [_emptyLabel setHidden:NO];
        [self.tableView.backgroundView setHidden:NO];
    }
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)cameraViewDismissed
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
