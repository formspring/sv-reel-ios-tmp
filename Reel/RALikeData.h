//
//  RALikeData.h
//  Reel
//
//  Created by Tim Bowen on 9/4/12.
//
//

#import <Foundation/Foundation.h>

@class RAAccountData;

@interface RALikeData : NSObject <RADataObject>

@property (assign) unsigned long long itemID;
@property (strong, nonatomic) RAAccountData *liker;
@property (strong, nonatomic) NSDate *created;
@property (assign) unsigned long long photoID;

@end
