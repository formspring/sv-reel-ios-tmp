//
//  RAReminderData.h
//  Reel
//
//  Created by Stephan Soileau on 8/28/12.
//
//

#import <UIKit/UIKit.h>
#import "RADataObject.h"
#import "RAReelData.h"

/**
 [{"id":"182577","account_id":"100135","reel_id":"365036","timestamp":"2012-08-29 18:11:16","notified":"no","photo_preview":[{"id":"182820","account_id":"100135","reel_id":"365036","file":"photo\/20120824\/5038262f491866.79470736.original.jpg","created":"2012-08-25 01:11:31","urls":{"original":"https:\/\/s3.amazonaws.com\/photos.dev.reel\/photo\/20120824\/5038262f491866.79470736.original.jpg","600x600":"https:\/\/s3.amazonaws.com\/photos.dev.reel\/photo\/20120824\/5038262f491866.79470736.600x600.jpg","600x600l":"https:\/\/s3.amazonaws.com\/photos.dev.reel\/photo\/20120824\/5038262f491866.79470736.600x600l.jpg","300x300":"https:\/\/s3.amazonaws.com\/photos.dev.reel\/photo\/20120824\/5038262f491866.79470736.300x300.jpg","300x300l":"https:\/\/s3.amazonaws.com\/photos.dev.reel\/photo\/20120824\/5038262f491866.79470736.300x300l.jpg","150x150":"https:\/\/s3.amazonaws.com\/photos.dev.reel\/photo\/20120824\/5038262f491866.79470736.150x150.jpg","150x150l":"https:\/\/s3.amazonaws.com\/photos.dev.reel\/photo\/20120824\/5038262f491866.79470736.150x150l.jpg"}}]}]
*/
@interface RAReminderData : NSObject <RADataObject>
@property (assign) unsigned long long itemID;
@property (assign, nonatomic) unsigned long accountId;
@property (assign, nonatomic) BOOL notified;
@property (strong, nonatomic) RAReelData* reel;
@property (strong, nonatomic) NSString *question;

+(RAReminderData *) fromDict:(NSDictionary *)dict;
@end
