//
//  RACamPreviewViewController.h
//  Reel
//
//  Created by Sandosh Vasudevan on 9/11/12.
//
//

#import <UIKit/UIKit.h>

@protocol RACamPreviewViewDelegate <NSObject>

-(void)previewAccepted;
-(void)previewDismissed;

@end

@interface RACamPreviewViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *previewImageView;
@property (strong, nonatomic) UIImage *preview;
@property (strong, nonatomic) id<RACamPreviewViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *photoBar;

-(id)initWithPhoto:(UIImage*)image;

@end
