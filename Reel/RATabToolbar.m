//
//  RATabbyToolBarThing.m
//  Reel
//
//  Created by Stephan Soileau on 8/15/12.
//
//

#import "RATabToolbar.h"

@implementation RATabToolbar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)withItems:(NSArray *)items {
    NSMutableArray *myItems = [[NSMutableArray alloc] init];
    for (id it in items) {
        if ([it isKindOfClass:[UIBarButtonItem class]]) {
            [myItems addObject:it];
        } else if([it isKindOfClass:[NSDictionary class]]) {
            // this most likely needs to be restyled but whatver
            SEL s= [[it objectForKey:@"selector"] pointerValue];
            UIBarButtonItem *i = [[UIBarButtonItem alloc] initWithTitle:[it objectForKey:@"title"]  style:UIBarButtonItemStylePlain target:[it objectForKey:@"target"] action:s];
            [myItems addObject:i];
        }
            
    }
    [self setItems:[NSArray arrayWithArray:myItems]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
