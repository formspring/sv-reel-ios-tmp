//
//  UINavigationController+RATransition.m
//  Reel
//
//  Created by Sandosh Vasudevan on 9/9/12.
//
//

#import "UINavigationController+RATransition.h"

@implementation UINavigationController (RATransition)

- (void) pushController: (UIViewController*) controller
         withTransition: (UIViewAnimationTransition) transition
{
    [UIView beginAnimations:nil context:NULL];
    [self pushViewController:controller animated:NO];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationTransition:transition forView:self.view cache:YES];
    [UIView commitAnimations];
}

- (void) presentModalViewController:(UIViewController *)modalViewController withTransition:(UIViewAnimationTransition)transition
{
    [UIView beginAnimations:nil context:NULL];
    [self presentModalViewController:modalViewController animated:NO];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationTransition:transition forView:self.view cache:YES];
    [UIView commitAnimations];
}

- (void)popControllerWithTransition: (UIViewAnimationTransition) transition
{
    [UIView beginAnimations:nil context:NULL];
    [self popViewControllerAnimated:NO];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationTransition:transition forView:self.view cache:YES];
    [UIView commitAnimations];
    
}

@end
