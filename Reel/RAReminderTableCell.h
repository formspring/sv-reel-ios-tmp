//
//  RAReminderTableCell.h
//  Reel
//
//  Created by Stephan Soileau on 8/28/12.
//
//

#import <UIKit/UIKit.h>
@class RAReminderData;


@interface RAReminderTableCell : UITableViewCell
@property (strong, nonatomic) RAReminderData *reminder;
@property (strong, nonatomic) UIImageView *reminderPhoto;

- (NSURL *) getPreviewUrl;
@end
