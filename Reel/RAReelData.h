//
//  RAReelData.h
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RAReelData : NSObject <RADataObject>

@property (assign) unsigned long long accountID;
@property (assign) unsigned long long itemID;
@property (strong, nonatomic) NSString *name;
@property (assign) BOOL reminderSettings;
@property (strong) NSDate *created;
@property (strong) NSDate *lastUpdated;
@property (assign) int photoCount;
@property (strong) NSArray *preview;
@property (strong, nonatomic) NSString *webURL;

//TODO
//@property CreatedDate
//@property lastUpdatedDate


+ (RAReelData *)fromDict:(NSDictionary *)dict;


@end
