//
//  RAReminderViewController.m
//  Reel
//
//  Created by Tim Bowen on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RAReminderViewController.h"
#import "RAReminderData.h"
#import "RAPhotoData.h"
#import "RAReminderTableCell.h"
#import "RAThumbnailGridViewController.h"
#import "RAReelData.h"
#import "RACamHolderViewController.h"
#import "RACamViewController.h"
#import "UIImageView+AFNetworking.h"


@interface RAReminderViewController () {
    NSIndexPath *_selectedIndexPath;
}
-(void)removeReminderAtIndexPath:(NSIndexPath *)path pause:(BOOL)pause;
@end

@implementation RAReminderViewController

- (id)initWithStyle:(UITableViewStyle)style andEndpoint:(API_ENDPOINT)endpoint forClass:(NSString *)className
{
    self = [super initWithStyle:style andEndpoint:endpoint forClass:className];
    if (self) {
    }
    return self;
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Reminders";
    self.trackedViewName = self.title;
    self.tableView.backgroundColor = [UIColor whiteColor];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    self.items = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Reminder Size %d", [self.items count]);
    // Return the number of rows in the section.
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"ReminderCell";
    RAReminderTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    RAReminderData *reminderItem = [self.items objectAtIndex:indexPath.row];
    if(cell == nil) {
        NSLog(@"Creating new table cell.");
        cell = [[RAReminderTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];


        //Camera button per Ade:
        UIImageView *cameraButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"camera-icon-reminder.png"]];
        cameraButton.frame = CGRectMake(25, 0, 40, 28);
        cell.accessoryView = cameraButton;
        
    }
    cell.reminder = reminderItem;
    cell.textLabel.text = reminderItem.reel.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Last photo %@", [reminderItem.reel.lastUpdated timeAgo]];
    [cell.reminderPhoto setImageWithURL:[cell getPreviewUrl]
                       placeholderImage:[UIImage imageNamed:@"qwop.png"]];

    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Edit";
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Edit reminder" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:@"Pause", nil];
        sheet.tag = indexPath.row;
        [sheet showFromTabBar:APPDELEGATE.tabbarController.tabBar];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

-(void)removeReminderAtIndexPath:(NSIndexPath *)indexPath pause:(BOOL)pause
{
    NSDictionary *params = nil;
    if (pause) {
        params = @{ @"snooze" : @"true" };
    }
    __block RAReminderData *reminderItem = [self.items objectAtIndex:indexPath.row];
    __block NSIndexPath *_indexPath = indexPath;
    [self.items removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [[RAApiClient sharedClient] delete:REMINDER
                              withPath:[NSString stringWithFormat:@"%lld", reminderItem.itemID]
                           queryParams:params
                          successBlock:^(NSDictionary *data) {
                              _indexPath = nil;
                              reminderItem = nil;
                          } errorBlock:^(NSError *error, NSURLRequest *request) {
                              [self.items insertObject:reminderItem atIndex:indexPath.row];
                              [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:_indexPath] withRowAnimation:UITableViewRowAnimationFade];
                              _indexPath = nil;
                              reminderItem = nil;
                              RAAlert(@"Error", [error localizedDescription]);
                          }];
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Behavior per Ade
    _selectedIndexPath = indexPath;
    
    RAReminderData *reminderItem = [self.items objectAtIndex:indexPath.row];
    [self showCameraForReminder:reminderItem];
}

- (void)showCameraForReminder:(RAReminderData *)reminderItem {
    RACamHolderViewController *camHolder = [[RACamHolderViewController alloc] init];
    camHolder.delegate = self;
    camHolder.reminder = reminderItem;
    camHolder.newUser =  NO;
    camHolder.camViewController.infoHidden = YES;
    UINavigationController *cameraNav = [[UINavigationController alloc] initWithRootViewController:camHolder];
    cameraNav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.tabBarController presentModalViewController:cameraNav animated:YES];
    //Why do I have to do this?
    [camHolder.camViewController viewDidAppear:NO];
}

-(void)photoSaved:(BOOL)save inReel:(RAReelData *)reel
{
    if (save) {
        __block NSUInteger publishedReelIndex;
        [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            RAReminderData *reminder = obj;
            if (reminder.reel.itemID == reel.itemID) {
                publishedReelIndex = idx;
                *stop = YES;
            }
        }];
        [self.items removeObjectAtIndex:publishedReelIndex];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:publishedReelIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        
        if (self.items.count == 0) {
            _emptyLabel = (UILabel*)[self.tableView.backgroundView viewWithTag:100];
            [_emptyLabel setHidden:NO];
            [self.tableView.backgroundView setHidden:NO];
        }
    }
    [self dismissModalViewControllerAnimated:NO];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DLog(@"%d", buttonIndex);
    switch (buttonIndex) {
        case 0:
            [self removeReminderAtIndexPath:[NSIndexPath indexPathForRow:actionSheet.tag inSection:0] pause:NO];
            break;
        case 1:
            [self removeReminderAtIndexPath:[NSIndexPath indexPathForRow:actionSheet.tag inSection:0] pause:YES];
            break;
        default:
            break;
    }
}

@end
