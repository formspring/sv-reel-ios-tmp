//
//  RARefreshTableHeaderView.h
//  Reel
//
//  Created by Sandosh Vasudevan on 8/31/12.
//
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"

@interface RARefreshTableHeaderView : EGORefreshTableHeaderView
-(id)initForFooterViewWithFrame:(CGRect)frame;
@end
