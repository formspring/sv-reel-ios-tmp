//
//  RAPaginatingTableViewController.m
//  Reel
//
//  Created by Tim Bowen on 8/29/12.
//
//

#import "RAPaginatingTableViewController.h"
#import "RADataObject.h"
#import "RAAccountData.h"
#import "RAProfileViewController.h"
#import "RAPhotoData.h"
#import <SDWebImage/SDImageCache.h>
#import "RAPhotoBrowser.h"
#import "RAReelPhotosViewController.h"
#import "RALikesAndCommentsViewController.h"
#import "SVProgressHUD.h"

#define MIN_LOAD_TIME 0.75

@interface RAPaginatingTableViewController ()
{
    NSDate *_loadingBegan;
}
@end

@implementation RAPaginatingTableViewController

@synthesize endpoint = _endpoint;
@synthesize dataClassName = _dataClassName;
@synthesize loadingMore = _loadingMore;
@synthesize paginationParams = _paginationParams;
@synthesize refreshHeader = _refreshHeader;
@synthesize photoPopover = _photoPopover;
@synthesize trackedViewName;

- (id)initWithStyle:(UITableViewStyle)style andEndpoint:(API_ENDPOINT)endpoint forClass:(NSString *)className
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.loadingMore = NO;
        self.items = [[NSMutableArray alloc] init];
        self.endpoint = endpoint;
        self.dataClassName = className;
        
        //Subclass to set params
        self.paginationParams = nil;
        self.urlPath = @"";
        _loadingBegan = [NSDate date];
        _paginatable = YES;
        _refreshable = YES;
    }
    return self;
}

- (void)showPhotoWithID:(unsigned long long)photoID {
    [self showLoadingIndicator];
    [[RAApiClient sharedClient] get:PHOTO withPath:[NSString stringWithFormat:@"%lld", photoID] queryParams:nil successBlock:^(NSDictionary *data) {
        RAPhotoData *photo = [RAPhotoData fromDict:data];
        [self showPhoto:photo];
        [self hideLoadingIndicator];
    } errorBlock:^(NSError *error, NSURLRequest *request) {
        [self hideLoadingIndicator];
    }];
}

- (void)showPhoto:(RAPhotoData *)photo {
    RAPhotoBrowser *browser = [[RAPhotoBrowser alloc] initWithRAPhoto:photo];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
}

- (void)showProfile:(RAAccountData *)account {
    RAProfileViewController *profileVC = [RAProfileViewController instanceForAccount:account];
    [self.navigationController pushViewController:profileVC animated:YES];
}

#pragma mark - RAFeedItemCell delegate methods

- (void)likeClicked:(RAPhotoData *)feedItem atIndexPath:(NSIndexPath *)indexPath {
    [feedItem toggleLike];
}

- (void)showReel:(RAReelData *)reel {
    RAReelPhotosViewController *photosVC = [RAReelPhotosViewController instanceForReel:reel];
    [self.navigationController pushViewController:photosVC animated:YES];
}


- (void)commentClicked:(RAPhotoData *)feedItem
{
    RALikesAndCommentsViewController *vc = [RALikesAndCommentsViewController instanceForPhoto:feedItem];
    vc.showComments = YES;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showLikeList:(RAPhotoData *)feedItem {
    RALikesAndCommentsViewController *vc = [RALikesAndCommentsViewController instanceForPhoto:feedItem];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark 

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.canRefresh) {
        RARefreshTableHeaderView *view = [[RARefreshTableHeaderView alloc]
                                          initWithFrame:CGRectMake(0.0f,
                                                                   0.0f - self.tableView.bounds.size.height,
                                                                   self.tableView.frame.size.width,
                                                                   self.tableView.bounds.size.height)];
        view.delegate = self;
        self.refreshHeader = view;
        [self.refreshHeader refreshLastUpdatedDate];
        self.refreshHeader.delegate = self;
        [self.tableView addSubview:self.refreshHeader];
    }
    
    self.refreshFooter = [[RARefreshTableHeaderView alloc] initForFooterViewWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, COLOR_BAR_HEIGHT)];
    self.tableView.tableFooterView = self.refreshFooter;
    [self.refreshFooter setHidden:YES];
    
    [self.tableView setBackgroundColor:COLOR_BEIGE];
    
    
    [self fetchDataWithParams:nil];
    NSArray *emptyViews = [[NSBundle mainBundle] loadNibNamed:@"EmptyViews" owner:self options:nil];
    int index = -1;
    switch (self.endpoint) {
        case HOME_FEED:
            self.tableView.backgroundView = emptyViews[0];
            index = 0;
            break;
        case ACTIVITY_FEED:
            self.tableView.backgroundView = emptyViews[1];
            index = 1;
            break;
        case REMINDER_FEED:
            self.tableView.backgroundView = emptyViews[2];
            index = 2;
            break;
        case ACCOUNT_DETAILS:
            self.tableView.backgroundView = emptyViews[3];
            index = 3;
            break;
        default:
            break;
    }
    if (self.tableView.backgroundView) {
        _emptyLabel = (UILabel*)[self.tableView.backgroundView viewWithTag:100];
        [_emptyLabel setHidden:YES];
        if(index == 0 || index == 3) {
            [_emptyLabel setTextColor:UIColorFromRGB(0x6d6656)];
        } else if (index == 1 || index == 2) {
            [_emptyLabel setTextColor:UIColorFromRGB(0x938a74)];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.items = nil;
    self.refreshHeader = nil;
    self.refreshFooter = nil;
    _emptyLabel = nil;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")
        && self.isViewLoaded
        && [self.view window] == nil)
    {
        self.items = nil;
        self.refreshHeader = nil;
        self.refreshFooter = nil;
        self.view = nil;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[[GAI sharedInstance] defaultTracker] trackView:self.trackedViewName];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(NSArray *)transformItemsArray:(NSArray *)array{
    return array;
}

-(NSMutableDictionary *)paramsForRefresh {
    return [NSMutableDictionary dictionary];
}

-(void)fetchDataWithParams:(NSDictionary*)params;
{
    NSMutableDictionary *paramsDict = [NSMutableDictionary dictionaryWithDictionary:self.paginationParams];
    [paramsDict addEntriesFromDictionary:params];
    BOOL append = false;
    if ([params objectForKey:@"max_id"] != nil) {
        append = true;
        [self.refreshFooter setHidden:NO];
        [self.refreshFooter setState:EGOOPullRefreshLoading];
    } else {
        [self showLoadingIndicator];
    }
    DLog(@"%@", params);
    self.loadingMore = YES;
    [_emptyLabel setHidden:YES];
    [[RAApiClient sharedClient] get:self.endpoint withPath:self.urlPath queryParams:paramsDict successBlock:^(NSDictionary *dataDict){
        NSArray *data = [dataDict objectForKey:@"matches"];
        if (data == nil) {
            data = (NSArray*)dataDict;
        }
        if([data count] != 0) {
            NSMutableArray *tempResultArray = [NSMutableArray arrayWithCapacity:[data count]];
            
            for (NSDictionary * dict in data) {
                id<RADataObject> newItem = [NSClassFromString(self.dataClassName) fromDict:dict];
                [tempResultArray addObject:newItem];
            }
            
            if (append) {
                NSMutableArray *itemsM = [NSMutableArray arrayWithArray:self.items];
                [itemsM addObjectsFromArray:[self transformItemsArray:tempResultArray]];
                self.items = itemsM;
            } else {
                NSMutableArray *transformedArray = [NSMutableArray arrayWithArray:[self transformItemsArray:tempResultArray]];
                if ([[dataDict objectForKey:@"seen_id"] boolValue]) {
                    [transformedArray addObjectsFromArray:self.items];
                }
                self.items = transformedArray;
            }
            [self.tableView reloadData];
        }
        if (self.items.count == 0) {
            [_emptyLabel setHidden:NO];
        } else {
            [self.tableView.backgroundView setHidden:YES];
        }
        if (!append) {
            //Add min loading
            NSDate *now = [NSDate date];
            NSTimeInterval interval = [now timeIntervalSinceDate:_loadingBegan];
            DLog(@"Nuts to your couch! %f", interval);
            //_loadingBegan is nil but only on the like list... wtf
            [self.refreshHeader egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
            [self.refreshHeader refreshLastUpdatedDate];
            [self hideLoadingIndicator];
        } else {
            [self.refreshFooter setHidden:YES];
            [self.refreshFooter setState:EGOOPullRefreshNormal];
        }
        self.loadingMore = NO;
    }
                         errorBlock:
     ^(NSError *error, NSURLRequest *request){
         self.loadingMore = NO;
         RAAlert(@"Error loading", [error localizedDescription]);
         
         if (!append) {
             [self.refreshHeader egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
             [self hideLoadingIndicator];
         } else {
             [self.refreshFooter setHidden:YES];
             [self.refreshFooter setState:EGOOPullRefreshNormal];
         }
     }];
}

- (void)dismissHeader {
    DLog(@"Calling this dismissheaderlolol");
    self.loadingMore = NO;
    [self.refreshHeader egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.loadingMore || !self.dataClassName) {
        return;
    }
    if (self.tableView.contentSize.height > self.view.bounds.size.height + 100 && self.tableView.contentOffset.y >= self.tableView.contentSize.height - (self.view.bounds.size.height * 2) ) {
        if(indexPath.row == [self.items count] - 1) {
            //Go get some more stuff home boy
            self.loadingMore = YES;

            id<RADataObject> lastItem = [self.items objectAtIndex:[self.items count] - 1];
            if ([lastItem isKindOfClass:[NSArray class]]) {
                lastItem = [(NSArray*)lastItem lastObject];
            }
            if(!lastItem || !lastItem.itemID) {
                return;
            }
                                         
            NSString *idString = [NSString stringWithFormat:@"%lld", lastItem.itemID];
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:idString forKey:@"max_id"];
            [self fetchDataWithParams:params];
        } 
    }
}

- (void)showLoadingIndicator
{
    if (self.refreshHeader == nil) {
        [SVProgressHUD show];
    } else {
        [self.refreshHeader setState:EGOOPullRefreshLoading];
        self.tableView.contentInset = UIEdgeInsetsMake(COLOR_BAR_HEIGHT, 0.0f, 0.0f, 0.0f);
    }
}

- (void)hideLoadingIndicator
{
    if (self.refreshHeader == nil) {
        [SVProgressHUD dismiss];
    } else {
        [self.refreshHeader setState:EGOOPullRefreshNormal];
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0.0f, 0.0f, 0.0f);
    }
}

#pragma mark EGORefreshTableHeaderViewDelegate methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view {
    //Go get more items
    _loadingBegan = [NSDate date];
    self.loadingMore = YES;
    if([self.items count] == 0) {
        //Empty array
        [self fetchDataWithParams:[self paramsForRefresh]];
        return;
    }
    id<RADataObject> firstItem = [self.items objectAtIndex:0];
    if ([firstItem isKindOfClass:[NSArray class]]) {
        firstItem = [(NSArray*)firstItem objectAtIndex:0];
    }
    NSMutableDictionary *params = [self paramsForRefresh];
    if (firstItem != nil) {
        [params setObject:[NSString stringWithFormat:@"%lld",firstItem.itemID] forKey:@"seen_id"];
    }
    DLog(@"%@", params);
    [self fetchDataWithParams:params];
}


- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view {
    return self.loadingMore;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	[self.refreshHeader egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	[self.refreshHeader egoRefreshScrollViewDidEndDragging:scrollView];
    
}


@end
