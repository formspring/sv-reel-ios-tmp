//
//  RANotificationTableViewController.m
//  Reel
//
//  Created by Tim Bowen on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RANotificationTableViewController.h"
#import "ASIHTTPRequest.h"
#import "RAActivityItemArray.h"
#import "RAApiClient.h"
#import "RAActivityItemData.h"
#import "RAAccountData.h"
#import "RAActivityTableCell.h"
#import "UIImageView+AFNetworking.h"


@interface RANotificationTableViewController ()

@end

@implementation RANotificationTableViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Activity";
    self.trackedViewName = self.title;
    self.tableView.backgroundColor = COLOR_BEIGE;
}

-(void)setItems:(NSMutableArray *)items
{
    [super setItems:items];
    if (self.items.count > 0) {
        self.tableView.backgroundColor = [UIColor whiteColor];
    }
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)avatarTapped:(id)sender {
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *) sender;
    CGPoint currentTouchPosition = [gesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
    RAActivityItemData *item = [self.items objectAtIndex:indexPath.row];
    [self showProfile:item.friendAccount];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ActivityNotificationCell";
    RAActivityTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    RAActivityItemData *item = [self.items objectAtIndex:indexPath.row];
    if(cell == nil) {
        cell = [[RAActivityTableCell alloc] initWIthItem:item andReuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.friendAvatarView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTapped:)];
        [cell.friendAvatarView addGestureRecognizer:tap];
    }
    [cell.friendAvatarView setImageWithURL:[NSURL URLWithString:item.friendAccount.avatar50]
                          placeholderImage:[UIImage imageNamed:@"qwop.png"]];
    cell.textLabel.text = item.friendAccount.name;
    cell.detailTextLabel.text = item.message;
    cell.feedItem = item;
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

#pragma mark - Table view delegate
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
//}

@end
